<?php
/**
 * Update Events For Repeat!
 *
 * Need two type date for view and for repeat,
 * because for repeat don't use minutes and seconds for Mysql fast search BY HOUR.
 *
 * In this script set MySQL params:
 * start_time - start event, it is equal get time from (f_inicio + h_inicio)
 * live_time - live events from start, it is equal get time from (f_fin - h_fin) - (f_inicio + h_inicio)
 * And change date start and date finish, if they are not corrected
 *
 * For repeat needed params:
 * f_inicio, h_inicio - standard parameter start date, use in main query;
 * f_fin, h_fin - standard parameter finish date, use in main query;
 * year - parameter year start date, use in main query;
 *
 * start_time - start event, it is equal get time from (f_inicio + h_inicio), use in repeat query;
 * live_time - live events from start, it is equal get time from (f_fin - h_fin) - (f_inicio + h_inicio), use in repeat query;
 * repeat - it is number, type repeat:
 *      0 - not repeat
 *      1 - repeat every day
 *      2 - repeat every week
 *      3 - repeat every month
 *      4 - repeat every year
 *
 * !!! Please set time in PHP server Timezone !!!
 *
 */
require_once '../server/config.php';

$query = 'SELECT * FROM timeline_2012 WHERE `repeat` > 0';

$res = mysql_query($query);
while ($row = mysql_fetch_assoc($res)) {
#   Start Date
	$f_inicio = explode('-', $row['f_inicio']);
	if (count($f_inicio) < 3) {
		echo "Error Exist DATE id event id - " . $row['id_evento'];
		continue;
	}
	$h_inicio = explode(':', $row['h_inicio']);
	if (count($h_inicio) < 3) {
		$h_inicio = array(
			isset($h_inicio[0]) ? $h_inicio[0] : 0,
			isset($h_inicio[1]) ? $h_inicio[1] : 0,
			0
		);
	}
	$rep_start_time = mktime($h_inicio[0], 0, 0, $f_inicio[1], $f_inicio[2], $f_inicio[0]);
	$view_start_time = mktime($h_inicio[0], $h_inicio[1], $h_inicio[2], $f_inicio[1], $f_inicio[2], $f_inicio[0]);

#   End Date
	$f_fin = explode('-', $row['f_fin']);
	if (count($f_fin) < 3) {
		$f_fin = $f_inicio;
	}

	$h_fin = explode(':', $row['h_fin']);
	if (count($h_fin) < 3) {
		$h_fin = array(
			isset($h_fin[0]) ? $h_fin[0] : $h_inicio[0] + 1,
			isset($h_fin[1]) ? $h_fin[1] : $h_inicio[1],
			0
		);
	}
	$rep_finish_time = mktime($h_fin[0], 0, 0, $f_fin[1], $f_fin[2], $f_fin[0]);
	$view_finish_time = mktime($h_fin[0], $h_fin[1], $h_fin[2], $f_fin[1], $f_fin[2], $f_fin[0]);

#   Control date start and End
	if ($rep_start_time >= $rep_finish_time) {
		$rep_finish_time = $rep_start_time + 3600; // IF date finish less date start THEN date finish equal date time + a hour
		$view_finish_time = $view_start_time + 3600; // IF date finish less date start THEN date finish equal date time + a hour
	}


#   Update Event
	$query = "UPDATE
	timeline_2012
SET
	f_inicio = '" . date('Y-m-d', $view_start_time) . "',
	h_inicio = '" . date('H:i:s', $view_start_time) . "',
	`year` = '" . date('Y', $view_start_time) . "',
	f_fin = '" . date('Y-m-d', $view_finish_time) . "',
	h_fin ='" . date('H:i:s', $view_finish_time) . "',
	start_time = " . $rep_start_time . ",
	live_time = " . ($rep_finish_time - $rep_start_time) . "
WHERE
	id_evento = " . $row['id_evento'];
	if (mysql_query($query)) {
		echo "Event " . $row['id_evento'] . " update;\n";
	} else {
		echo '****** Event ' . $row['id_evento'] . " Error update ******;\n";
		echo "$query\n";
	}
}