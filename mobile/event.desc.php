<?php
require_once '../server/config.php';

if (!isset($_REQUEST['id']) && !intval($_REQUEST['id']))
	exit;
$format = array('%m/%d/%Y', '%m/%Y', '%Y');

$query = "SELECT timeline_2012.id_evento
	 , timeline_2012.titulo
     , timeline_2012.descripcion
     , timeline_2012.imagen

     , DATE_FORMAT(timeline_2012.f_inicio, IF (timeline_2012.scale & 8 = 8, '" . $format[0] . "', IF (timeline_2012.scale & 16 = 16, '" . $format[1] . "', '" . $format[2] . "'))) AS f_inicio
	 , TIME_FORMAT(timeline_2012.h_inicio, IF (timeline_2012.scale & 4 = 4, '%H:%i:%S', '')) as h_inicio

	 , DATE_FORMAT(timeline_2012.f_fin, IF (timeline_2012.scale & 8 = 8, '" . $format[0] . "', IF (timeline_2012.scale & 16 = 16, '" . $format[1] . "', '" . $format[2] . "'))) AS f_fin
	 , TIME_FORMAT(timeline_2012.h_fin, IF (timeline_2012.scale & 4 = 4, '%H:%i:%S', '')) as h_fin

     , timeline_2012.f_tipo
     , timeline_2012.etiquetas
     , timeline_2012.lugar
     , timeline_2012.lugar2
     , timeline_2012.source
     , timeline_2012.latitud
     , timeline_2012.longitud
     , timeline_2012.latitud2
     , timeline_2012.longitud2
     , icons.category
     , icons.subcategory
     , icons.subcategory2
     , timeline_2012.fk_user
     , timeline_2012.link_video
     , timeline_2012.link_wiki
     , timeline_2012.link
FROM
  timeline_2012
  LEFT JOIN icons ON timeline_2012.category_id = icons.id
WHERE
  timeline_2012.id_evento = " . (int)$_REQUEST['id'] . " LIMIT 1";

$res = mysql_query($query);

if (!mysql_num_rows($res)) {
	exit();
}


$row = mysql_fetch_assoc($res);

$row['titulo'] = htmlentities($row['titulo'], ENT_COMPAT, 'UTF-8');
$row['descripcion'] = htmlentities($row['descripcion'], ENT_COMPAT, 'UTF-8');

$date_start = $row['f_inicio'];
$time_int = strtotime($row['h_inicio']);
if ($row['h_inicio'] && $time_int != strtotime('00:00:00')) {
	$date_start .= ' ' . $row['h_inicio'];
}
if ($date_start != '00/00/0000' && $date_start != '00/0000' && $date_start != '0000') {
	$row['f_inicio'] = $date_start;
} else {
	$row['f_inicio'] = null;
}

$date_finish = $row['f_fin'];
$time_int = strtotime($row['h_fin']);
if ($row['h_fin'] && $time_int != strtotime('00:00:00')) {
	$date_finish .= ' ' . $row['h_fin'];
}
if ($date_finish != '00/00/0000' && $date_finish != '00/0000' && $date_finish != '0000') {
	$row['f_fin'] = $date_finish;
} else {
	$row['f_fin'] = null;
}

$tags_to_display = array();
$tags = explode(',', $row['etiquetas']);
for ($i = 0, $count_tags = count($tags); $i < $count_tags; $i++) {
	$res_teg = explode(';', $tags[$i]);

	for ($j = 0, $count_tags_2 = count($res_teg); $j < $count_tags_2; $j++) {
		$tags_to_display[] = trim($res_teg[$j]);
	}
}
$row['etiquetas'] = implode(', ', $tags_to_display);

$row['source'] = $row['source'] ? $row['source'] : "50I50";

$url = $row['imagen'] ? (PROTOCOL . URL_PARENT .'/server/thumbnails.php?w=100&h=100&p=1&q=60&url=' . $row['imagen']) : PROTOCOL . URL_MOBILE . 'img/thumb.png';

require_once ('header.php');
?>
<div id="header" data-role="header">
	<a href="<?= PROTOCOL . URL_MOBILE; ?>" data-icon="arrow-l" data-role="button" data-iconpos="notext"></a>

	<h1><?=$row['titulo'];?></h1>
</div>

<div id="event-content">
	<h3><a href="<?= $row['link']; ?>" target="_blank"><?=$row['titulo'];?></a></h3>
	<p><strong>Description: </strong><?= $row['descripcion']; ?></p><br/>
	<span id="ep_img"><img class="float-right" src="<?=$row['imagen']?>" alt=""/></span><br/>

	<p><strong>Date type: </strong><?=$row['f_tipo'];?></p>

	<p><strong>Date Start: </strong><?=$row['f_inicio'];?></p><?php

	if (!empty($row['f_fin'])) :
		?><p><strong>Date End: </strong><?= $row['f_fin']; ?></p><?php
	endif;

	if (!empty($row['lugar'])) :
		?><p><strong>Place Start: </strong><?= $row['lugar']; ?></p><?php
	endif;

	if (!empty($row['lugar2'])) :
		?><p><strong>Place End: </strong><?= $row['lugar2']; ?></p><br/><?php
	endif;

	?><br/>

	<p><strong>Category: </strong><?=$row['category'];?></p><?php

	if (!empty($row['subcategory'])) :
		?><p><strong>Subcategory: </strong><?= $row['subcategory']; ?></p><?php
	endif;

	if (!empty($row['subcategory2'])) :
		?><p><strong>Subcategory2: </strong><?= $row['subcategory2']; ?></p><?php
	endif;

	if (!empty($row['etiquetas'])) :
		?><p><strong>Tags: </strong><?= $row['etiquetas']; ?></p><?php
	endif;

	?><p><strong>Source: </strong><?=$row['source'];?></p>
</div>

</body>
</html>