<<<<<<< HEAD
<?php
require_once '../server/config.php';
require_once PATH_MOBILE . 'server/Table/DB.php';
require_once './server/Obj/Users.php';

$user = Obj_Users::isLogged();
if (empty($user)) {
	header('Location: ' . PROTOCOL . URL_MOBILE);
	header("Status: 404 Not Found");
	exit;
}

require_once '../server/Category.php';
?><!DOCTYPE html>
<html>
<head>
	<title>Inform</title>

	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_MOBILE; ?>css/jquery.mobile.structure-1.3.1.css"/>
	<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_MOBILE; ?>css/themes/50i50.them.min.css"/>

	<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_PARENT ?>time_icons/sprite/mini/category_icons.css"/>

	<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.core.css"/>
	<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.jqm.css"/>
	<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.animation.css"/>

	<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>css/page.inform.css"/>

	<style type="text/css">
			/*
				Default width and height params upload block in global.php file
				../../server/global.php

				Constant:
				define('SCALED_IMG_WIDTH', int width);
				define('SCALED_IMG_HEIGHT', int height);
				define('SCALED_IMG_CROP', bool crop);
			*/
		#box_upload_img,
		#box_upload_img_overflow,
		#box_upload_img_overflow .box_upload_img_view,
		#box_upload_img_overflow .box_upload_text_view p {
			width: <?=SCALED_IMG_WIDTH;?>px;
			height: <?=SCALED_IMG_HEIGHT;?>px;
		}
		
	.ui-loader{
         z-index: 1501;
        }	  
        .modalWindow{
         width: 100%;
         height: 100%;
         position: absolute;
         z-index: 1500;
        }
	</style>

	<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/jquery.mobile-1.3.1.js"></script>

	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.core.js"></script>
	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.datetime.js"></script>
	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.jqm.js"></script>

	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/load-image.min.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/canvas-to-blob.min.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.iframe-transport.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.fileupload.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.fileupload-process.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.fileupload-resize.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.fileupload-validate.js"></script>

	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/page.inform.js"></script>

	<script type="text/javascript">
		var URL_MOBILE = '<?=PROTOCOL . URL_MOBILE;?>',
			IMAGE_SIZE = [<?=SCALED_IMG_WIDTH;?>, <?=SCALED_IMG_HEIGHT;?>, <?=SCALED_IMG_CROP;?>]; // [width, height, crop]
			

  	    jQuery.ajaxSetup({
             // Disable caching of AJAX responses 
             cache: false
            }); 	
	   
	</script>
</head>
<body>

<div data-role="page" data-theme="a">
	<!-- Error Message -->
	<div id="message_inform" data-role="popup" data-transition="flip" data-theme="g" data-history="false"></div>
	<!-- Error Message -->

	<div id="header" data-role="header">
		<a href="<?= PROTOCOL . URL_MOBILE; ?>" rel="external" data-role="button"><img src="imgheader/back.png"></a>
		<button type="button" id="submit_inform" data-theme="g" >Done</button>
		<h1>Inform</h1>
	</div>

	<div data-role="content">
		<form id="form_inform" method="post" action="javascript:void(0);" data-ajax="true"
		      enctype="multipart/form-data">
			<div data-role="collapsible-set">
				<!-- Basic -->
				<div data-role="collapsible" data-icon="false" data-collapsed="false" data-theme="a"
				     data-content-theme="c" data-inset="true">
					<h3>Basic</h3>
					Enter at least a title or a tag.
					<label for="share_title" class="ui-hidden-accessible">Title*</label>
					<input name="share_title" id="share_title" value="" placeholder="Title" type="text">

					<label for="share_tag" class="ui-hidden-accessible">Tags*</label>
					<input name="share_tag" id="share_tag" value="" placeholder="tag1; tag2; tag3" type="text">

					<label for="share_desc" class="ui-hidden-accessible">Description:</label>
					<textarea cols="40" rows="10" name="share_desc" id="share_desc"
					          placeholder="Description"></textarea>

					<div id="box_upload_img">
						<a id="remove_img" href="#" data-rel="close" data-inline="true" data-mini="true"
						   data-iconpos="notext" data-icon="delete" data-role="button" data-corners="true"
						   data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="b"
						   title="Close panel"></a>

						<div id="box_upload_img_overflow">
							<div class="box_upload_img_view"></div>
							<div class="box_upload_text_view">
								<p>Click to insert image<br/>
                                <img src="imgheader/camera.png" alt="" /></p>
							</div>
							<div class="box_upload_img_hide">
								<input class="input_img" id="fileupload" type="file" name="file"/>
							</div>
						</div>
					</div>


					<div id="progress">
						<div class="bar"></div>
					</div>
                                        
<div data-role="controlgroup" data-theme="a" data-type="horizontal" align="center">
         <input type="button" data-theme="a" value="RotateLeft" data-inline="true" id="btnLeft" data-icon="back" data-iconpos="notext">
         <input type="button" data-theme="a" value="RotateRight" data-inline="true" id="btnRight" data-icon="forward" data-iconpos="notext">
</div>
<script>
    $("#btnLeft").click(function() {
	  if ($('#upload_file_link').length)
	  {
	    var imaginea=$('#upload_file_link').attr('value');
        if (imaginea!="" && imaginea!=undefined){
		 $("body").append('<div class="modalWindow"/>');
   	     $.mobile.showPageLoadingMsg("a","");			
 		 $.ajax({
         dataType: 'jsonp',
         jsonp: 'jsonp_callback',
         timeout: 10000,
         url: 'http://50i50.org/website/mobile/rotateImage.php?imagine='+imaginea+'&grade=90',
         success: function jsonp_callback(json) {
          if (json=="done"){
	       $(".modalWindow").remove();	
           $.mobile.hidePageLoadingMsg();	
		   if ($('#box_upload_img_overflow').children(0).children(0).prop('tagName').toLowerCase()=='img'){
            $('#box_upload_img_overflow').children(0).children(0).attr('src',imaginea+'?'+Math.random());
		   }
		   else{
		    var canvas = $('canvas').get(0);;
            var context = canvas.getContext('2d');
            var imageObj = new Image();
            imageObj.onload = function() {
             context.drawImage(imageObj, 0, 0);
            };
           imageObj.src = imaginea+'?'+Math.random(); 
		  }

         }//if done
		 else{
          $(".modalWindow").remove();	
          $.mobile.hidePageLoadingMsg();	
		 }
        },
        error: function(){
        	$(".modalWindow").remove();
            $.mobile.hidePageLoadingMsg(); 
         }
       }); //ajax     
	} //if exist imaginea
  }//if exist id
 });//function left click
		 
		 
		 
$("#btnRight").click(function() {
	  if ($('#upload_file_link').length)
	  {
	    var imaginea=$('#upload_file_link').attr('value');
        if (imaginea!="" && imaginea!=undefined){
		 $("body").append('<div class="modalWindow"/>');
   	     $.mobile.showPageLoadingMsg("a","");			
 		 $.ajax({
         dataType: 'jsonp',
         jsonp: 'jsonp_callback',
         timeout: 10000,
         url: 'http://50i50.org/website/mobile/rotateImage.php?imagine='+imaginea+'&grade=-90',
         success: function jsonp_callback(json) {
          if (json=="done"){
	       $(".modalWindow").remove();	
           $.mobile.hidePageLoadingMsg();	
		   if ($('#box_upload_img_overflow').children(0).children(0).prop('tagName').toLowerCase()=='img'){
            $('#box_upload_img_overflow').children(0).children(0).attr('src',imaginea+'?'+Math.random());
		   }
		   else{
		    var canvas = $('canvas').get(0);
            var context = canvas.getContext('2d');
            var imageObj = new Image();
            imageObj.onload = function() {
             context.drawImage(imageObj, 0, 0);
            };
           imageObj.src = imaginea+'?'+Math.random(); 
		  }
         }//if done
		 else{
          $(".modalWindow").remove();	
          $.mobile.hidePageLoadingMsg();	
		 }
        },
        error: function(){
        	$(".modalWindow").remove();
            $.mobile.hidePageLoadingMsg(); 
         }
       }); //ajax     
	} //if exist imaginea
  }//if exist id
  });//function right click
</script>



					<div data-role="fieldcontain">
						<label for="privacy"> Privacy:</label>
						<select name="privacy" id="privacy" data-role="slider" data-theme="a" data-track-theme="f">
						<option value="privacy" selected="selected" >  Private  </option>
						<option value="public"> Public </option>
							
						</select>
					</div>

					<label for="more_link" class="ui-hidden-accessible">Link</label>
					<input type="text" id="more_link" name="more_link" placeholder="Link (ex: www.50i50.org)"/>
				</div>
				<!-- Basic -->


				<!-- Category -->
				<div data-role="collapsible" data-theme="a"
				     data-content-theme="a" >
					<h4>Category</h4>
					<h4>Quick Selection:</h4>
					<div data-content-theme="a" data-theme="a" data-iconpos="right" id = "quick_public_selection" style = "display:none">
					<fieldset data-role="controlgroup" data-theme="a" data-type="horizontal"  data-corners = "false"  >
							<?php $cnt = 1; ?>
							<?php $objCat = new Category(); foreach ($objCat->aGroupCat as $key => $cat) : ?>
								<?php if($cnt < 9 && $cat['name'] != '' && $cat['name'] != '...'){ ?>
									<input type="radio" value="<?=$cat['use_id'];?>" id="sub_cat_<?=$cat['use_id'];?>" name="category" class = "quick_selector"   />
									<label for="sub_cat_<?=$cat['use_id'];?>"><img src="http://50i50.org/time_icons/<?=$cat['icon'];?>" /> <br />
									<?=$cat['name'];?></label>
								<?php $cnt++ ; } ?>
							<?php endforeach; ?>
					</fieldset>
				    </div>
					<div data-content-theme="a" id = "quick_private_selection" data-theme="a" style = "display:block">
					<fieldset data-role="controlgroup" data-type="horizontal" data-theme="a" data-corners = "false"  >
							<?php $cnt = 1; ?>
							<?php 
							$objCat = array(10001 => array('use_id' => 10001,'name' => 'Event','icon' => 'PUBevent.png'),
											10019 => array('use_id' => 10019,'name' => 'Deal','icon' => 'PUBdeal.png'),
											10057 => array('use_id' => 10057,'name' => 'Personal','icon' => 'PUBperso.png'),
											10085 => array('use_id' => 10085,'name' => 'News','icon' => 'PUBnews.png'),
											10114 => array('use_id' => 10114,'name' => 'Place','icon' => 'PUBplace.png'),
											11300 => array('use_id' => 11300,'name' => 'Transport','icon' => 'PUBtrans.png'),
											11400 => array('use_id' => 11400,'name' => 'Alert','icon' => 'PUBalert.png'),
											11500 => array('use_id' => 11500,'name' => 'Must see','icon' => 'PUBmust.png')
										);
								 foreach ($objCat as $key => $cat) : ?>
								<?php if($cnt < 9 && $cat['name'] != '' && $cat['name'] != '...'){ ?>
									<input type="radio" value="<?=$cat['use_id'];?>" id="sub_cat_<?=$cat['use_id'];?>" name="category" class = "quick_selector" <?php echo $cat['name'] == 'Personal' ? 'checked' : '' ; ?> />
									<label for="sub_cat_<?=$cat['use_id'];?>"><img src="http://50i50.org/time_icons/<?php echo str_replace('PUB','PRI',$cat['icon']);?>" /> <br />
									<?=$cat['name'];?></label>
								<?php $cnt++ ; } ?>
							<?php endforeach; ?>
					</fieldset>
					</div>
					<br />
					<hr />
					<br />
					<a href="javascript:advancedCategoryToggle();" data-role="button" data-icon="arrow-r" data-iconpos="right" data-theme="a" id="show_advanced_category_toggle">Advanced Selection</a>
					<div id="cat_list" data-role="collapsible-set">
						<?php $objCat = new Category(); foreach ($objCat->aGroupCat as $key => $cat) : ?>
							<div class="cat_list" data-role="collapsible" data-theme="l" data-content-theme="j">
								<h3 data-cat_id="<?= $key; ?>" style="position: relative">
									<span
										class="mini_icon <?= str_replace('.png', '', $cat['icon']) ?>"><?=$cat['name'];?></span>
								</h3>

								<div class="sub_cat_list"></div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<!-- Category -->

				<!-- Date/Time -->
				<div data-role="collapsible" data-theme="a"
				     data-content-theme="c"  data-inset="false"
				   >
					<h3>Date/Time</h3>
					<h4>Now and during:</h4>
					<fieldset data-role="controlgroup" data-type="horizontal" id="type_time_box"
					          style="margin:0 auto; text-align:center;">
						<label for="now_during_0">Minute</label>
						<input id="now_during_0" name="now_during" value="minute" type="radio" checked="checked"/>
						<label for="now_during_1">Hour</label>
						<input id="now_during_1" name="now_during" value="hour" type="radio"/>
					</fieldset>

					<label for="time_slider" class="ui-hidden-accessible">Time Slider</label>
					<input type="range" data-theme="c" name="time_slider" id="time_slider" value="1" min="0" max="60"/>
					<br/>
					<hr/>
					<h4>Or enter date and time:</h4>

					<div data-role="fieldcontain">
						<label for="time_start">Starts</label>
						<input id="time_start" value="" type="text" placeholder="Date/Time"/>
					</div>
					<div data-role="fieldcontain">
						<label for="time_end">Ends</label>
						<input id="time_end" value="" type="text" placeholder="Date/Time"/>
					</div>
					<div data-role="fieldcontain">
						<label for="time_repeat">Repeat</label>
						<select name="time_repeat" data-inline="true" data-theme="a" id="time_repeat">
							<option value="0">Never</option>
							<option value="1">Every day</option>
							<option value="2">Every week</option>
							<option value="3">Every month</option>
							<option value="4">Every year</option>
						</select>
					</div>
				</div>
				<!-- Date/Time -->

				<!-- Location -->
				<div data-role="collapsible" data-theme="a"
				     data-content-theme="c" >
					<h3>Location</h3>
					<h4>Default location:</h4>
					<input name="loc_hear" id="loc_hear" class="custom" data-theme="a" type="checkbox" checked="checked" value=""/>
					<label data-inline="true" for="loc_hear">Here</label>
					<br/>
					<hr/>
					<h4>Or enter address:</h4>
					<label for="loc_address" class="ui-hidden-accessible">Address</label>
					<input name="loc_address" id="loc_address" value="" placeholder="Address" type="text">
					<h4>Or coordinates:</h4>
					<label for="loc_coordinates" class="ui-hidden-accessible">Coordinates</label>
					<input name="loc_coordinates" id="loc_coordinates" value="" placeholder="Coordinates (ex: -22.232,45.446)" type="text">
				</div>
				<!-- Location -->

				<input type="hidden" name="loc_default_time" id="loc_default_time"/>

				<input type="hidden" name="loc_start_time" id="loc_start_time"/>
				<input type="hidden" name="loc_end_time" id="loc_end_time"/>

				<input type="hidden" name="loc_custom_address" id="loc_custom_address"/>
				<input type="hidden" name="loc_custom_coordinate" id="loc_custom_coordinate"/>
				<input type="hidden" name="upload_file_link" id="upload_file_link"/>
				<input type="hidden" name="action" value="event_save"/>
				<input type="hidden" name="json" value="1"/>
			</div>
		</form>
	</div>
</div>
<style>
	#quick_public_selection .ui-controlgroup .ui-radio{
		width:50%;
		display:inline;
	}

	#quick_private_selection .ui-controlgroup .ui-radio{
		width:50%;
		display:inline;
	}
</style>
<script type = "text/javascript" >
			$( document ).on( "pageinit", function( event ) {
  				$("#cat_list").hide();

			});
			$( "#privacy" ).on( 'slidestop',function( event ) {
						var cat_type = $(this).val();
						if(cat_type == 'privacy'){
							$('#quick_public_selection').hide();
							$('#quick_private_selection').show();
						}else{
							$('#quick_private_selection').hide();
							$('#quick_public_selection').show();
						}
				});
			function advancedCategoryToggle(){
				$("#cat_list").slideToggle();
			}
</script>
</body>
=======
<?php
require_once '../server/config.php';
require_once PATH_MOBILE . 'server/Table/DB.php';
require_once './server/Obj/Users.php';

$user = Obj_Users::isLogged();
if (empty($user)) {
	header('Location: ' . PROTOCOL . URL_MOBILE);
	header("Status: 404 Not Found");
	exit;
}

require_once '../server/Category.php';
?><!DOCTYPE html>
<html>
<head>
	<title>Inform</title>

	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_MOBILE; ?>css/jquery.mobile.structure-1.3.1.css"/>
	<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_MOBILE; ?>css/themes/50i50.them.min.css"/>

	<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_PARENT ?>time_icons/sprite/mini/category_icons.css"/>

	<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.core.css"/>
	<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.jqm.css"/>
	<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.animation.css"/>

	<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>css/page.inform.css"/>

	<style type="text/css">
			/*
				Default width and height params upload block in global.php file
				../../server/global.php

				Constant:
				define('SCALED_IMG_WIDTH', int width);
				define('SCALED_IMG_HEIGHT', int height);
				define('SCALED_IMG_CROP', bool crop);
			*/
		#box_upload_img,
		#box_upload_img_overflow,
		#box_upload_img_overflow .box_upload_img_view,
		#box_upload_img_overflow .box_upload_text_view p {
			width: <?=SCALED_IMG_WIDTH;?>px;
			height: <?=SCALED_IMG_HEIGHT;?>px;
		}
		
	.ui-loader{
         z-index: 1501;
        }	  
        .modalWindow{
         width: 100%;
         height: 100%;
         position: absolute;
         z-index: 1500;
        }
	</style>

	<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/jquery.mobile-1.3.1.js"></script>

	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.core.js"></script>
	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.datetime.js"></script>
	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.jqm.js"></script>

	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/load-image.min.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/canvas-to-blob.min.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.iframe-transport.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.fileupload.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.fileupload-process.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.fileupload-resize.js"></script>
	<script src="<?= PROTOCOL . URL_MOBILE; ?>plugin/jq-file-upload/js/jquery.fileupload-validate.js"></script>

	<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/page.inform.js"></script>

	<script type="text/javascript">
		var URL_MOBILE = '<?=PROTOCOL . URL_MOBILE;?>',
			IMAGE_SIZE = [<?=SCALED_IMG_WIDTH;?>, <?=SCALED_IMG_HEIGHT;?>, <?=SCALED_IMG_CROP;?>]; // [width, height, crop]
			

  	    jQuery.ajaxSetup({
             // Disable caching of AJAX responses 
             cache: false
            }); 	
	   
	</script>
</head>
<body>

<div data-role="page" data-theme="a">
	<!-- Error Message -->
	<div id="message_inform" data-role="popup" data-transition="flip" data-theme="g" data-history="false"></div>
	<!-- Error Message -->

	<div id="header" data-role="header">
		<a href="<?= PROTOCOL . URL_MOBILE; ?>" rel="external" data-role="button"><img src="imgheader/back.png"></a>
		<button type="button" id="submit_inform" data-theme="g" >Done</button>
		<h1>Inform</h1>
	</div>

	<div data-role="content">
		<form id="form_inform" method="post" action="javascript:void(0);" data-ajax="true"
		      enctype="multipart/form-data">
			<div data-role="collapsible-set">
				<!-- Basic -->
				<div data-role="collapsible" data-icon="false" data-collapsed="false" data-theme="a"
				     data-content-theme="c" data-inset="true">
					<h3>Basic</h3>
					Enter at least a title or a tag.
					<label for="share_title" class="ui-hidden-accessible">Title*</label>
					<input name="share_title" id="share_title" value="" placeholder="Title" type="text">

					<label for="share_tag" class="ui-hidden-accessible">Tags*</label>
					<input name="share_tag" id="share_tag" value="" placeholder="tag1; tag2; tag3" type="text">

					<label for="share_desc" class="ui-hidden-accessible">Description:</label>
					<textarea cols="40" rows="10" name="share_desc" id="share_desc"
					          placeholder="Description"></textarea>

					<div id="box_upload_img">
						<a id="remove_img" href="#" data-rel="close" data-inline="true" data-mini="true"
						   data-iconpos="notext" data-icon="delete" data-role="button" data-corners="true"
						   data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="b"
						   title="Close panel"></a>

						<div id="box_upload_img_overflow">
							<div class="box_upload_img_view"></div>
							<div class="box_upload_text_view">
								<p>Click to insert image<br/>
                                <img src="imgheader/camera.png" alt="" /></p>
							</div>
							<div class="box_upload_img_hide">
								<input class="input_img" id="fileupload" type="file" name="file"/>
							</div>
						</div>
					</div>


					<div id="progress">
						<div class="bar"></div>
					</div>
                                        
<div data-role="controlgroup" data-theme="a" data-type="horizontal" align="center">
         <input type="button" data-theme="a" value="RotateLeft" data-inline="true" id="btnLeft" data-icon="back" data-iconpos="notext">
         <input type="button" data-theme="a" value="RotateRight" data-inline="true" id="btnRight" data-icon="forward" data-iconpos="notext">
</div>
<script>
    $("#btnLeft").click(function() {
	  if ($('#upload_file_link').length)
	  {
	    var imaginea=$('#upload_file_link').attr('value');
        if (imaginea!="" && imaginea!=undefined){
		 $("body").append('<div class="modalWindow"/>');
   	     $.mobile.showPageLoadingMsg("a","");			
 		 $.ajax({
         dataType: 'jsonp',
         jsonp: 'jsonp_callback',
         timeout: 10000,
         url: 'http://50i50.org/website/mobile/rotateImage.php?imagine='+imaginea+'&grade=90',
         success: function jsonp_callback(json) {
          if (json=="done"){
	       $(".modalWindow").remove();	
           $.mobile.hidePageLoadingMsg();	
		   if ($('#box_upload_img_overflow').children(0).children(0).prop('tagName').toLowerCase()=='img'){
            $('#box_upload_img_overflow').children(0).children(0).attr('src',imaginea+'?'+Math.random());
		   }
		   else{
		    var canvas = $('canvas').get(0);;
            var context = canvas.getContext('2d');
            var imageObj = new Image();
            imageObj.onload = function() {
             context.drawImage(imageObj, 0, 0);
            };
           imageObj.src = imaginea+'?'+Math.random(); 
		  }

         }//if done
		 else{
          $(".modalWindow").remove();	
          $.mobile.hidePageLoadingMsg();	
		 }
        },
        error: function(){
        	$(".modalWindow").remove();
            $.mobile.hidePageLoadingMsg(); 
         }
       }); //ajax     
	} //if exist imaginea
  }//if exist id
 });//function left click
		 
		 
		 
$("#btnRight").click(function() {
	  if ($('#upload_file_link').length)
	  {
	    var imaginea=$('#upload_file_link').attr('value');
        if (imaginea!="" && imaginea!=undefined){
		 $("body").append('<div class="modalWindow"/>');
   	     $.mobile.showPageLoadingMsg("a","");			
 		 $.ajax({
         dataType: 'jsonp',
         jsonp: 'jsonp_callback',
         timeout: 10000,
         url: 'http://50i50.org/website/mobile/rotateImage.php?imagine='+imaginea+'&grade=-90',
         success: function jsonp_callback(json) {
          if (json=="done"){
	       $(".modalWindow").remove();	
           $.mobile.hidePageLoadingMsg();	
		   if ($('#box_upload_img_overflow').children(0).children(0).prop('tagName').toLowerCase()=='img'){
            $('#box_upload_img_overflow').children(0).children(0).attr('src',imaginea+'?'+Math.random());
		   }
		   else{
		    var canvas = $('canvas').get(0);
            var context = canvas.getContext('2d');
            var imageObj = new Image();
            imageObj.onload = function() {
             context.drawImage(imageObj, 0, 0);
            };
           imageObj.src = imaginea+'?'+Math.random(); 
		  }
         }//if done
		 else{
          $(".modalWindow").remove();	
          $.mobile.hidePageLoadingMsg();	
		 }
        },
        error: function(){
        	$(".modalWindow").remove();
            $.mobile.hidePageLoadingMsg(); 
         }
       }); //ajax     
	} //if exist imaginea
  }//if exist id
  });//function right click
</script>



					<div data-role="fieldcontain">
						<label for="privacy"> Privacy:</label>
						<select name="privacy" id="privacy" data-role="slider" data-theme="a" data-track-theme="f">
						<option value="privacy" selected="selected" >  Private  </option>
						<option value="public"> Public </option>
							
						</select>
					</div>

					<label for="more_link" class="ui-hidden-accessible">Link</label>
					<input type="text" id="more_link" name="more_link" placeholder="Link (ex: www.50i50.org)"/>
				</div>
				<!-- Basic -->


				<!-- Category -->
				<div data-role="collapsible" data-theme="a"
				     data-content-theme="a" >
					<h4>Category</h4>
					<h4>Quick Selection:</h4>
					<div data-content-theme="a" data-theme="a" data-iconpos="right" id = "quick_public_selection" style = "display:none">
					<fieldset data-role="controlgroup" data-theme="a" data-type="horizontal"  data-corners = "false"  >
							<?php $cnt = 1; ?>
							<?php $objCat = new Category(); foreach ($objCat->aGroupCat as $key => $cat) : ?>
								<?php if($cnt < 9 && $cat['name'] != '' && $cat['name'] != '...'){ ?>
									<input type="radio" value="<?=$cat['use_id'];?>" id="sub_cat_<?=$cat['use_id'];?>" name="category" class = "quick_selector"   />
									<label for="sub_cat_<?=$cat['use_id'];?>"><img src="http://50i50.org/time_icons/<?=$cat['icon'];?>" /> <br />
									<?=$cat['name'];?></label>
								<?php $cnt++ ; } ?>
							<?php endforeach; ?>
					</fieldset>
				    </div>
					<div data-content-theme="a" id = "quick_private_selection" data-theme="a" style = "display:block">
					<fieldset data-role="controlgroup" data-type="horizontal" data-theme="a" data-corners = "false"  >
							<?php $cnt = 1; ?>
							<?php 
							$objCat = array(10001 => array('use_id' => 10001,'name' => 'Event','icon' => 'PUBevent.png'),
											10019 => array('use_id' => 10019,'name' => 'Deal','icon' => 'PUBdeal.png'),
											10057 => array('use_id' => 10057,'name' => 'Personal','icon' => 'PUBperso.png'),
											10085 => array('use_id' => 10085,'name' => 'News','icon' => 'PUBnews.png'),
											10114 => array('use_id' => 10114,'name' => 'Place','icon' => 'PUBplace.png'),
											11300 => array('use_id' => 11300,'name' => 'Transport','icon' => 'PUBtrans.png'),
											11400 => array('use_id' => 11400,'name' => 'Alert','icon' => 'PUBalert.png'),
											11500 => array('use_id' => 11500,'name' => 'Must see','icon' => 'PUBmust.png')
										);
								 foreach ($objCat as $key => $cat) : ?>
								<?php if($cnt < 9 && $cat['name'] != '' && $cat['name'] != '...'){ ?>
									<input type="radio" value="<?=$cat['use_id'];?>" id="sub_cat_<?=$cat['use_id'];?>" name="category" class = "quick_selector" <?php echo $cat['name'] == 'Personal' ? 'checked' : '' ; ?> />
									<label for="sub_cat_<?=$cat['use_id'];?>"><img src="http://50i50.org/time_icons/<?php echo str_replace('PUB','PRI',$cat['icon']);?>" /> <br />
									<?=$cat['name'];?></label>
								<?php $cnt++ ; } ?>
							<?php endforeach; ?>
					</fieldset>
					</div>
					<br />
					<hr />
					<br />
					<a href="javascript:advancedCategoryToggle();" data-role="button" data-icon="arrow-r" data-iconpos="right" data-theme="a" id="show_advanced_category_toggle">Advanced Selection</a>
					<div id="cat_list" data-role="collapsible-set">
						<?php $objCat = new Category(); foreach ($objCat->aGroupCat as $key => $cat) : ?>
							<div class="cat_list" data-role="collapsible" data-theme="l" data-content-theme="j">
								<h3 data-cat_id="<?= $key; ?>" style="position: relative">
									<span
										class="mini_icon <?= str_replace('.png', '', $cat['icon']) ?>"><?=$cat['name'];?></span>
								</h3>

								<div class="sub_cat_list"></div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<!-- Category -->

				<!-- Date/Time -->
				<div data-role="collapsible" data-theme="a"
				     data-content-theme="c"  data-inset="false"
				   >
					<h3>Date/Time</h3>
					<h4>Now and during:</h4>
					<fieldset data-role="controlgroup" data-type="horizontal" id="type_time_box"
					          style="margin:0 auto; text-align:center;">
						<label for="now_during_0">Minute</label>
						<input id="now_during_0" name="now_during" value="minute" type="radio" checked="checked"/>
						<label for="now_during_1">Hour</label>
						<input id="now_during_1" name="now_during" value="hour" type="radio"/>
					</fieldset>

					<label for="time_slider" class="ui-hidden-accessible">Time Slider</label>
					<input type="range" data-theme="c" name="time_slider" id="time_slider" value="1" min="0" max="60"/>
					<br/>
					<hr/>
					<h4>Or enter date and time:</h4>

					<div data-role="fieldcontain">
						<label for="time_start">Starts</label>
						<input id="time_start" value="" type="text" placeholder="Date/Time"/>
					</div>
					<div data-role="fieldcontain">
						<label for="time_end">Ends</label>
						<input id="time_end" value="" type="text" placeholder="Date/Time"/>
					</div>
					<div data-role="fieldcontain">
						<label for="time_repeat">Repeat</label>
						<select name="time_repeat" data-inline="true" data-theme="a" id="time_repeat">
							<option value="0">Never</option>
							<option value="1">Every day</option>
							<option value="2">Every week</option>
							<option value="3">Every month</option>
							<option value="4">Every year</option>
						</select>
					</div>
				</div>
				<!-- Date/Time -->

				<!-- Location -->
				<div data-role="collapsible" data-theme="a"
				     data-content-theme="c" >
					<h3>Location</h3>
					<h4>Default location:</h4>
					<input name="loc_hear" id="loc_hear" class="custom" data-theme="a" type="checkbox" checked="checked" value=""/>
					<label data-inline="true" for="loc_hear">Here</label>
					<br/>
					<hr/>
					<h4>Or enter address:</h4>
					<label for="loc_address" class="ui-hidden-accessible">Address</label>
					<input name="loc_address" id="loc_address" value="" placeholder="Address" type="text">
					<h4>Or coordinates:</h4>
					<label for="loc_coordinates" class="ui-hidden-accessible">Coordinates</label>
					<input name="loc_coordinates" id="loc_coordinates" value="" placeholder="Coordinates (ex: -22.232,45.446)" type="text">
				</div>
				<!-- Location -->

				<input type="hidden" name="loc_default_time" id="loc_default_time"/>

				<input type="hidden" name="loc_start_time" id="loc_start_time"/>
				<input type="hidden" name="loc_end_time" id="loc_end_time"/>

				<input type="hidden" name="loc_custom_address" id="loc_custom_address"/>
				<input type="hidden" name="loc_custom_coordinate" id="loc_custom_coordinate"/>
				<input type="hidden" name="upload_file_link" id="upload_file_link"/>
				<input type="hidden" name="action" value="event_save"/>
				<input type="hidden" name="json" value="1"/>
			</div>
		</form>
	</div>
</div>
<style>
	#quick_public_selection .ui-controlgroup .ui-radio{
		width:50%;
		display:inline;
	}

	#quick_private_selection .ui-controlgroup .ui-radio{
		width:50%;
		display:inline;
	}
</style>
<script type = "text/javascript" >
			$( document ).on( "pageinit", function( event ) {
  				$("#cat_list").hide();

			});
			$( "#privacy" ).on( 'slidestop',function( event ) {
						var cat_type = $(this).val();
						if(cat_type == 'privacy'){
							$('#quick_public_selection').hide();
							$('#quick_private_selection').show();
						}else{
							$('#quick_private_selection').hide();
							$('#quick_public_selection').show();
						}
				});
			function advancedCategoryToggle(){
				$("#cat_list").slideToggle();
			}
</script>
</body>
>>>>>>> 3c36cad36a4b21d3dea4c0b46a03fff477bf229e
</html>