<?php //
require_once 'define.php';
require_once '../server/config.php';
require_once PATH_MOBILE . 'server/Table/DB.php';
require_once './server/Obj/Users.php';

$user_js_obj = '{}';
$type_privacy = array('public');

$user = Obj_Users::isLogged();
//if (empty($user)) {
//	$user = Obj_Users::LoginByID(11);
//}

if ($user) {
	$user_js_obj = urlencode(json_encode($user));
	$type_privacy[] = 'private';
}

$loc = $user['lang'];
$domain = 'mobile_index';
include ('../inc/language.php');

/* Get Cookie */
$cookie = isset($_COOKIE[MOBILE_COOKIE]) ? json_decode($_COOKIE[MOBILE_COOKIE], true) : array();


$cookie_name = 'splash_page_seen'; // name of cookie
// check if cookie exists ornot and redirects to proper page
if (!isset($_COOKIE[$cookie_name])) {

	$cookie_value = 'redirection'; // what we gonna store in cookie
	$cookie_expire = time() + (192 * 60 * 60); // expiration date of cookie 8 hours from now

	// create cookie and redirect
	setcookie($cookie_name, $cookie_value, $cookie_expire, '/');
	header("Location: splash.html");
	exit;
}


?>
<!DOCTYPE html>
<html>
	<head>
		<title><?=_("What's happening around you?")?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
		<meta name="apple-mobile-web-app-capable" content="yes"/>
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

		<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_MOBILE; ?>css/jquery.mobile.structure-1.3.1.css"/>
		<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_MOBILE; ?>css/themes/50i50.them.min.css"/>
		<link type="text/css" rel="stylesheet" media="screen" href="<?= PROTOCOL . URL_MOBILE; ?>css/main.css"/>

		<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.core.css"/>
		<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.jqm.css"/>
		<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/css/mobiscroll.animation.css"/>

		<link type="text/css" rel="stylesheet" href="<?= PROTOCOL . URL_MOBILE; ?>css/jqm-icon-pack-2.0-original.css"/>

		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/jquery.mobile-1.3.1.js"></script>
		<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>js/markerclusterer.2.0.6.js"></script>
		<script type="text/javascript" src="<?= PROTOCOL . URL_GEOKRONO; ?>js/infobox.js"></script>

		<script type="text/javascript" src="js/events.script.js"></script>
		<script type="text/javascript" src="js/map.script.js"></script>
		<script type="text/javascript" src="js/users.script.js"></script>

		<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.core.js"></script>
		<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.datetime.js"></script>
		<script type="text/javascript" src="<?= PROTOCOL . URL_MOBILE; ?>plugin/mobiscroll/js/mobiscroll.jqm.js"></script>

		<script type="text/javascript">
			var URL_MOBILE = '<?=PROTOCOL . URL_MOBILE;?>';
			var URL_PARENT = '<?=PROTOCOL . URL_PARENT;?>';
			var URL_GEOKRONO = '<?=PROTOCOL . URL_GEOKRONO;?>';
			var MOBILE_COOKIE = '<?=MOBILE_COOKIE;?>';
			// Hides mobile browser's address bar when page is done loading.
			window.addEventListener('load', function () {
				setTimeout(function () {
					window.scrollTo(0, 1);
				}, 1);
				  				
				/*if (document.cookie.indexOf("show_popup_on_entry") >= 0) {
  					$('#intro_popup').popup('open');
  				}
  				$('#popup-close-button').click(function() {
       				$( "#intro_popup" ).popup( "close" );
       				$( "#intro_popup" ).popup( "close" );
   				});
   				$('#im-ready-button').click(function() {
       				$( "#intro_popup" ).popup( "close" );
       				$( "#intro_popup" ).popup( "close" );
   				});	*/		
			}, false);
			

				
     			
 			
		</script>

		<script type="text/javascript" src="js/main.js"></script>
		<style type="text/css">
         .customBtn1 {
          height: 45px !important;
     	  width:47px !important;
 	      background:url(imgheader/discover.png);
         }
         .customBtn2 {
          height: 40px !important;
	      width:40px !important;
	      background:url(imgheader/past.png);
         }
         .customBtn3 {
          height: 40px !important;
	      width:40px !important;
	      background:url(imgheader/future.png);
         }
         .customBtn4 {
          height: 40px !important;
	      width:40px !important;
	      background:url(imgheader/account.png);
         }
         .customBtn5 {
          height: 40px !important;
	      width:40px !important;
	      background:url(imgheader/inform.png);
         }

         .ui-icon-myapp-close{
                background-image: url("imgheader/close.png");
                background-size: 21px 19px;
            }

            .ui-icon{
                background-color: transparent!important;
                width: 21px;
                height: 21px;
            }

            .ui-icon-myapp-question{
                background-image: url("imgheader/help.png");
                background-size: 18px 18px;
                width: 21px;
                height: 21px;
            }

            .ui-icon-shadow{
                box-shadow: 0 0px 0 transparent;
            }
         .event_content{
          color: white;
         }
      
        #event_description{
          background:rgba(0, 0, 0, 0.8);
		  color:#ffffff;
		  padding-left:0px;
		  padding-right:10px;
		 /* height:100%;	*/
        }
        #event_description-popup{
		 /*height:80%;	*/
		}	
		#event_description img{
		 width:120px;
		 height:120px;
		}
		.popupheader{
		 background:url(imgheader/headerpopup.jpg) repeat;
		 height:69px;
 		 font-size:18px;
		 font-weight:bold;
		 padding-top:10px;
		 padding-left:130px;
		 position:relative;
		 margin:0;
		 margin-top:10px;
		}
		.popupheader a{
		  color:#ffffff;
		  text-decoration:none;
		 }
		 .imagineheader{
			 position:absolute;
			 top:-20px;
			 left:5px;
		 }
		 .tabel{
		  margin-top:30px;
		  margin-bottom:20px;
		  font-size:16px;
     	  text-shadow:none;		  
		 }
		 .tabel tr{
		  height:20px;
		 }
		 
		.firstcell{
		 color:#00b0f0;
		 padding-left:10px;
		 padding-right:10px;
		}

       .customBtn6 {
         height: 40px !important;
	width: 45px !important;
	background:url(imgheader/close.png);
       }
       .customBtn7 {
         height: 40px !important;
	width: 45px !important;
	background:url(imgheader/close-details.png);
       }
       .ui-body-c{
         border: 0;
       }

       #header.ui-bar-a{
        border-top:1px solid #3F3F3F;
        background:linear-gradient(#3F3F3F, #030303) repeat scroll 0 0 #333333;
       }
	   
	   
#user_signin, #main_settings, #intro_popup, #message_error, #event_description, #user_signup, #user_event_list, #user_settings, #events_list_block, #list, #your_data_box, #account_block, #quest_delete_account, #purchase{
 background:rgba(0, 0, 0, 0.8);
 text-shadow:none;
 color:#ffffff;
 text-decoration:none;
}

	</style>
        
	</head>


<body style="overflow: hidden;">
	<div id="wrapper" data-role="page" style="overflow: hidden;">

		<input type="hidden" id="DEFAULT_MAP_LAT_LNG" value="<?=MAP_SET_LAT_LNG?>" />
		<input type="hidden" id="DEFAULT_MAP_ZOOM" value="<?=MAP_SET_ZOOM?>" />

		<input type="hidden" id="SEARCH_TIME_STEP" value="<?=SEARCH_SET_TIME_STEP;?>" />

		<input type="hidden" id="USER_INFO" value="<?=$user_js_obj?>" />

		<div id="message_error" data-role="popup" data-transition="flip" data-theme="g" data-history="false"></div>

		<div id="event_description" data-role="popup" data-overlay-theme="c" data-transition="flip" data-position-to="window" data-history="false">
			
            <a href="#" id="mybackbtn" data-rel="back" class="ui-btn-right ui-btn ui-btn-inline customBtn7 ui-btn-corner-all"></a>
			<div class="event_content"></div>
		</div>

 <script>		
$('#event_description').on('popupbeforeposition', function( event, ui ) {
//var tst=$("#event_description").children(0).attr('id').toLowerCase();
if (!$('#testpopup').length){
  var headerul,contentul,imaginea,anterior,outputul;
  outputul="";
  anterior="";
  contentul="";
 $("#event_description").children().each(function(index, element) {
     $(this).children().each(function(index, element) {
        var tagul=$(this).prop('tagName').toLowerCase();
		//alert(tagul);
		switch (tagul){
			case 'h3':
			 headerul=$(this).html();
			 anterior="";
			break;
			case 'span':
			 imaginea=$(this).html();
			 anterior="";
			break;
			case 'p':
			 var tmpstr=$(this).html(); 
			 var px=tmpstr.indexOf('</strong>');
			 var p1=tmpstr.substring(0,px+9);
			 p1=p1.substring(8,p1.length-10);
			 var p2=tmpstr.substring(px+9,tmpstr.length);
			 if (p1=="Description:"){
			   contentul=contentul+"<tr valign='top' align='left'><td class='firstcell' colspan='2'>"+p1+"</td></tr>";
			   contentul=contentul+"<tr valign='top' align='left'><td align='left' colspan='2'><div style='margin-left:30px'>"+p2+"</div></td></tr>";			   
			 }
			 else{
             contentul=contentul+"<tr valign='top' align='left'><td class='firstcell'>"+p1+"</td><td align='left' width='100%'>"+p2+"</td></tr>";
			 }
			break;
			case 'br':
			 if (anterior!="br"){contentul=contentul+"<tr valign='top' align='left'><td><br></td><td><br></td></tr>";anterior="br";}
			 else{anterior="";}
			break;
		}//end switch
    }); //this children
});//children each
contentul="<div class='tabel'><table cellspacing='0' cellpadding='0' border='0'>"+contentul+"</table><span id='testpopup'></span></div>";
contentul="<a href='#' id='mybackbtn' data-rel='back' class='ui-btn-right ui-btn ui-btn-inline customBtn7 ui-btn-corner-all'></a>"+"<div class='event_content'><div class='popupheader'>"+headerul+"<div class='imagineheader'>"+imaginea+"</div></div>"+contentul;
$("#event_description").html(contentul+"</div>");

$(".popupheader a").removeClass("ui-link");
}//if has no testpopup children
$("#event_description").css("height","auto");
});//popupbeforeposition

</script>   

		<div data-role="popup" data-position-to="window" id="intro_popup" data-overlay-theme="a" data-theme="a" style="max-width:100%;">
			<div data-role="header" data-theme="a" class="ui-corner-top ui-header ui-bar-a" role="banner">
				<h1 class="ui-title" role="heading" aria-level="1">Welcome</h1>
			</div>		
			<a href="#" data-rel="back" id="popup-close-button" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
			<div data-role="content" data-theme="g" class="ui-corner-bottom ui-content ui-body-d" role="main">
				<h1 class="ui-title">50i50 is now open for Santa Clara University</h1>
				<ul>
				<li>Some tips:</li>
					<li>Click arrows to discover past and future occurences.</li>
					<li>Change the time scale to 'day' for a global view.</li>
					<li>Search: movie, event, sport, news, alert, building, deal, etc.</li>
					<li>Register to inform what is happening around you.</li>	
				</ul>							
				<a href="#" data-rel="back" id="im-ready-button" data-role="button" style="position:relative;left:23%;" data-inline="true" data-transition="flow" data-theme="a" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" class="ui-btn ui-btn-up-b ui-shadow ui-btn-corner-all ui-btn-inline"><span class="ui-btn-inner"><span class="ui-btn-text">I'm Ready!</span></span></a>  
			</div>
		</div>   

	<!-- **** Content **** -->
		<div id="header" data-role="header">

			<a href="#main_settings" data-rel="popup" class="customBtn1 ui-btn ui-btn-inline"></a>

			<div id="current_date" style="width:140px;">
                <a id="step_back" href="#" class="customBtn2"></a>
				<h1><?=date('Y') . ' <br />' . date('M d') . ' <br />' . date('H:i');?></h1>
                <a id="step_forward" href="#" class="customBtn3"></a>
			</div>

			<div class="ui-btn-right">
				<a class="customBtn4 ui-btn ui-btn-inline" id="button_user_signin" href="#user_signin" data-inline="true"></a>
				<a class="customBtn5 ui-btn ui-btn-inline" data-inline="true" rel="external" id="button_go_inform" href="<?= PROTOCOL . URL_MOBILE; ?>inform.php"></a>
				<div class="clear_both"></div>
			</div>

			<div id="global_search">
				<form id="search_form" action="javascript:void(0);" method="post">
					<input name="s" placeholder="<?=_('Search (movie, news, event, etc.)')?>" type="search" />
				</form>
			</div>
		</div>

		<div class="mobile_logo"></div>
		<div class="google_plug"></div>
		<div data-role="content" id="map"></div>
		<div data-role="content" id="list" class="hidden" style="display: none;">
			<ul id="list_content" data-divider-theme="b" data-theme="a" data-mini="true"></ul>
		</div>

	<!-- **** Panels **** -->
	
		<!-- **** DISCOVER **** -->
		<div data-role="panel" data-display="overlay" id="main_settings">
			<ul data-role="listview" data-theme="a" data-icon="false">
				<li data-icon="myapp-close"><a href="#" data-rel="close"><?=_('Discover');?></a></li>
			</ul>

			<br />


			<div data-role="collapsible-set" data-theme="a" data-content-theme="a">

				<div class="text_center" data-role="collapsible" data-collapsed="false" data-collapsed-icon="false" data-inset="false">
					<h4><?=_('Center in Time-Space')?></h4>
					<p>
						<a id="time_now" href="#" data-theme="a" data-role="button" data-inline="true"><?=_('Now');?></a>
						<a id="hear_loc" href="#" data-theme="a" data-role="button" data-inline="true"><?=_('Here')?></a>
					</p>
					<form id="location_input" action=""><p><input type="text" placeholder="(<?=_('Go to');?>)"/></p></form>
					<form action=""><p><input type="text" placeholder="(<?=_('Jump in time');?>)" id="time_input"/></p></form>
				</div>

				<div class="text_center" data-role="collapsible" data-collapsed="false" data-collapsed-icon="false" data-inset="false">
					<h4><?=_('View');?></h4>
					<fieldset data-role="controlgroup" data-type="horizontal" id="view_box">
						<label for="view_0"><?=_('Map');?></label><input name="step" id="view_0" value="map" checked="checked" type="radio" />
						<label for="view_1"><?=_('List');?></label><input id="view_1" name="step" value="list" type="radio"/>
					</fieldset>
				</div>

				<div class="text_center" data-role="collapsible" data-collapsed="false" data-collapsed-icon="false" data-inset="false">
					<h4><?=_('Time scale');?></h4>
					<fieldset data-role="controlgroup" data-type="horizontal" id="type_step_box">
						<label for="type_step_0"><?=_('Hour');?></label><input id="type_step_0" name="step"<?=SEARCH_SET_TIME_STEP == 'hour' ? ' checked="checked"' : ''?> value="hour" type="radio" />
						<label for="type_step_1"><?=_('Day');?></label><input id="type_step_1" name="step"<?=SEARCH_SET_TIME_STEP == 'day' ? ' checked="checked"' : ''?> value="day" type="radio" />
					</fieldset>
				</div>
				<div class="text_center" data-role="collapsible" data-collapsed-icon="false" data-inset="false">
					<h4>Privacy</h4>
					<fieldset data-role="controlgroup" data-type="horizontal" id="type_privacy">
						 <label for="privacy_public"><?=_('Public');?>  </label><input id="privacy_public" name="privacy[]"<?=in_array('public', $type_privacy) ? ' checked="checked"' : ''?> value="public" type="checkbox" />
						 <label for="privacy_private"><?=_('Private');?>  </label><input id="privacy_private" name="privacy[]"<?=in_array('private', $type_privacy) ? ' checked="checked"' : ''?> value="private" type="checkbox" />
					</fieldset>
				</div>
			</div>

			<br/>

			<div style="margin: 20px">
				<a><?=_('Help & Feedback');?></a>
				<a href="help/slideshows/index.html" rel="external" data-role="button" data-theme="i" data-shadow="true" data-mini="true" data-inline="true" data-icon="myapp-question" data-iconpos="notext" data-iconshadow="true"><?=_('Help');?></a>
			</div>

		</div><!-- ACCOUNT SETTINGS -->
		<div data-role="panel" data-position="right" data-display="overlay" id="user_signin">
			<ul data-role="listview" data-theme="a" data-icon="false">
				<li data-icon="myapp-close"><a href="#" data-rel="close"><?=_('Login');?></a></li>
			</ul>

			<br/>

			<p><?=_('No account yet? <a href="#user_signup">Register here</a>');?></p>

			<hr/>

			<h4><?=_('Login with your 50i50 account.');?></h4>

			<div class="text_center" data-role="content" data-theme="a">
				<form id="login_form" class="userform" method="post" action="<?= PROTOCOL . URL_MOBILE; ?>server/users.php">
					<label for="email" style="text-align: left;"><?=_('Username or Email');?>:</label>
					<input placeholder="Username or Email" type="text" name="email" id="email"/>

					<label for="password" style="text-align: left;"><?=_('Password');?>:</label>
					<input placeholder="Password" type="password" name="password" id="password" autocomplete="off"/>

					<input type="submit" data-theme="g" data-inline="true" value="<?=_('Login');?>"/>

					<input type="hidden" name="action" value="sign_in"/>
				</form>
				<a id="button_forgot_your_pass" href="#"><?=_('Forgot your password?');?></a>
			</div>

			<div id="forgot_your_pass">
				<hr/>
				<div class="text_center" data-role="content" data-theme="a">
					<form class="userform" method="post" action="<?= PROTOCOL . URL_MOBILE; ?>server/users.php">
						<label for="forgot_pass" style="text-align: left;"><?=_('Enter Username or Email');?>:</label>
						<input placeholder="Username or Email" type="text" name="forgot_pass" id="forgot_pass"/>

						<input type="submit" data-theme="g" data-inline="true" value="Forgot"/>

						<input type="hidden" name="action" value="forgot_password"/>
					</form>
				</div>
			</div>
	<!-- <hr/><h4>Or use an existing account.</h4><div id="social_signin_block" data-role="content" data-theme="a" style="margin: 0 auto; text-align: center;"></div> -->

		</div><!-- LOGIN -->
		<div data-role="panel" data-position="right" data-display="overlay" id="user_signup">
			<ul data-role="listview" data-theme="a" data-icon="false">
				<li data-icon="delete"><a href="#" data-rel="close"><?=_('Registration');?></a></li>
			</ul>

			<br/>

			<p><a href="#user_signin"><?=_('Go to Login')?></a></p>

			<div class="text_center" data-role="content" data-theme="a">
				<form class="userform" method="post" action="<?= PROTOCOL . URL_MOBILE; ?>server/users.php">
					<label for="reg_username" style="text-align: left;"><?=_('Username');?>:</label>
					<input placeholder="Username" type="text" name="username" id="reg_username"/>

					<label for="reg_email" style="text-align: left;"><?=_('Email');?>:</label>
					<input placeholder="Email" type="text" name="email" id="reg_email"/>

					<label for="reg_password" style="text-align: left;"><?=_('Password');?>:</label>
					<input placeholder="Password" type="password" name="password" id="reg_password" autocomplete="off"/>

					<label for="reg_conf_password" style="text-align: left;"><?=_('Confirm Password');?>:</label>
					<input placeholder="Confirm Password" type="password" name="conf_password" id="reg_conf_password"
					       autocomplete="off"/>

					<input type="submit" data-inline="true" data-theme="g" value="<?=_('Sign Up')?>"/>

					<input type="hidden" name="action" value="sign_up"/>
				</form>
			</div>
		</div>
		<!-- YOUR INFORMATION -->
		<div data-role="panel" data-position="right" data-display="overlay" id="user_event_list">
			<ul id="your_data_box" data-role="listview" data-theme="a" data-icon="false">
				<li data-icon="myapp-close"><a href="#" data-rel="close"><?=_('Your Information');?></a></li>
			</ul>

			<div id="account_block" style="margin: 20px 0">
				<a href="#" id="button_your_account"><?=_('User');?></a>
				&nbsp;
				<a data-role="button" data-rel="back" data-inline="true" data-theme="i" data-mini="true" id="button_sign_out" href="#"><?=_('Sign Out');?></a>
			</div>

			<ul id="events_list_block" data-divider-theme="b" data-mini="true" style="height: 100%; overflow-y: scroll;" data-split-theme="f" data-split-icon="delete"></ul>

			<div data-role="popup" id="purchase" data-theme="a" class="ui-content" style="max-width:340px; padding-bottom:1em;padding-top:1em; float:left; margin-right:5px; margin:auto;">
				<h3><?=_('Delete Information?');?></h3>
				<br/>
				<a id="button_del_event" href="#" data-role="button" data-rel="back" data-theme="f" data-inline="true" data-mini="true"><?=_('Delete');?></a>
				<a href="#" data-role="button" data-rel="back" data-inline="true" data-mini="true" data-theme="c"><?=_('Cancel');?></a>
			</div>
		</div>
		<!-- MEMBER ACCOUNT SETTINGS -->
		<div data-role="panel" data-position="right" data-display="overlay" id="user_settings">
			<ul data-role="listview" data-theme="a" data-icon="false">
				<li data-icon="delete"><a href="#" data-rel="close"><?=_('Your Account');?></a></li>
			</ul>

			<br/>

			<div data-role="content" data-theme="a">
				<form class="userform" method="post" action="<?= PROTOCOL . URL_MOBILE; ?>server/users.php">
					<label for="edit_username"><?=_('Username');?>:</label>
					<input type="text" name="username" placeholder="(<?=_('username');?>)" id="edit_username"/>

					<label for="edit_email"><?=_('Email');?>:</label>
					<input type="text" name="email" placeholder="(<?=_('email');?>)" id="edit_email"/>

					<!-- LANGUAGE BUTTON to be implemented later
					<label for="edit_email"><?=_('Language');?>:</label>
					<select name="lang" data-mini="true" data-inline="true">
						<?php foreach($lang as $key => $name) : ?>
						<option value="<?=$key . (($key == $loc) ? '" selected="selected' : '');?>"><?=$name;?></option>
						<?php endforeach; ?>
					</select>
					END of language button!-->

					<label for="edit_old_password"><?=_('Current Password');?>:</label>
					<input type="password" autocomplete="off" name="old_password" placeholder="(<?=_('current password');?>)" id="edit_old_password"/>

					<label for="edit_password"><?=_('New Password');?>:</label>
					<input type="password" autocomplete="off" name="password" placeholder="(<?=_('new password');?>)" id="edit_password"/>

					<label for="edit_conf_password"><?=_('Confirm Password');?>:</label>
					<input type="password" autocomplete="off" name="conf_password" placeholder="(<?=_('confirm password');?>)" id="edit_conf_password"/>

					<br/>

					<input type="submit" data-theme="g" value="<?=_('Save Changes')?>"/>
					<input data-corners="true" type="hidden" name="action" value="user_info_edit"/>
				</form>
			</div>

			<div style="margin: 20px">
				<a><?=_('Delete account?');?></a>
				<a data-role="button" data-theme="k" data-inline="true" data-icon="alert" data-rel="popup" data-position-to="window" data-mini="true" data-transition="pop" href="#quest_delete_account" data-iconpos="notext"><?=_('Delete')?></a>
			</div>

			<div data-role="popup" id="quest_delete_account" data-theme="d" data-overlay-theme="b" class="ui-content" style="max-width:340px; padding-bottom:1em;">
				<h3><?=_('Are you sure you want to delete your account with all the information you have created?');?></h3>
				<a id="button_del_account" href="#" style="margin:30px" data-role="button" data-rel="back" data-theme="k" data-icon="check" data-inline="true"><?=_('Delete');?></a>
				<a href="#" data-role="button" data-rel="back" data-inline="true" data-theme="a"><?=_('Cancel');?></a>
			</div>
		</div><!-- MEMBER ACCOUNT SETTINGS -->

	</div>

	<!-- Google Analytics -->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-19934650-1']);
		_gaq.push(['_setDomainName', '50i50.org']);
		_gaq.push(['_setAllowLinker', true]);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;	
			ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

</body>
</html>