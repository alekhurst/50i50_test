<<<<<<< HEAD
function Map() {
	var GoogleMap,
		MarkerClusters,
		MarkerPosition,
// Bind Function
		bindFunc = {
		change_position: function () {

		},

			get_user_position: function (position) {
				if (!position) {
					console.log('Error');
				}
			}
		},
		map_window_coordinate = [],
		existaMarker=0,
		image = new google.maps.MarkerImage(
              'imgheader/bluedot.png',
              null, // size
              null, // origin
              new google.maps.Point( 8, 8 ), // anchor (move to center of marker)
              new google.maps.Size( 17, 17 ) // scaled size (required for Retina display icon)
            ),
			marker = new google.maps.Marker({
							flat: true,
							icon: image,
							optimized: false,
							title: 'You are here',				
				visible:false
				});


	function placeMarker(position) {
		if (MarkerClusters) {
//			MarkerPosition.setPosition(position);
		}

		console.log(URL_MOBILE + 'img/hear.png');

		/*var image = new google.maps.MarkerImage(
				 'img/here.png',
				new google.maps.Size(36, 41),
				new google.maps.Point(0, 0),
				new google.maps.Point(0, 41)
			),
			marker = new google.maps.Marker({
				position: position,
				icon: image,
				visible: true
			});*/
//			alert(existaMarker);
            

            if (existaMarker==0){
			 marker.setMap(GoogleMap);
			 marker.setVisible(true);
			 marker.setPosition(position);
			 existaMarker=1;
			}
			else{			
			  marker.setPosition(position);
			}
		events.loading(false);

}



	function setCenterMap(geometry) {
		if (!geometry) {
			return;
		}

		GoogleMap.setCenter(geometry.location);
		GoogleMap.fitBounds(geometry.viewport);
	}

	function showEvent(marker) {
		if (!marker.info.getMap()) {
			marker.info.open(GoogleMap, marker);
			return true;
		}

		if (marker.info.isHidden_) {
			marker.info.show();
			return true;
		}

		return false;
	}

	function hideEvent(marker) {
		marker.info.hide();
		active_marker = null;
	}

	function hideEvents(no_close_marker) {
		var markers = MarkerClusters.getMarkers();

		for (var i = 0; i < markers.length; i++) {
			if (markers[i] != no_close_marker && markers[i].info.getMap() && !markers[i].info.isHidden_) {
				markers[i].info.hide();
			}
		}

		active_marker = null;
	}

// Public
	this.active_event = 0;

	this.init = function (option) {
		var map_option = {
				center: this.getObjLatLng([option.lat, option.lng]),
				zoom: option.zoom,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			},
			minimumClusterSize = 14;

		GoogleMap = new google.maps.Map(document.getElementById('map'), map_option);

		MarkerPosition = new google.maps.Marker({
			map: GoogleMap
		});

		MarkerClusters = new MarkerClusterer(GoogleMap, [], {minimumClusterSize: minimumClusterSize});

		google.maps.event.addListener(GoogleMap, 'tilesloaded', function () {
			map_window_coordinate = GoogleMap.getBounds();

			bindFunc.change_position();
		});

		google.maps.event.addListener(MarkerClusters, "clusteringend", function (cluster) {
			map_window_coordinate = GoogleMap.getBounds();

			var clusters = cluster.getClusters();

			for (var j in clusters) {
				var cluster_marker = clusters[j].getMarkers();

				for (var i in cluster_marker) {
					if (cluster_marker[i].getMap()) {
						cluster_marker[i].setVisible(true);
					} else {
						cluster_marker[i].setVisible(false);
					}
				}
			}

		});

	};

	this.bind = function (func, handler) {
		function binder(func) {
			return function (a) {
				func.call(this, a);
				handler(a);

				return this;
			}
		}

		bindFunc[func] = binder(bindFunc[func]);
	};

	this.setPositionMarker = function () {
		placeMarker(GoogleMap.getCenter());
	};

	this.getUserLocation = function (set_marker) {
		var self = this;
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(
				function (position) {
					initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    GoogleMap.setCenter(initialLocation);
					
					position = [position.coords.latitude, position.coords.longitude];


					self.setLocByCoordinates(position);

					//if (set_marker) {

						self.setPositionMarker();
					//}
				},

				function (error) {
//					bindFunc.get_user_position(null);
				}
			);

		}
		return this;
	};

	this.getObjLatLng = function (position) {
		return new google.maps.LatLng(position[0], position[1]);
	};

	this.setLocByAddress = function (address) {
		var geo = new google.maps.Geocoder();

		geo.geocode({address: address}, function (res) {
			if (typeof res[0].geometry === "undefined") {
				return;
			}

			setCenterMap(res[0].geometry);
		});
	};

	this.setLocByCoordinates = function (lat_lng) {
		var geo = new google.maps.Geocoder();

		geo.geocode({location: this.getObjLatLng(lat_lng)}, function (res) {
			if (typeof res[0].geometry === "undefined") {
				return;
			}

			setCenterMap(res[0].geometry);
		});
	};

	this.getWindowLocation = function () {
		return [
			map_window_coordinate.getNorthEast().lat(),
			map_window_coordinate.getNorthEast().lng(),
			map_window_coordinate.getSouthWest().lat(),
			map_window_coordinate.getSouthWest().lng()
		];
	};

	this.resizeWindow = function () {
		google.maps.event.trigger(GoogleMap, "resize");
	};

// TODO: Markers Function go to this.init().
// Markers Function
	var active_marker = null;
	this.Markers = {
		setActiveMarker: function (val) {
			active_marker = val;
		},

		delActiveMarker: function () {
			active_marker = null;
		},

		create: function (box, position, icon, id) {
			var image = new google.maps.MarkerImage(
					URL_PARENT + 'time_icons/' + icon,
					new google.maps.Size(36, 41),
					new google.maps.Point(0, 0),
					new google.maps.Point(0, 41)
				),
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(position[0], position[1]),
					icon: image,
					visible: true
				});

			marker.event_id = id;

			marker.info = new InfoBox({
				content: box,
				disableAutoPan: true,
				maxWidth: 0,
				pixelOffset: new google.maps.Size(35, -35),
				zIndex: 2,
				closeBoxURL: "",
				infoBoxClearance: new google.maps.Size(1, 1),
				isHidden: false,
				pane: "floatPane",
				enableEventPropagation: true
			});

			if (id == active_marker) {
				BindMarker();
			}

			google.maps.event.addListener(marker, 'click', BindMarker);

			return marker;

			function BindMarker() {
				hideEvents(marker);

				if (showEvent(marker)) {
					active_marker = marker.event_id;
					return;
				}

				hideEvent(marker);
			}
		},

		add: function (marker) {
			MarkerClusters.addMarker(marker);
		},

		clear: function () {
			if (!MarkerClusters) {
				return;
			}

			MarkerClusters.clearMarkers();
		}
	};
}

var map_events = {
	search: null,

	load: function () {
		events.loading(true);
		var location = Map.getWindowLocation();

		var self = this;

		if (self.query) {
			self.query.abort();
		}

		var params = {
			count_events: true,
			type: events.step.name,
			pne: [location[0], location[1]],//obj_map.point_north_east,
			psw: [location[2], location[3]],//obj_map.point_south_west,
			search: this.search,
			iDisplayStart: 0,
			iDisplayLength: -1,
			privacy: [
				$('#privacy_public').is(':checked'),
				$('#privacy_private').is(':checked')
			]
		};

		self.query = $.ajax({
			url: URL_MOBILE + 'server/index.php?block=mobile-map&date=' + events.date.get_for_query(),
			data: params,
			success: function (response) {
				self.remove();
				self.place(response.events_array);

				events.loading(false);
				self.query = null;
			},
			error: function(){
				setTimeout(function() {
  				 events.loading(false);
                }, 1500);

			}
		});
	},

	place: function (events) {
		for (var i = 0, periods = events.length; i < periods; i++) {
			Map.Markers.add(this.template(events[i]));
		}
	},

	template: function (event) {
		var img = event.imagen.length ? event.imagen : '';
		var time = event.h_inicio ? event.h_inicio : '';
		var temporaryEventTitle = event.titulo;


		// Modify time for displaying
		var displayTime = time.substr(0,5);                                    // Take out the seconds
		var hoursUsedForCalculation = parseInt(displayTime.substr(0,2),10);    // 'Hours' portion of time to be manipulated into USA time
		var minutesUsedForCalculation = displayTime.substr(3,2);               // 'Minutes' portion of time used for am/pm string addition
		if(hoursUsedForCalculation <= 11) {                                    // This if grabs the am hours and takes off leading 0's
			if(hoursUsedForCalculation == 0)
				hoursUsedForCalculation = 12;
			hoursUsedForCalculation = hoursUsedForCalculation.toString();
			if(hoursUsedForCalculation.charAt(0) == 0)
				hoursUsedForCalculation = hoursUsedForCalculation.substr(1,1); // Take out first 0 (05:00 -> 5:00)
			minutesUsedForCalculation = minutesUsedForCalculation + 'am';
		}
		else if(hoursUsedForCalculation >= 13) {                               // This if grabs pm times except for 12 and converts them to USA time
			hoursUsedForCalculation = hoursUsedForCalculation - 12;
			hoursUsedForCalculation = hoursUsedForCalculation.toString();
			minutesUsedForCalculation = minutesUsedForCalculation + 'pm';
		}
		else{                                                                  // This else grabs anything at 12pm
			hoursUsedForCalculation = hoursUsedForCalculation.toString();
			minutesUsedForCalculation = minutesUsedForCalculation + 'pm';
		}
		displayTime = hoursUsedForCalculation + ':' + minutesUsedForCalculation;


		var el = '<div id="map_event_' + event.id + '" class="block">' +
			'<h1 class="title">' + img + temporaryEventTitle + '</h1>' +
			'<p class="date">' + event.f_inicio + ' ' + displayTime + '</p><div class="clear"></div></div>';

		return Map.Markers.create(el, [event.latitud, event.longitud], event.icon, event.id);
	},

	remove: function () {
		$('div.infoBox', '#map').remove();

		Map.Markers.clear();
	}
};




//	this.setPosition = function (address) {
//		var geo = new google.maps.Geocoder();
//
//		geo.geocode(address, function (res) {
//			var address = '';
//			var address_row = '';
//			if (!res) {
//				return;
//			}
//			for (var i = 0; i < res[0].address_components.length; i++) {
//				address_row = res[0].address_components[i];
//				for (var j = 0; j < address_row.types.length; j++) {
//					if (address_row.types[j] == 'locality') {
//						address += address_row.long_name + ', '
//					}
//					if (address_row.types[j] == 'administrative_area_level_1') {
//						address += address_row.long_name + ', '
//					}
//					if (address_row.types[j] == 'country') {
//						address += address_row.long_name
//					}
//				}
//			}
//
//			geo.geocode({address: address}, function (res) {
//				if (!res) {
//					return;
//				}
//
//				GoogleMap.setCenter(res[0].geometry.location);
//				GoogleMap.fitBounds(res[0].geometry.viewport);
//			});
//		});
//	};


var useragent = navigator.userAgent;
GoogleMap.style.width = mapCanvas.style.height = '100%';
if ( useragent.indexOf('iPhone') !== -1 || useragent.indexOf('Android') !== -1 ) {
	navigator.geolocation.watchPosition( 
	getUserLocation, 
	null, 
	{ 
 	 enableHighAccuracy: true, 
	 maximumAge: 30000, 
	 timeout: 27000 
	}
  );			
} else if ( navigator.geolocation ) {
 navigator.geolocation.getCurrentPosition( getUserLocation);
}
=======
function Map() {
	var GoogleMap,
		MarkerClusters,
		MarkerPosition,
// Bind Function
		bindFunc = {
		change_position: function () {

		},

			get_user_position: function (position) {
				if (!position) {
					console.log('Error');
				}
			}
		},
		map_window_coordinate = [],
		existaMarker=0,
		image = new google.maps.MarkerImage(
              'imgheader/bluedot.png',
              null, // size
              null, // origin
              new google.maps.Point( 8, 8 ), // anchor (move to center of marker)
              new google.maps.Size( 17, 17 ) // scaled size (required for Retina display icon)
            ),
			marker = new google.maps.Marker({
							flat: true,
							icon: image,
							optimized: false,
							title: 'You are here',				
				visible:false
				});


	function placeMarker(position) {
		if (MarkerClusters) {
//			MarkerPosition.setPosition(position);
		}

		console.log(URL_MOBILE + 'img/hear.png');

		/*var image = new google.maps.MarkerImage(
				 'img/here.png',
				new google.maps.Size(36, 41),
				new google.maps.Point(0, 0),
				new google.maps.Point(0, 41)
			),
			marker = new google.maps.Marker({
				position: position,
				icon: image,
				visible: true
			});*/
//			alert(existaMarker);
            

            if (existaMarker==0){
			 marker.setMap(GoogleMap);
			 marker.setVisible(true);
			 marker.setPosition(position);
			 existaMarker=1;
			}
			else{			
			  marker.setPosition(position);
			}
		events.loading(false);

}



	function setCenterMap(geometry) {
		if (!geometry) {
			return;
		}

		GoogleMap.setCenter(geometry.location);
		GoogleMap.fitBounds(geometry.viewport);
	}

	function showEvent(marker) {
		if (!marker.info.getMap()) {
			marker.info.open(GoogleMap, marker);
			return true;
		}

		if (marker.info.isHidden_) {
			marker.info.show();
			return true;
		}

		return false;
	}

	function hideEvent(marker) {
		marker.info.hide();
		active_marker = null;
	}

	function hideEvents(no_close_marker) {
		var markers = MarkerClusters.getMarkers();

		for (var i = 0; i < markers.length; i++) {
			if (markers[i] != no_close_marker && markers[i].info.getMap() && !markers[i].info.isHidden_) {
				markers[i].info.hide();
			}
		}

		active_marker = null;
	}

// Public
	this.active_event = 0;

	this.init = function (option) {
		var map_option = {
				center: this.getObjLatLng([option.lat, option.lng]),
				zoom: option.zoom,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			},
			minimumClusterSize = 14;

		GoogleMap = new google.maps.Map(document.getElementById('map'), map_option);

		MarkerPosition = new google.maps.Marker({
			map: GoogleMap
		});

		MarkerClusters = new MarkerClusterer(GoogleMap, [], {minimumClusterSize: minimumClusterSize});

		google.maps.event.addListener(GoogleMap, 'tilesloaded', function () {
			map_window_coordinate = GoogleMap.getBounds();

			bindFunc.change_position();
		});

		google.maps.event.addListener(MarkerClusters, "clusteringend", function (cluster) {
			map_window_coordinate = GoogleMap.getBounds();

			var clusters = cluster.getClusters();

			for (var j in clusters) {
				var cluster_marker = clusters[j].getMarkers();

				for (var i in cluster_marker) {
					if (cluster_marker[i].getMap()) {
						cluster_marker[i].setVisible(true);
					} else {
						cluster_marker[i].setVisible(false);
					}
				}
			}

		});

	};

	this.bind = function (func, handler) {
		function binder(func) {
			return function (a) {
				func.call(this, a);
				handler(a);

				return this;
			}
		}

		bindFunc[func] = binder(bindFunc[func]);
	};

	this.setPositionMarker = function () {
		placeMarker(GoogleMap.getCenter());
	};

	this.getUserLocation = function (set_marker) {
		var self = this;
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(
				function (position) {
					initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    GoogleMap.setCenter(initialLocation);
					
					position = [position.coords.latitude, position.coords.longitude];


					self.setLocByCoordinates(position);

					//if (set_marker) {

						self.setPositionMarker();
					//}
				},

				function (error) {
//					bindFunc.get_user_position(null);
				}
			);

		}
		return this;
	};

	this.getObjLatLng = function (position) {
		return new google.maps.LatLng(position[0], position[1]);
	};

	this.setLocByAddress = function (address) {
		var geo = new google.maps.Geocoder();

		geo.geocode({address: address}, function (res) {
			if (typeof res[0].geometry === "undefined") {
				return;
			}

			setCenterMap(res[0].geometry);
		});
	};

	this.setLocByCoordinates = function (lat_lng) {
		var geo = new google.maps.Geocoder();

		geo.geocode({location: this.getObjLatLng(lat_lng)}, function (res) {
			if (typeof res[0].geometry === "undefined") {
				return;
			}

			setCenterMap(res[0].geometry);
		});
	};

	this.getWindowLocation = function () {
		return [
			map_window_coordinate.getNorthEast().lat(),
			map_window_coordinate.getNorthEast().lng(),
			map_window_coordinate.getSouthWest().lat(),
			map_window_coordinate.getSouthWest().lng()
		];
	};

	this.resizeWindow = function () {
		google.maps.event.trigger(GoogleMap, "resize");
	};

// TODO: Markers Function go to this.init().
// Markers Function
	var active_marker = null;
	this.Markers = {
		setActiveMarker: function (val) {
			active_marker = val;
		},

		delActiveMarker: function () {
			active_marker = null;
		},

		create: function (box, position, icon, id) {
			var image = new google.maps.MarkerImage(
					URL_PARENT + 'time_icons/' + icon,
					new google.maps.Size(36, 41),
					new google.maps.Point(0, 0),
					new google.maps.Point(0, 41)
				),
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(position[0], position[1]),
					icon: image,
					visible: true
				});

			marker.event_id = id;

			marker.info = new InfoBox({
				content: box,
				disableAutoPan: true,
				maxWidth: 0,
				pixelOffset: new google.maps.Size(35, -35),
				zIndex: 2,
				closeBoxURL: "",
				infoBoxClearance: new google.maps.Size(1, 1),
				isHidden: false,
				pane: "floatPane",
				enableEventPropagation: true
			});

			if (id == active_marker) {
				BindMarker();
			}

			google.maps.event.addListener(marker, 'click', BindMarker);

			return marker;

			function BindMarker() {
				hideEvents(marker);

				if (showEvent(marker)) {
					active_marker = marker.event_id;
					return;
				}

				hideEvent(marker);
			}
		},

		add: function (marker) {
			MarkerClusters.addMarker(marker);
		},

		clear: function () {
			if (!MarkerClusters) {
				return;
			}

			MarkerClusters.clearMarkers();
		}
	};
}

var map_events = {
	search: null,

	load: function () {
		events.loading(true);
		var location = Map.getWindowLocation();

		var self = this;

		if (self.query) {
			self.query.abort();
		}

		var params = {
			count_events: true,
			type: events.step.name,
			pne: [location[0], location[1]],//obj_map.point_north_east,
			psw: [location[2], location[3]],//obj_map.point_south_west,
			search: this.search,
			iDisplayStart: 0,
			iDisplayLength: -1,
			privacy: [
				$('#privacy_public').is(':checked'),
				$('#privacy_private').is(':checked')
			]
		};

		self.query = $.ajax({
			url: URL_MOBILE + 'server/index.php?block=mobile-map&date=' + events.date.get_for_query(),
			data: params,
			success: function (response) {
				events.loading(false);
				self.remove();
				self.place(response.events_array);
				self.query = null;
			},
			error: function(){
				setTimeout(function() {
  				 events.loading(false);
                }, 1500);

			}
		});
	},

	place: function (events) {
		for (var i = 0, periods = events.length; i < periods; i++) {
			Map.Markers.add(this.template(events[i]));
		}
	},

	template: function (event) {
		var img = event.imagen.length ? event.imagen : '';
		var time = event.h_inicio ? event.h_inicio : '';
		var temporaryEventTitle = event.titulo;


		// Modify time for displaying
		var displayTime = time.substr(0,5);                                    // Take out the seconds
		var hoursUsedForCalculation = parseInt(displayTime.substr(0,2),10);    // 'Hours' portion of time to be manipulated into USA time
		var minutesUsedForCalculation = displayTime.substr(3,2);               // 'Minutes' portion of time used for am/pm string addition
		if(hoursUsedForCalculation <= 11) {                                    // This if grabs the am hours and takes off leading 0's
			if(hoursUsedForCalculation == 0)
				hoursUsedForCalculation = 12;
			hoursUsedForCalculation = hoursUsedForCalculation.toString();
			if(hoursUsedForCalculation.charAt(0) == 0)
				hoursUsedForCalculation = hoursUsedForCalculation.substr(1,1); // Take out first 0 (05:00 -> 5:00)
			minutesUsedForCalculation = minutesUsedForCalculation + 'am';
		}
		else if(hoursUsedForCalculation >= 13) {                               // This if grabs pm times except for 12 and converts them to USA time
			hoursUsedForCalculation = hoursUsedForCalculation - 12;
			hoursUsedForCalculation = hoursUsedForCalculation.toString();
			minutesUsedForCalculation = minutesUsedForCalculation + 'pm';
		}
		else{                                                                  // This else grabs anything at 12pm
			hoursUsedForCalculation = hoursUsedForCalculation.toString();
			minutesUsedForCalculation = minutesUsedForCalculation + 'pm';
		}
		displayTime = hoursUsedForCalculation + ':' + minutesUsedForCalculation;


		var el = '<div id="map_event_' + event.id + '" class="block">' +
			'<h1 class="title">' + img + temporaryEventTitle + '</h1>' +
			'<p class="date">' + event.f_inicio + ' ' + displayTime + '</p><div class="clear"></div></div>';

		return Map.Markers.create(el, [event.latitud, event.longitud], event.icon, event.id);
	},

	remove: function () {
		$('div.infoBox', '#map').remove();

		Map.Markers.clear();
	}
};




//	this.setPosition = function (address) {
//		var geo = new google.maps.Geocoder();
//
//		geo.geocode(address, function (res) {
//			var address = '';
//			var address_row = '';
//			if (!res) {
//				return;
//			}
//			for (var i = 0; i < res[0].address_components.length; i++) {
//				address_row = res[0].address_components[i];
//				for (var j = 0; j < address_row.types.length; j++) {
//					if (address_row.types[j] == 'locality') {
//						address += address_row.long_name + ', '
//					}
//					if (address_row.types[j] == 'administrative_area_level_1') {
//						address += address_row.long_name + ', '
//					}
//					if (address_row.types[j] == 'country') {
//						address += address_row.long_name
//					}
//				}
//			}
//
//			geo.geocode({address: address}, function (res) {
//				if (!res) {
//					return;
//				}
//
//				GoogleMap.setCenter(res[0].geometry.location);
//				GoogleMap.fitBounds(res[0].geometry.viewport);
//			});
//		});
//	};


var useragent = navigator.userAgent;
GoogleMap.style.width = mapCanvas.style.height = '100%';
if ( useragent.indexOf('iPhone') !== -1 || useragent.indexOf('Android') !== -1 ) {
	navigator.geolocation.watchPosition( 
	getUserLocation, 
	null, 
	{ 
 	 enableHighAccuracy: true, 
	 maximumAge: 30000, 
	 timeout: 27000 
	}
  );			
} else if ( navigator.geolocation ) {
 navigator.geolocation.getCurrentPosition( getUserLocation);
}
>>>>>>> 3bf313abecd4c3484d2bcfa6a19c63c50d04fb88
