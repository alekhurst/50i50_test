var obj_users = {
	user: {},
	init: function (user) {
		if (typeof(user.id) != 'undefined') {
			this.user_sign_in(user);
		} else {
			this.user_sign_out();
		}
		this.bind_events();
	},

	bind_events: function () {
		var self = this;

		$('#user_signup form, #forgot_your_pass form, #login_form').submit(function () {
			var param = $(this).serialize(),
				url = $(this).attr('action'),
				callback = $(this.action).val();

			self.query(url, param, callback);
			return false;
		});

		$('#user_settings form').submit(function () {
			if (!obj_users.user_is()) {
				events.error_message('You must register/login to view your own data <a href="#user_signin">login</a>');
				return false;
			}

			var param = $(this).serialize(),
				url = $(this).attr('action'),
				callback = $(this.action).val(),
				old_pass = $(this).find('#edit_old_password').val(),
				pass = $(this).find('#edit_password').val(),
				conf_pass = $(this).find('#edit_conf_password').val();

			if ((pass || conf_pass) && !old_pass) {

				events.error_message('Enter old password!');
				return false;
			} else if (old_pass && ((pass != conf_pass) || pass == '')) {

				events.error_message('Enter correct new password!');
				return false;
			}

			self.query(url, param, callback);
			return false;
		});

		$('#button_your_account').bind('vclick click', function () {
			var params = { action: 'user_info_get' },
				url = URL_MOBILE + 'server/users.php';

			$('#user_settings').panel('open');
			self.query(url, params, params.action);

			return false;
		});

		$('#button_go_inform').bind('vclick click', function () {
			if (self.user_is()) {
				return true;
			}

			events.error_message('You must register/login to inform. <a href="#user_signin">login</a>');
			return false;

		});

		$('#button_user_signin').bind('vclick click', function () {
			if (self.user_is()) {

				var params = {action: 'events_list'},
					url = URL_MOBILE + 'server/users.php';

				$("#user_event_list").panel("open");
				events.loading(true);
				self.query(url, params, params.action);

				return false;
			}

			return true;
		});

		$('#button_sign_out').bind('vclick click', function () {
			var params = { action: 'sign_out' },
				url = URL_MOBILE + 'server/users.php';

			self.query(url, params, params.action);

			return false;
		});

		$('#button_del_account').bind('vclick', function () {
			var params = { action: 'user_info_del' },
				url = URL_MOBILE + 'server/users.php';

			self.query(url, params, params.action);
		});



		return self;
	},

	bind_sign_in_social: function (eventObj) {
		var params = {
				uid: eventObj.UID,
				provider_uid: eventObj.loginProviderUID,
				provider: eventObj.provider,
				eventName: eventObj.eventName,
				timestamp: eventObj.timestamp,

				user_name: eventObj.user.nickname,
				user_email: eventObj.user.email,

				action: 'sign_in_social'
			},
			url = URL_MOBILE + 'server/users.php';

		alert(url);
		obj_users.query(url, params, params.action);
	},

	bind_sign_out_social: function (eventObj) {
		this.user_sign_out();
	},




	user_sign_view:function (user_name, theme, link) {
		console.log(user_name, theme, link);

		$('#button_user_signin').attr('href', link);
		if (theme=='a' || theme=='A'){
			$('#button_user_signin').css('background','url(imgheader/account.png)');
			$('#button_go_inform').css('background','url(imgheader/inform.png)');
		}
		else{
			$('#button_user_signin').css('background','url(imgheader/account2.png)');
			$('#button_go_inform').css('background','url(imgheader/inform2.png)');	
		}

		$('#button_your_account').text(user_name);

		return this;
	},

	user_sign_in: function (user) {
		this.user = user;
		return this.user_sign_view(user.username, 'b', '#user_event_list');
	},

	user_is: function () {
		if (typeof this.user['id'] == 'undefined') {
//			this.user_sign_out();
			return false;
		}

		return  true;
	},

	user_sign_out: function () {
		this.user = {};

		var form = '#user_settings form';

		$('#edit_username, #edit_email', form).val('');

		return this.user_sign_view('', 'a', '#user_signin');
	},



	is_user: function () {
		return this.user['id'] ? true : false;
	},

	user_signin_view: function () {
		var user = '',
			theme = 'a',
			link = '#user_signin';

		if (this.is_user()) {
			user = this.user.username;
			theme = 'b';
			link = '#user_event_list';
		}

		$('#button_user_signin').attr('href', link);
		if (theme=='a' || theme=='A'){
			$('#button_user_signin').css('background','url(imgheader/account.png)');
			$('#button_go_inform').css('background','url(imgheader/inform.png)');
		}
		else{
			$('#button_user_signin').css('background','url(imgheader/account2.png)');
			$('#button_go_inform').css('background','url(imgheader/inform2.png)');	
		}
		$('#button_your_account').text(user);

		return this;
	},


	query: function (url, params, callback) {
		events.loading(true);

		var self = this;
		$.ajax({
			url: url + '?json=1',
			type: 'post',
			data: params,
			dataType: 'json',
			success: function (resp) {
				events.loading(false);

				if (resp.info) {
					console.log(resp.info);
					events.error_message(resp.info);
				}

				self.callback[callback](resp)
			}
		});

	},

	callback: {
		sign_up: function (resp) {
			if (!resp.error) {
				$("#user_signin").panel("open");
			}
		},

		sign_in: function (resp) {
			if (!resp.error) {
				var params = {action: 'events_list'},
					url = URL_MOBILE + 'server/users.php';

				obj_users
					.user_sign_in(resp.user)
					.query(url, params, params.action);

				$("#user_event_list").panel("open");


			} else {
				obj_users.user_sign_out();
			}
		},

		sign_out: function () {
			obj_users.user_sign_out();
			$('#user_event_list').panel('close');
		},

		forgot_password: function (resp) {
			if (!resp.error) {
				$('#forgot_your_pass').slideUp()
					.find('#forgot_pass').val('');
			}
		},

		user_info_edit: function (resp) {
			if (!resp.error) {
				obj_users.user_sign_in(resp.user);
			}
		},
		user_info_get: function (resp) {
			if (resp.error) {
				obj_users.user_sign_out();
				events.error_message(resp.info);
				$('#user_event_list').panel('close');
			} else {
				var form = '#user_settings form';

				$('#edit_username', form).val(resp.user.username);
				$('#edit_email', form).val(resp.user.email);

				$('#user_settings').panel('open');
			}
		},

		events_list: function (resp) {
			if (resp.error) {
				obj_users.user_sign_out();
				$('#user_event_list').panel('close');
				return false
			}

			$('#events_list_block').html(resp.content).listview({
				autodividers: true,
				filter: true,
				icon: 'delete',
				autodividersSelector: function ( li ) {
					var out = li.find('.date_sort').html();
					return out;
				},
				filterCallback: function (text, searchValue, item) {
					return text.toString().toLowerCase().indexOf(searchValue) === -1;
				}

			}).listview('refresh');
			var acc_h = $('#account_block').height();
			var filter_h = $('#user_event_list div.ui-panel-inner .ui-listview-filter').height();

			$('#events_list_block').height($(window).height() - $('#your_data_box').height() - acc_h - filter_h + 30);

			$(window).resize();
			events.loading(false);

			return true;
		},

		user_info_del: function (resp) {
			if (!resp.error) {
				obj_users.user_sign_out();
				$('#events_list_block li').remove();
				$('#user_settings').panel('close');
			}
		}

	}
};