$(document).on("pageinit", function () {
	$("#go_video_preview")
		.find('iframe').attr({width: 0, height: 0})
		.end().on({
			popupafterclose: function () {

				var data_cookie = getCookie(MOBILE_COOKIE),
					PREVIEW_VIDEO_VERSION = $('#PREVIEW_VIDEO_VERSION').val();

				$(this).find('iframe').attr({width: 0, height: 0});

				if (data_cookie) {
					data_cookie = $.parseJSON(data_cookie);
					data_cookie.VIDEO_PREVIEW = PREVIEW_VIDEO_VERSION;
				} else {
					data_cookie = {VIDEO_PREVIEW: PREVIEW_VIDEO_VERSION};
				}

				setCookie(MOBILE_COOKIE, JSON.stringify(data_cookie), 360);

				$('#its_beta').popup('open', {
					history: false
				});
			},

			popupbeforeposition: function () {
				var size = [$(window).width(), $(window).height()];
				$(this).find('iframe').width(size[0] - 100).height(size[1] - 50);
			}
		});
});

$(window).load(function () {
	$("#go_video_preview").popup('open', {
		history: false
	});
});