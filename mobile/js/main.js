$.ajaxSetup({
	url: URL_MOBILE + '/server/index.php',
	type: 'post',
	dataType: 'json',
	error: function () {
		console.log('Stop Work!');
	}
});

var Map = new Map;
function changeDateTime(el) {
	var date = $(el.target).mobiscroll('getDate');

	events.set_date(date, 'obj')
		.show_date()
		.go();
}

$(document)
	.ready(function () {
		var coordinate = $('#DEFAULT_MAP_LAT_LNG').val().split(';'),
			zoom = $('#DEFAULT_MAP_ZOOM').val(),
			step = $('#SEARCH_TIME_STEP').val(),
			user = $.parseJSON(decodeURIComponent($('#USER_INFO').val()));

		events.init(step);

		Map.bind('get_user_position', function (position) {
			Map.setLocByCoordinates(position || [37.69065350120003, -122.18050947109373])
		});

		Map.bind('change_position', function () {
			events.loading(true).go();
		});

		Map.getUserLocation();

		Map.init({
			lat: parseFloat(coordinate[0]),
			lng: parseFloat(coordinate[1]),
			zoom: +zoom
		});

		resize_window();

		obj_users.init(user);


		$('#events_list_block').on('click', '.ui-link-inherit', function () {
			var event_data = $(this).data();


			var arr_date = event_data.date.split(','),
				obj_date = new Date(arr_date[0],arr_date[1]-1,arr_date[2],arr_date[3],arr_date[4],arr_date[5]);
			events.set_date(obj_date, 'obj').show_date();

			Map.setLocByCoordinates([+event_data.ln, +event_data.lg]);

			events.viewEvent($(this).parents('li').data('id')).go();

			return false;
		});

		$('#location_input').submit(function () {
			var text = $(this).find('input').val().trim();
			if (text) {
				Map.setLocByAddress(text);
			}

			return false;
		});

		$('#time_input').change(changeDateTime).mobiscroll().datetime({
			theme: 'jqm',
			display: 'modal',
			animate: 'slidevertical',
			mode: 'scroller'
		});

// bind BUTTON
		$('#search_form').submit(function () {
			var s = $(this).find('input').val();

			events
				.loading(true)
				.search(s)
				.go();

			return false;
		});
	})
	.on('vclick', '#hear_loc', function () {
		events.loading(true);
		Map.getUserLocation(true);
//		obj_map.default_coordinates();

	})
	.on('vclick', '#time_now', function () {
		events
			.loading(true)
			.set_date(new Date(), 'obj')
			.show_date()
			.go();

	})
	.on('vclick', '#step_back', function () {
		events
			.loading(true)
			.step_back()
			.go();

	})
	.on('vclick', '#step_forward', function () {
		events
			.loading(true)
			.step_forward()
			.go();

	})
	.on('change', '#type_step_box input', function () {
		var step_id = $(this).val();

		events
			.loading(true)
			.set_step(step_id)
			.show_date()
			.go();

	})
	.on('change', '#view_box input', function () {
		events.type_view = $(this).val();

		if (events.type_view == 'map') {
			$('#list').hide();
			$('#map').show();
		} else {
			$('#map').hide();
			$('#list').show();
		}

		resize_window();
		events
			.loading(true)
			.go();
	})
	.on('vclick', '#search_form a', function () {
		events
			.loading(true)
			.search(null)
			.go();
	})
	.on('vclick', '#map .infoBox .block', function () {
		var event_id = $(this).attr('id').replace('map_event_', '');

		events
			.loading(true)
			.view_event(event_id);
	})
	.on('change', '#type_privacy input', function () {

		if ($('#privacy_private').is(':checked') == true && !obj_users.is_user()) {
			$('#privacy_private').removeAttr('checked').checkboxradio('refresh');
			events.error_message('You must register/login to view your own data <a href="#user_signin">login</a>');
			return false;
		}

		events
			.loading(true)
			.go();

		return true;
	})
	.on('vclick', '#b_add_event', function () {
		events.loading(true);
//		obj_map.def_coord_add_event();

	})
	.on('vclick', '#button_forgot_your_pass', function () {
		var forgot_block = $('#forgot_your_pass');

		if (forgot_block.is(':visible')) {
			forgot_block.slideUp();
		} else {
			forgot_block.slideDown();
		}

		return false;
	})
	.on('vclick', '#events_list_block .delete_event', function () {
		events.event_del = $(this).parent().data('id');
		return true;
	})
	.on('vclick', '#button_del_event', function () {
		var params = {event: events.event_del, action: 'event_del'};

		$.ajax({
			url: URL_MOBILE + 'server/users.php?json=1',
			data: params,
			success: function (resp) {
				events.error_message(resp.info);

				if (!resp.error) {
					$('#ev_' + params.event, '#events_list_block').remove();
					$('#events_list_block').listview('refresh');
				}
			}
		});

		events.del_event = 0;
		return true;
	});


function resize_window() {
	var height = $(window).height(),
		top_menu = $('#header').height();

	$('#map').height(height - top_menu - 2);
	$('#list').height(height - top_menu - 32);

//	google.maps.event.trigger(obj_map.map, "resize");
	Map.resizeWindow();
}

$(window).resize(function () {
	resize_window();
});


function setCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 86400000)); // 24 * 60 * 60 * 1000
		var expires = "; expires=" + date.toGMTString();
	} else {
		var expires = "";
	}

	document.cookie = encodeURI(name) + "=" + encodeURI(value) + expires + "; path=/";
}

function getCookie(name) {
	var nameEQ = encodeURI(name) + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
	}
	return null;
}

function delCookie(name) {
	setCookie(name, "", -1);
}