var obj_date = {
		date: new Date(),

		months_long: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

		months_short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],

		week_days_long: [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

		week_days_short: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ],

		set_time: function (date) {
			this.date.setTime(date);

			return this;
		},

		set_obj: function (date) {
			this.date = date;

			return this;
		},

		get: function () {
			return this.date;
		},

		get_week_day_long: function (id) {
			return this.week_days_long[id];
		},

		view_week_day: function () {
			var ziuaSaptamana =[ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
			return ziuaSaptamana[this.date.getDay()];
		},

		view_year: function () {
			return this.date.getFullYear();
		},

		view_month: function () {
			return this.months_short[this.date.getMonth()];
		},

		view_day: function () {
			var day = this.date.getDate();

			return (day < 10 ? '0' : '') + day;
		},

		view_hour: function () {
			var hour = this.date.getHours();

			return (hour < 10 ? '0' : '') + hour;
		},
		
		//FUNCTIONS FOR OVERHEAD BAR DISPLAY
		
		view_hour2: function () {
			var hour = this.date.getHours();
			
			if (hour >= 12) {
				hour = hour-12;
			}
			if (hour == 0) {
				hour = 12;
			}

			return hour;
		},

		view_ampm: function () {
			var hour = this.date.getHours();
			
			var dd = "am";
			if (hour >= 12) {
				dd = "pm";
			}

			return dd;
		},
		
		//FUNCTIONS FOR OVERHEAD BAR DISPLAY

		view_minutes: function () {
			var minutes = this.date.getMinutes();

			return (minutes < 10 ? '0' : '') + minutes;
		},

		get_for_query: function () {
			var date = [
				this.date.getFullYear(),
				this.date.getMonth() + 1,
				this.date.getDate(),
				this.date.getHours(),
				this.date.getMinutes(),
				this.date.getSeconds(),
				this.date.getTimezoneOffset()/60
			];

			return date.join(',');
		}
	},

	obj_step = {
		day: {
			name: 'day',

			next: function (date) {
				if (date) {
					return new Date(date.setDate(date.getDate() + 1));
				}

				return date;
			},

			prev: function (date) {
				if (date) {
					return new Date(date.setDate(date.getDate() - 1));
				}

				return date;
			}
		},

		hour: {
			name: 'hour',

			next: function (date) {
				if (date) {
					return new Date(date.setHours(date.getHours() + 1));
				}

				return date;
			},

			prev: function (date) {
				if (date) {
					return new Date(date.setHours(date.getHours() - 1));
				}

				return date;
			}
		}
	},

	list_events = {
		load: function () {
			var location = Map.getWindowLocation();

			var self = this;

			if (self.query) {
				self.query.abort();
			}

			var params = {
				count_events: true,
				type: events.step.name,
				pne: [location[0], location[1]],//obj_map.point_north_east,
				psw: [location[2], location[3]],//obj_map.point_south_west,
				search: this.search,
				iDisplayStart: 0,
				iDisplayLength: -1,
				privacy: [
					$('#privacy_public').is(':checked'),
					$('#privacy_private').is(':checked')
				]
			};

			$('#list_content').load(URL_MOBILE + 'server/index.php?block=mobile-list&date=' + events.date.get_for_query(), params, function () {
				events.loading(false);
				$(this).listview({
					autodividers: true,
					filter: true,
					icon: 'delete',
					autodividersSelector: function ( li ) {
						var out = li.find('.date_sort').html();
						return out;
					},
					filterCallback: function (text, searchValue, item) {
						return text.toString().toLowerCase().indexOf(searchValue) === -1;
					}

				}).listview('refresh');
			});


		},
		remove: function () {

		}

	},

	events = {
		date: {},
		step: {},
		query: {},
		type_view: 'map',

		init: function (step) {
			var self = this;

			self.set_date(new Date(), 'obj')
				.set_step(step)
				.show_date();

		},

		viewEvent: function (id) {
			Map.Markers.setActiveMarker(id);

			return this;
		},

		go: function () {
			if (this.type_view == 'map') {
				map_events.load();
			} else {
				list_events.load();
			}


			return this;
		},

		loading: function (status) {
			status = status ? 'show' : 'hide';

			$.mobile.loading(status, {
				text: "",
				textVisible: true,
				theme: "z",
				html: ""
			});

			return this;
		},

		set_date: function (date, format) {
			if (format == 'obj') {
				this.date = obj_date.set_obj(date);
			} else {
				this.date = obj_date.set_time(date);
			}

			return this;
		},

		set_step: function (step_id) {
			this.step = obj_step[step_id];

			return this;
		},

		set_map_settings: function () {

			return this;
		},

		step_forward: function () {
			var date = this.step.next(this.date.get());

			return this
				.set_date(date, 'obj')
				.show_date();
		},

		step_back: function () {
			var date = this.step.prev(this.date.get());

			return this
				.set_date(date, 'obj')
				.show_date();
		},

		//TIME DISPLAY ON OVERHEAD BAR
		
		show_date: function () {
			var self = this,
				date = self.date.view_week_day() + '<br />'
					+ self.date.view_month() + ' ' + self.date.view_day() + '<br />',
				line_h = 2;

			if (this.step.name == 'hour') {
				date += self.date.view_hour2() + self.date.view_ampm();
				line_h++;
			}

			$('#current_date h1').css('line-height', 33/line_h + 'px').html(date);

			return self;
		},
		
		//END TIME DISPLAY ON OVERHEAD BAR

		search: function (s) {
			map_events.search = s;

			return this;
		},

		view_event: function (id) {
			$('.event_content', '#event_description').load(URL_MOBILE + 'server/events.php', {id: id}, function () {
				$(this).parent('div').popup('open', {
					history: false
				});
				events.loading(false);
			});

			$("#event_description").on({
				popupbeforeposition: function () {
					var h = $(window).height() - 50;
					$("#event_description").css("height", h);
				}
			});

			return this;
		},

		error_message: function (info) {
			$('#message_error').html(info).popup('open');
		}
	};
	
