<?php
require_once '../../server/config.php';

$cat_id = isset($_REQUEST['cat_id']) ? (int)$_REQUEST['cat_id'] : 0;
if (empty($cat_id)) {
	exit;
}
require_once '../../server/Category.php';


$obj_cat = new Category();
if (!isset($obj_cat->aGroupCat[$cat_id])) {
	exit;
}

$child_id = isset($_REQUEST['child_id']) ? (int)$_REQUEST['child_id'] : 0;
if (empty($child_id)) { // If list its category and not subcategory

	echo '<fieldset data-role="controlgroup" data-iconpos="right">';
	foreach ($obj_cat->aGroupCat[$cat_id]['sub_cat'] as $key => $cat) {
		if (!count($cat['sub_cat'])) {
			echo temp_category_list_inform_radio($cat);
			continue;
		}

		echo temp_category_list_inform_link($cat, $cat_id);
	}
	echo '</fieldset>';

	exit;
}

// If list subcategory
if (!isset($obj_cat->aGroupCat[$cat_id]['sub_cat'][$child_id])) {
	exit;
}

echo '<fieldset data-role="controlgroup" data-iconpos="right">';
foreach ($obj_cat->aGroupCat[$cat_id]['sub_cat'][$child_id]['sub_cat'] as $key => $cat) {
	echo temp_category_list_inform_radio($cat, $obj_cat->aGroupCat[$cat_id]['name'], 'mini_icon_sub2');
}
echo '</fieldset>';

exit;
/* END */



/*
 * Template view Radio Input subcategory and subcategory2
 * */
function temp_category_list_inform_radio($cat, $parent_cat_name = '', $class = 'mini_icon_sub')
{
	return '
<input name="category" id="sub_cat_' . $cat['id'] .'" type="radio" value="' . $cat['id'] .'" />
<label class="mini_icon_label" for="sub_cat_' . $cat['id'] .'">
	<span class="' . $class . ' ' . $parent_cat_name . ' ' . get_class_icon($cat['icon']) . '"></span>
	' . $cat['name'] .'
</label>';
}

/*
 * Template view Link for subcategory list
 * */
function temp_category_list_inform_link($cat, $parent_id, $class = 'mini_icon_sub')
{
	return '
<div class="cat_list" data-role="collapsible" data-theme="a" data-content-theme="j">
	<h3 class="mini_icon_h3" data-cat_id="' . $parent_id . '" data-child_id="' . $cat['id'] .'">
		<span class="' . $class . ' ' . get_class_icon($cat['icon']) . '"></span>
		' .$cat['name'] . '
	</h3>
	<div class="sub_cat_list"></div>
</div>';

}

/*
 * Generate css class subcategory icon
 * */
function get_class_icon($icon)
{
	return str_replace('.png', '', $icon);
}