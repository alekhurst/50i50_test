<<<<<<< HEAD
<?php
require_once PATH_MOBILE . 'server/Table/DB.php';
require_once PATH_MOBILE . 'server/Obj/Users.php';
require_once PATH_MOBILE . 'server/Obj/Tags.php';
require_once PATH_MOBILE . 'server/Obj/Events.php';
require_once ABS_PATH . 'inc/Mail.php';


class Controller_Users
{
	public $user;
	public $Result;

	private $mail;
	private $permission = array(
		'sign_in' => 0,
		'sign_out' => 0,
		'sign_up' => 0,
		'forgot_password' => 0,

		'user_info_get' => 1,
		'user_info_edit' => 2,
		'user_info_del' => 2,

		'events_list' => 1,
		'event_del' => 2,
		'event_save' => 2
	);

	public function __construct($return_type = 'json')
	{
		$this->Result = new Result($return_type);

		$this->db = new Table_Users();
		$this->user = Obj_Users::isLogged();
	}

	public function action($func)
	{
		$access = isset($this->user['type']) ? $this->user['type'] : 0;

		if (isset($this->permission[$func]) && $this->permission[$func] <= $access) {
			return $this->$func();
		}

		return $this->Result->view(1, 'Permission Denied.');
	}

	private function sign_in()
	{
		$login = isset($_POST['email']) ? trim($_POST['email']) : null;
		if (empty($login)) {
			return $this->Result->view(1, 'The  username or email is incorrect.');
		}

		$pass = isset($_POST['password']) ? trim($_POST['password']) : null;
		if (empty($pass)) {
			return $this->Result->view(1, 'The password is incorrect.');
		}

		$this->user = Obj_Users::Login($login, $pass);
		$error = empty($this->user) || !is_array($this->user);

		if ($error) {
			$info = 'Your username or password was incorrect, try again.';
		} else {
			$info = 'Welcome to 50i50';
		}

		return $this->Result->view($error, $info, array('user' => $this->user));
	}

// ACTION
	/* USER INFO CHANGE */

	private function sign_out()
	{
		Obj_Users::Logout();

		return $this->Result->view(0, 'Bye!');
	}

	private function sign_up()
	{

		$email = isset($_POST['email']) ? trim($_POST['email']) : null;
		if (empty($email)) {
			return $this->Result->view(true, 'The email is incorrect.');
		}

		$username = isset($_POST['username']) ? trim($_POST['username']) : null;
		if (empty($username)) {
			return $this->Result->view(true, 'The username is incorrect.');
		}

		$pass = isset($_POST['password']) ? trim($_POST['password']) : null;
		if (empty($pass)) {
			return $this->Result->view(true, 'The password is incorrect.');
		}

		$conf_pass = isset($_POST['conf_password']) ? trim($_POST['conf_password']) : null;
		if (empty($conf_pass) || $pass != $conf_pass) {
			return $this->Result->view(true, 'Enter correct password!');
		}

		$user = $this->db->getUserByLoginOrEmail($username, $email);
		if (is_array($user)) {
			return $this->Result->view(true, 'The username or email you entered already exists. Please try something different.');

		}

		$res = $this->db->addUser($username, $email, $pass);
		if (empty($res)) {
			return $this->Result->view(1, 'Error saving new user. Please contact the site administrator - ' . ADMIN_EMAIL);
		}


		$mail = new Mail();
		$mail->addAddress($email, $username);
		$mail->setSubject('Welcome to 50i50!');
		$mail->setMsg("<p>Hello $username, welcome to 50i50!</p>

<p>Your account details are:<br />
Username: $username <br />
Email: $email<br />
Password: $pass</p>
<p>If you did not create this account:<br /
-Login to 50i50 and delete the account<br />
-Or change the password to take ownership</p>
<p>You are officially a member of our community and can start using 50i50 to its full potential.<br />
Inform others of what's happening around you to take part in an adventure through time and space.</p>
<p>Ready to get started? Download the app, or go to: " . PROTOCOL . URL_MOBILE . '<br /><br />
The 50i50 team</p>');

		$res = $mail->send();

		if ($res) {
			return $this->Result->view(1, $res);
		}

		return $this->Result->view(0, 'Thank you for registering! Please login now.');
	}

	private function forgot_password()
	{
		$login = isset($_POST['forgot_pass']) ? trim($_POST['forgot_pass']) : null;
		if (empty($login)) {
			return $this->Result->view(1, 'Enter Username or Email!');
		}

		$user = $this->db->getUserByLoginOrEmail($login);
		if (empty($user)) {
			return $this->Result->view(1, 'The username or password you entered is incorrect. Please, try again');
		}

		$pass = md5(time() . rand(0, 10000));
		$user_info = array('password' => $pass);

		if (!$this->access('user_info_edit')) {
			return $this->Result->view(1, 'Permission Denied.');
		}

		$error = $this->db->updateUserInfo($user_info, $user['id']);

		if (empty($error)) {
			$info = 'Error! Your password was not changed. Please contact support.' . ADMIN_EMAIL;
		} else {
			$info = 'Your new password has been sent via email.';

			$mail = new Mail();
			$mail->addAddress($user['email'], $user['username']);
			$mail->setSubject('Your new 50i50 password');
			$mail->setMsg('<p>Hello ' . $user['username'] . ', you have recently requested to reset your 50i50 password.</p>
<p>Your new password is: ' . $pass . '</p>
<p>If you did not make this request, please login to 50i50.org with the password given in this email, click on your username under the Account menu, and enter a new password.</p>
<p>You may login here now: ' . PROTOCOL . URL_MOBILE . '</p>');

			$res = $mail->send();

			if ($res) {
				return $this->Result->view(1, $res);
			}
		}

		return $this->Result->view($error, $info);
	}

	public function access($func)
	{
		$access = isset($this->user['type']) ? $this->user['type'] : 0;

		if (isset($this->permission[$func]) && $this->permission[$func] >= $access) {
			return true;
		}

		return false; //$this->Result->view(1, 'Permission Denied.');
	}

	private function user_info_get()
	{
		if (empty($this->user)) {
			return $this->Result->view(true, 'You must login first. <a href="#user_signin">Login here</a>');
		}

		$user = $this->db->getUserById($this->user['id']);
		if (empty($user)) {
			Obj_Users::Logout();
			return $this->Result->view(true, 'You must login first <a href="#user_signin">Login here</a>');
		}

		return $this->Result->view(false, null, array('user' => $user));
	}

	private function user_info_edit()
	{
		$username = isset($_POST['username']) ? trim($_POST['username']) : null;
		$email = isset($_POST['email']) ? trim($_POST['email']) : null;

		if (!$username && !$email) {
			return $this->Result->view(true, 'Enter Username or Email');
		}

		if ($this->user['email'] != $email && $this->db->getUserByEmail($email)) {
			return $this->Result->view(true, 'Error! This email already exists.');
		}

		if ($this->user['username'] != $username && $this->db->getUserByUsername($username)) {
			return $this->Result->view(true, 'Error! This username already exists.');
		}

		$old_pass = isset($_POST['old_password']) ? $_POST['old_password'] : '';
		$pass = isset($_POST['password']) ? $_POST['password'] : '';
		$conf_pass = isset($_POST['conf_password']) ? $_POST['conf_password'] : '';
		$lang = isset($_POST['lang']) ? $_POST['lang'] : 'en';
		$user_info = array(
			'username' => $username,
			'email' => $email,
			'lang' => $lang
		);


		if ($pass || $conf_pass) {
			if ($pass !== $conf_pass) {
				return $this->Result->view(true, 'Enter the Correct Password!');
			} else if (!$this->db->verificationPassword($this->user['id'], $old_pass)) {
				return $this->Result->view(true, 'Enter the Correct Old Password');
			}

			$user_info['password'] = $pass;
		}

		if (!$this->db->updateUserInfo($user_info, $this->user['id'])) {
			return $this->Result->view(true, 'Error Updating Info. Please Contact the site administrator. ' . ADMIN_EMAIL);
		}

		$this->user = Obj_Users::LoginByID($this->user['id']);

		return $this->Result->view(false, 'Account Info Changed', array('user' => $this->user));
	}

	private function user_info_del()
	{
//		if (empty($this->user)) {
//			return $this->Result->view(true, 'You must login first. <a href="#user_signin">Login here</a>');
//		}

		$error = !Obj_Events::delEventsByUserID($this->user['id']);

		if ($error) {
			$info = 'Error deleting your information. Please contact the site administrator - ' . ADMIN_EMAIL;
		} else {
			$error = !$this->db->delUser($this->user['id']);

			if ($error) {
				$info = 'Error deleting your account. Please contact the site administrator - ' . ADMIN_EMAIL;
			} else {
				$info = 'Your Account and Information have been deleted!';
			}
		}

		return $this->Result->view($error, $info);
	}

	/* EVENTS INFO CHANGE */

	private function events_list()
	{
		$content = '';

		$events = Obj_Events::getEventsByUserId($this->user['id']);

		for ($i = 0, $c = count($events); $i < $c; $i++) {
			$d = explode('-', $events[$i]['f_inicio']);
			$t = explode(':', $events[$i]['h_inicio']);

			$time = mktime($t[0], $t[1], $t[2], $d[1], $d[2], $d[0]);
			$date = array($d[0], $d[1], $d[2], $t[0], $t[1], $t[2]);

			$content .= '<li id="ev_' . $events[$i]['id_evento'] . '" data-id="' . $events[$i]['id_evento'] . '">
	<a href="#" data-ln="' . $events[$i]['latitud'] . '" data-lg="' . $events[$i]['longitud'] . '" data-date="' . implode(',', $date) . '">
		<span class="date_sort" style="display: none;">' . date('l, F d, Y', $time) . '</span>
		<img src="' . ($events[$i]['imagen'] ? $events[$i]['imagen'] : PROTOCOL . URL_MOBILE . 'img/thumb.png') . '">
		<h2>' . $events[$i]['titulo'] . '</h2>
		<p><strong>' . $events[$i]['lugar'] . '</strong></p><p>' . $events[$i]['descripcion'] . '</p>
		<p class="ui-li-aside"><strong>' . date('h:i', $time) . '</strong> ' . date('A', $time) . '</p>
	</a>
	<a class="delete_event" href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Delete Information</a>
</li>';
			//' . PROTOCOL . URL_MOBILE . 'event.desc.php?id=' . $events[$i]['id_evento'] . '
		}

		return $this->Result->view(false, null, array('content' => $content));
	}

	private function event_save()
	{
		$title = isset($_POST['share_title']) ? trim($_POST['share_title']) : '';
		if (empty($title)) {
			return $this->Result->view(true, '<p class="error">Please, enter title.</p>');
		}

		$coordinate = isset($_POST['loc_custom_coordinate']) ? explode(',', $_POST['loc_custom_coordinate']) : array();
		if (count($coordinate) < 2) {
			return $this->Result->view(true, '<p class="error">Please, enter location.</p>');
		}


		$tbEvents = new Table_Events();

//		if ($tbEvents->getEventByTitle($title)) {
//			return $this->Result->view(true, '<p class="error">Sorry, but event <b>' . $title . '</b> already exists as a category. Please change the title.</p>');
//		}

		$more_link = isset($_POST['more_link']) ? trim($_POST['more_link']) : '';
		if (strpos($more_link, 'http://') === false && strpos($more_link, 'https://') === false) {
			$more_link = 'http://' . $more_link;
		}

// Date/Time Block
		$post_start_time = $post_end_time = 0;
		$view_start_time = $view_end_time = 0;

		$arr_date = isset($_POST['loc_start_time']) ? explode(',', $_POST['loc_start_time']) : array();
		if (count($arr_date) == 6) {
			$post_start_time = mktime($arr_date[0], 0, 0, $arr_date[3], $arr_date[4], $arr_date[5]);
			$view_start_time = mktime($arr_date[0], $arr_date[1], $arr_date[2], $arr_date[3], $arr_date[4], $arr_date[5]);
		}

		if (empty($post_start_time)) {
			$arr_date = isset($_POST['loc_default_time']) ? explode(',', $_POST['loc_default_time']) : array();

			if (count($arr_date) == 6) {
				$post_start_time = $post_end_time = mktime($arr_date[0], 0, 0, $arr_date[3], $arr_date[4], $arr_date[5]);
				$view_start_time = $view_end_time = mktime($arr_date[0], $arr_date[1], $arr_date[2], $arr_date[3], $arr_date[4], $arr_date[5]);
			}
			$timer = isset($_POST['time_slider']) ? (int)$_POST['time_slider'] : 0;
			if ($timer) {
				switch ($_POST['now_during']) {
					case 'hour':
						$timer *= 3600;
						break;

					case 'minute':
					default:
						$timer *= 60;
				}

				$post_end_time += $timer;
				$view_end_time += $timer;
			}
		} else {
			$arr_date = isset($_POST['loc_end_time']) ? explode(',', $_POST['loc_end_time']) : array();
			if (count($arr_date) == 6) {
				$post_end_time = mktime($arr_date[0], 0, 0, $arr_date[3], $arr_date[4], $arr_date[5]);
				$view_end_time = mktime($arr_date[0], $arr_date[1], $arr_date[2], $arr_date[3], $arr_date[4], $arr_date[5]);
			}
		}

		$event = array(
			'titulo' => $title,
			'descripcion' => isset($_POST['share_desc']) ? trim($_POST['share_desc']) : '',
			'etiquetas' => isset($_POST['share_tag']) ? trim($_POST['share_tag']) : '',
			'privacy' => (isset($_POST['privacy']) && $_POST['privacy'] == 'privacy') ? 1 : 0,
			'link' => $more_link,
			'imagen' => isset($_POST['upload_file_link']) ? $_POST['upload_file_link'] : '',

			'category_id' => (isset($_POST['category']) && (int)$_POST['category']) ? (int)$_POST['category'] : 58,
			'repeat' => isset($_POST['time_repeat']) ? (int)$_POST['time_repeat'] : 0,

			'lugar' => isset($_POST['loc_custom_address']) ? trim($_POST['loc_custom_address']) : '',
			'latitud' => (float)$coordinate[0],
			'longitud' => (float)$coordinate[1],
			'latitud2' => 0,
			'longitud2' => 0,

			'scale' => 31, // Visible event in scale type - {Second=1, Hours=2, Weeks=4, Days=8, Months=16} == 31.
			'user_id' => $this->user['id'],
			'source' => $this->user['username'],

			'f_inicio' => date('Y-m-d', $view_start_time),
			'h_inicio' => date('H:i:s', $view_start_time),
			'year' => date('Y', $view_start_time),
			'f_fin' => date('Y-m-d', $view_end_time),
			'h_fin' => date('H:i:s', $view_end_time),
			'start_time' => $post_start_time,
			'live_time' => $post_end_time - $post_start_time
		);

		$event_id = $tbEvents->addNewEvent($event);

		if (empty($event_id)) {
			return $this->Result->view(true, '<p class="error">User Update error. Please contact the site administrator' . ADMIN_EMAIL . '</p>');
		}

		Obj_Tags::addNewTags($event['etiquetas']);

		return $this->Result->view(false, '<p class="ok">Save Event!</p>', array('id' => $event_id));
	}

	private function event_del()
	{
		$id = isset($_REQUEST['event']) ? intval($_REQUEST['event']) : false;
		if (empty($id)) {
			return $this->Result->view(1, 'No ID param');
		}

		if (Obj_Events::delEventByID($id, $this->user['id'])) {
			return $this->Result->view(0, 'Event Deleted.');
		}

		return $this->Result->view(1, 'Error deleting your event. Please contact the site administrator - ' . ADMIN_EMAIL);
	}

}

class Result
{
	public $type;

	public function __construct($type = 'json')
	{
		$this->type = $type;
	}

	public function view($error = 0, $info = '', $result = array())
	{

		switch ($this->type) {
			case 'json' :
				$result['error'] = (int)$error;
				$result['info'] = $info;
				return json_encode($result);
				break;

			case 'html' :
				return empty($error) ? $result : $info;
				break;

			default :
				return array('error' => (int)$error, 'info' => $info, $result);
		}
	}
}
=======
<?php
require_once PATH_MOBILE . 'server/Table/DB.php';
require_once PATH_MOBILE . 'server/Obj/Users.php';
require_once PATH_MOBILE . 'server/Obj/Tags.php';
require_once PATH_MOBILE . 'server/Obj/Events.php';
require_once ABS_PATH . 'inc/Mail.php';


class Controller_Users
{
	public $user;
	public $Result;

	private $mail;
	private $permission = array(
		'sign_in' => 0,
		'sign_out' => 0,
		'sign_up' => 0,
		'forgot_password' => 0,

		'user_info_get' => 1,
		'user_info_edit' => 2,
		'user_info_del' => 2,

		'events_list' => 1,
		'event_del' => 2,
		'event_save' => 2
	);

	public function __construct($return_type = 'json')
	{
		$this->Result = new Result($return_type);

		$this->db = new Table_Users();
		$this->user = Obj_Users::isLogged();
	}

	public function action($func)
	{
		$access = isset($this->user['type']) ? $this->user['type'] : 0;

		if (isset($this->permission[$func]) && $this->permission[$func] <= $access) {
			return $this->$func();
		}

		return $this->Result->view(1, 'Permission Denied.');
	}

	private function sign_in()
	{
		$login = isset($_POST['email']) ? trim($_POST['email']) : null;
		if (empty($login)) {
			return $this->Result->view(1, 'You forgot a Username or Email');
		}

		$pass = isset($_POST['password']) ? trim($_POST['password']) : null;
		if (empty($pass)) {
			return $this->Result->view(1, 'You forgot a Password!');
		}

		$this->user = Obj_Users::Login($login, $pass);
		$error = empty($this->user) || !is_array($this->user);

		if ($error) {
			$info = 'Your username or password was incorrect, try again!';
		} else {
			$info = 'Welcome to 50i50';
		}

		return $this->Result->view($error, $info, array('user' => $this->user));
	}

// ACTION
	/* USER INFO CHANGE */

	private function sign_out()
	{
		Obj_Users::Logout();

		return $this->Result->view(0, 'Bye.');
	}

	private function sign_up()
	{

		$email = isset($_POST['email']) ? trim($_POST['email']) : null;
		if (empty($email)) {
			return $this->Result->view(true, 'You forgot an Email!');
		}

		$username = isset($_POST['username']) ? trim($_POST['username']) : null;
		if (empty($username)) {
			return $this->Result->view(true, 'You forgot a Username!');
		}

		$pass = isset($_POST['password']) ? trim($_POST['password']) : null;
		if (empty($pass)) {
			return $this->Result->view(true, 'You forgot a Password!');
		}

		$conf_pass = isset($_POST['conf_password']) ? trim($_POST['conf_password']) : null;
		if (empty($conf_pass) || $pass != $conf_pass) {
			return $this->Result->view(true, 'Enter Correct Password!');
		}

		$user = $this->db->getUserByLoginOrEmail($username, $email);
		if (is_array($user)) {
			return $this->Result->view(true, 'The username or email you entered already exists. Please try something different');

		}

		$res = $this->db->addUser($username, $email, $pass);
		if (empty($res)) {
			return $this->Result->view(1, 'Error saving new user. Please contact the site administrator - ' . ADMIN_EMAIL);
		}


		$mail = new Mail();
		$mail->addAddress($email, $username);
		$mail->setSubject('Welcome to 50i50!');
		$mail->setMsg("<p>Hello $username, Welcome to 50i50!</p>

<p>Your account details are:<br />
Username: $username <br />
Email: $email<br />
Password: $pass</p>
<p>If you did not create this account:<br /
-Login to 50i50 and delete the account<br />
-Or change the passoword to take ownership</p>
<p>You are officially a member of our community and can start using 50i50 to its full potential!<br />
Inform others of what's happening around you to take part in an adventure through time and space.</p>
<p>Ready to get started? Download the app, or go to: " . PROTOCOL . URL_MOBILE . '<br /><br />
The 50i50 team</p>');

		$res = $mail->send();

		if ($res) {
			return $this->Result->view(1, $res);
		}

		return $this->Result->view(0, 'Thank you for registering! Please login now.');
	}

	private function forgot_password()
	{
		$login = isset($_POST['forgot_pass']) ? trim($_POST['forgot_pass']) : null;
		if (empty($login)) {
			return $this->Result->view(1, 'Enter Username or Email!');
		}

		$user = $this->db->getUserByLoginOrEmail($login);
		if (empty($user)) {
			return $this->Result->view(1, 'The username or password you entered is incorrect. Please, try again');
		}

		$pass = md5(time() . rand(0, 10000));
		$user_info = array('password' => $pass);

		if (!$this->access('user_info_edit')) {
			return $this->Result->view(1, 'Permission Denied.');
		}

		$error = $this->db->updateUserInfo($user_info, $user['id']);

		if (empty($error)) {
			$info = 'Error! Your password was not changed. Please contact support.' . ADMIN_EMAIL;
		} else {
			$info = 'Your new password has been sent via email.';

			$mail = new Mail();
			$mail->addAddress($user['email'], $user['username']);
			$mail->setSubject('Your new 50i50 password');
			$mail->setMsg('<p>Hello ' . $user['username'] . ',you have recently requested to reset your 50i50 password.</p>
<p>Your new password is: ' . $pass . '</p>
<p>If you did not make this request, please login to 50i50.org with the password given in this email, click on your username under the Account menu, and enter a new password.</p>
<p>You may login here now: ' . PROTOCOL . URL_MOBILE . '</p>');

			$res = $mail->send();

			if ($res) {
				return $this->Result->view(1, $res);
			}
		}

		return $this->Result->view($error, $info);
	}

	public function access($func)
	{
		$access = isset($this->user['type']) ? $this->user['type'] : 0;

		if (isset($this->permission[$func]) && $this->permission[$func] >= $access) {
			return true;
		}

		return false; //$this->Result->view(1, 'Permission Denied.');
	}

	private function user_info_get()
	{
		if (empty($this->user)) {
			return $this->Result->view(true, 'You must login first. <a href="#user_signin">Login here</a>');
		}

		$user = $this->db->getUserById($this->user['id']);
		if (empty($user)) {
			Obj_Users::Logout();
			return $this->Result->view(true, 'You must login first <a href="#user_signin">Login here</a>');
		}

		return $this->Result->view(false, null, array('user' => $user));
	}

	private function user_info_edit()
	{
		$username = isset($_POST['username']) ? trim($_POST['username']) : null;
		$email = isset($_POST['email']) ? trim($_POST['email']) : null;

		if (!$username && !$email) {
			return $this->Result->view(true, 'Enter Username or Email');
		}

		if ($this->user['email'] != $email && $this->db->getUserByEmail($email)) {
			return $this->Result->view(true, 'Error! This email already exists.');
		}

		if ($this->user['username'] != $username && $this->db->getUserByUsername($username)) {
			return $this->Result->view(true, 'Error! This username already exists.');
		}

		$old_pass = isset($_POST['old_password']) ? $_POST['old_password'] : '';
		$pass = isset($_POST['password']) ? $_POST['password'] : '';
		$conf_pass = isset($_POST['conf_password']) ? $_POST['conf_password'] : '';
		$lang = isset($_POST['lang']) ? $_POST['lang'] : 'en';
		$user_info = array(
			'username' => $username,
			'email' => $email,
			'lang' => $lang
		);


		if ($pass || $conf_pass) {
			if ($pass !== $conf_pass) {
				return $this->Result->view(true, 'Enter the Correct Password!');
			} else if (!$this->db->verificationPassword($this->user['id'], $old_pass)) {
				return $this->Result->view(true, 'Enter the Correct Old Password');
			}

			$user_info['password'] = $pass;
		}

		if (!$this->db->updateUserInfo($user_info, $this->user['id'])) {
			return $this->Result->view(true, 'Error Updating Info. Please Contact the site administrator - ' . ADMIN_EMAIL);
		}

		$this->user = Obj_Users::LoginByID($this->user['id']);

		return $this->Result->view(false, 'Account Info Changed', array('user' => $this->user));
	}

	private function user_info_del()
	{
//		if (empty($this->user)) {
//			return $this->Result->view(true, 'You must login first. <a href="#user_signin">Login here</a>');
//		}

		$error = !Obj_Events::delEventsByUserID($this->user['id']);

		if ($error) {
			$info = 'Error deleting your information. Please contact the site administrator - ' . ADMIN_EMAIL;
		} else {
			$error = !$this->db->delUser($this->user['id']);

			if ($error) {
				$info = 'Error deleting your account. Please contact the site administrator - ' . ADMIN_EMAIL;
			} else {
				$info = 'Your Account and Information have been deleted!';
			}
		}

		return $this->Result->view($error, $info);
	}

	/* EVENTS INFO CHANGE */

	private function events_list()
	{
		$content = '';

		$events = Obj_Events::getEventsByUserId($this->user['id']);

		for ($i = 0, $c = count($events); $i < $c; $i++) {
			$d = explode('-', $events[$i]['f_inicio']);
			$t = explode(':', $events[$i]['h_inicio']);

			$time = mktime($t[0], $t[1], $t[2], $d[1], $d[2], $d[0]);
			$date = array($d[0], $d[1], $d[2], $t[0], $t[1], $t[2]);

			$content .= '<li id="ev_' . $events[$i]['id_evento'] . '" data-id="' . $events[$i]['id_evento'] . '">
	<a href="#" data-ln="' . $events[$i]['latitud'] . '" data-lg="' . $events[$i]['longitud'] . '" data-date="' . implode(',', $date) . '">
		<span class="date_sort" style="display: none;">' . date('l, F d, Y', $time) . '</span>
		<img src="' . ($events[$i]['imagen'] ? $events[$i]['imagen'] : PROTOCOL . URL_MOBILE . 'img/thumb.png') . '">
		<h2>' . $events[$i]['titulo'] . '</h2>
		<p><strong>' . $events[$i]['lugar'] . '</strong></p><p>' . $events[$i]['descripcion'] . '</p>
		<p class="ui-li-aside"><strong>' . date('h:i', $time) . '</strong> ' . date('A', $time) . '</p>
	</a>
	<a class="delete_event" href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Delete Information</a>
</li>';
			//' . PROTOCOL . URL_MOBILE . 'event.desc.php?id=' . $events[$i]['id_evento'] . '
		}

		return $this->Result->view(false, null, array('content' => $content));
	}

	private function event_save()
	{
		$title = isset($_POST['share_title']) ? trim($_POST['share_title']) : '';
		if (empty($title)) {
			return $this->Result->view(true, '<p class="error">Please, enter title.</p>');
		}

		$coordinate = isset($_POST['loc_custom_coordinate']) ? explode(',', $_POST['loc_custom_coordinate']) : array();
		if (count($coordinate) < 2) {
			return $this->Result->view(true, '<p class="error">Please, enter location.</p>');
		}


		$tbEvents = new Table_Events();

//		if ($tbEvents->getEventByTitle($title)) {
//			return $this->Result->view(true, '<p class="error">Sorry, but event <b>' . $title . '</b> already exists as a category. Please change the title.</p>');
//		}

		$more_link = isset($_POST['more_link']) ? trim($_POST['more_link']) : '';
		if (strpos($more_link, 'http://') === false && strpos($more_link, 'https://') === false) {
			$more_link = 'http://' . $more_link;
		}

// Date/Time Block
		$post_start_time = $post_end_time = 0;
		$view_start_time = $view_end_time = 0;

		$arr_date = isset($_POST['loc_start_time']) ? explode(',', $_POST['loc_start_time']) : array();
		if (count($arr_date) == 6) {
			$post_start_time = mktime($arr_date[0], 0, 0, $arr_date[3], $arr_date[4], $arr_date[5]);
			$view_start_time = mktime($arr_date[0], $arr_date[1], $arr_date[2], $arr_date[3], $arr_date[4], $arr_date[5]);
		}

		if (empty($post_start_time)) {
			$arr_date = isset($_POST['loc_default_time']) ? explode(',', $_POST['loc_default_time']) : array();

			if (count($arr_date) == 6) {
				$post_start_time = $post_end_time = mktime($arr_date[0], 0, 0, $arr_date[3], $arr_date[4], $arr_date[5]);
				$view_start_time = $view_end_time = mktime($arr_date[0], $arr_date[1], $arr_date[2], $arr_date[3], $arr_date[4], $arr_date[5]);
			}
			$timer = isset($_POST['time_slider']) ? (int)$_POST['time_slider'] : 0;
			if ($timer) {
				switch ($_POST['now_during']) {
					case 'hour':
						$timer *= 3600;
						break;

					case 'minute':
					default:
						$timer *= 60;
				}

				$post_end_time += $timer;
				$view_end_time += $timer;
			}
		} else {
			$arr_date = isset($_POST['loc_end_time']) ? explode(',', $_POST['loc_end_time']) : array();
			if (count($arr_date) == 6) {
				$post_end_time = mktime($arr_date[0], 0, 0, $arr_date[3], $arr_date[4], $arr_date[5]);
				$view_end_time = mktime($arr_date[0], $arr_date[1], $arr_date[2], $arr_date[3], $arr_date[4], $arr_date[5]);
			}
		}

		$event = array(
			'titulo' => $title,
			'descripcion' => isset($_POST['share_desc']) ? trim($_POST['share_desc']) : '',
			'etiquetas' => isset($_POST['share_tag']) ? trim($_POST['share_tag']) : '',
			'privacy' => (isset($_POST['privacy']) && $_POST['privacy'] == 'privacy') ? 1 : 0,
			'link' => $more_link,
			'imagen' => isset($_POST['upload_file_link']) ? $_POST['upload_file_link'] : '',

			'category_id' => (isset($_POST['category']) && (int)$_POST['category']) ? (int)$_POST['category'] : 58,
			'repeat' => isset($_POST['time_repeat']) ? (int)$_POST['time_repeat'] : 0,

			'lugar' => isset($_POST['loc_custom_address']) ? trim($_POST['loc_custom_address']) : '',
			'latitud' => (float)$coordinate[0],
			'longitud' => (float)$coordinate[1],
			'latitud2' => 0,
			'longitud2' => 0,

			'scale' => 31, // Visible event in scale type - {Second=1, Hours=2, Weeks=4, Days=8, Months=16} == 31.
			'user_id' => $this->user['id'],
			'source' => $this->user['username'],

			'f_inicio' => date('Y-m-d', $view_start_time),
			'h_inicio' => date('H:i:s', $view_start_time),
			'year' => date('Y', $view_start_time),
			'f_fin' => date('Y-m-d', $view_end_time),
			'h_fin' => date('H:i:s', $view_end_time),
			'start_time' => $post_start_time,
			'live_time' => $post_end_time - $post_start_time
		);

		$event_id = $tbEvents->addNewEvent($event);

		if (empty($event_id)) {
			return $this->Result->view(true, '<p class="error">User Update error. Please contact the site administrator' . ADMIN_EMAIL . '</p>');
		}

		Obj_Tags::addNewTags($event['etiquetas']);

		return $this->Result->view(false, '<p class="ok">Save Event!</p>', array('id' => $event_id));
	}

	private function event_del()
	{
		$id = isset($_REQUEST['event']) ? intval($_REQUEST['event']) : false;
		if (empty($id)) {
			return $this->Result->view(1, 'No ID param');
		}

		if (Obj_Events::delEventByID($id, $this->user['id'])) {
			return $this->Result->view(0, 'Event Deleted.');
		}

		return $this->Result->view(1, 'Error deleting your event. Please contact the site administrator - ' . ADMIN_EMAIL);
	}

}

class Result
{
	public $type;

	public function __construct($type = 'json')
	{
		$this->type = $type;
	}

	public function view($error = 0, $info = '', $result = array())
	{

		switch ($this->type) {
			case 'json' :
				$result['error'] = (int)$error;
				$result['info'] = $info;
				return json_encode($result);
				break;

			case 'html' :
				return empty($error) ? $result : $info;
				break;

			default :
				return array('error' => (int)$error, 'info' => $info, $result);
		}
	}
}
>>>>>>> 3bf313abecd4c3484d2bcfa6a19c63c50d04fb88
