<?php

class Table_Events extends DB
{
	static private $tb = 'timeline_2012';

	function delEventsByUserId($user_id)
	{
		$query = 'DELETE FROM ' . self::$tb . ' WHERE user_id = ' . intval($user_id);
		return mysql_query($query);
	}

	function getEventsByUserId($user_id)
	{
		$query = "SELECT * FROM " . self::$tb . " WHERE user_id=" . $user_id . " ORDER BY f_inicio DESC";
		$res = mysql_query($query);
		$events = array();
		while ($row = mysql_fetch_assoc($res)) {
			$events[] = $row;
		}
		return $events;
	}

	function delEventsById($id, $user_id)
	{
		$query = 'DELETE FROM ' . self::$tb . ' WHERE id_evento = ' . intval($id) . ' AND user_id =' . intval($user_id);
		return mysql_query($query);
	}

	function getEventById($id)
	{
		$query = "SELECT * FROM " . self::$tb . " WHERE id_evento=" . intval($id) . " LIMIT 1";
		$res = mysql_query($query);
		if (mysql_num_rows($res)) {
			return mysql_fetch_assoc($res);
		}

		return false;
	}

	function getEventByTitle($title)
	{
		$query = "SELECT * FROM " . self::$tb . " WHERE titulo='" . $this->escape($title) . "' LIMIT 1";
		$res = mysql_query($query);
		if (mysql_num_rows($res)) {
			return mysql_fetch_assoc($res);
		}

		return false;
	}

	function addNewEvent($event)
	{
		$cols = array();
		$vals = array();
		foreach ($event as $key => $val) {
			$cols[] = "`" . $key . "`";
			$vals[] = "'" . $this->escape($val) . "'";
		}

		$query = 'INSERT INTO ' . self::$tb . '(' . implode(',', $cols) . ') VALUE(' . implode(',', $vals) . ')';

		if (mysql_query($query)) {
			return mysql_insert_id();
		}

		return false;
	}
}