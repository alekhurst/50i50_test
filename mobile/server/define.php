<?php
/* Preview PopUp */
define('PREVIEW_VIDEO_VERSION', 'v.18.06.2013');// change version for open video again

/* Search Default settings */
define('SEARCH_SET_TIME_STEP', 'hour');// Values - hour | day

/* Map Settings */
define('MAP_SET_ZOOM', 10);
define('MAP_SET_LAT_LNG', '37.69065350120003;-122.18050947109373');// Set Latitude and Longitude - separated ';'
