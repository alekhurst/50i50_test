<?php
require_once PATH_MOBILE . 'server/Table/Users.php';

class Obj_Users
{

	static function addUser()
	{

	}

	static function delUser()
	{

	}

	static function editUser()
	{

	}

	static function Login($login, $pass)
	{
		$tbUsers = new Table_Users();
		$arrUser = $tbUsers->getUserForLogin($login, $pass);

		if (empty($arrUser)) {
			return false;
		}

		$strDate = date('Y-m-d h:i:s');
		$strIp = $_SERVER['REMOTE_ADDR'];
		$key = self::setRememberUser($arrUser, $strDate, $strIp);

		$arrLogin = array(
			'visit_date' => $strDate,
			'visit_key' => self::getUserKey($key, $strIp, $arrUser['username'])
		);

		$tbUsers->updateUserInfo($arrLogin, $arrUser['id']);

		self::setSessionUser($arrUser);

		return $arrUser;
	}

	static function LoginByID($id)
	{
		$tbUsers = new Table_Users();
		$arrUser = $tbUsers->getUserById($id);

		if (empty($arrUser)) {
			return false;
		}

		$strDate = date('Y-m-d H:i:s');
		$strIp = $_SERVER['REMOTE_ADDR'];
		$key = self::setRememberUser($arrUser, $strDate, $strIp);

		$arrLogin = array(
			'visit_date' => $strDate,
			'visit_key' => self::getUserKey($key, $strIp, $arrUser['username'])
		);

		$tbUsers->updateUserInfo($arrLogin, $arrUser['id']);

		self::setSessionUser($arrUser);

		return $arrUser;
	}

	static function isLogged()
	{
		$arrUser = self::getSessionUser();
		if ($arrUser) {
			return $arrUser;
		}

		return self::getRememberUser();
	}

	static function Logout()
	{
		self::delRememberUser();

		self::delSessionUser();
	}


	static function setRememberUser($arrUser, $strDate, $strIp)
	{
		$arrCookie = isset($_COOKIE[MOBILE_COOKIE]) ? json_decode($_COOKIE[MOBILE_COOKIE], true) : array();
		$arrCookie['KEY'] = self::getCookieKey($arrUser['email'], $strDate, $strIp);
		$arrCookie['USER'] = $arrUser['username'];

		setcookie(MOBILE_COOKIE, json_encode($arrCookie), time() + 3600 * 24 * 365, '/');

		return $arrCookie['KEY'];
	}

	static function getRememberUser()
	{
		$arrCookie = isset($_COOKIE[MOBILE_COOKIE]) ? json_decode($_COOKIE[MOBILE_COOKIE], true) : array();

		if (isset($arrCookie['KEY']) && isset($arrCookie['USER'])) {
			$key = self::getUserKey($arrCookie['KEY'], $_SERVER['REMOTE_ADDR'], $arrCookie['USER']);

			$tbUsers = new Table_Users();
			$arrUser = $tbUsers->getUserByUserKey($arrCookie['USER'], $key);

			if ($arrUser) {
				self::setSessionUser($arrUser);
				return $arrUser;
			}
		}

		return false;
	}

	static function delRememberUser()
	{
		$arrCookie = isset($_COOKIE[MOBILE_COOKIE]) ? json_decode($_COOKIE[MOBILE_COOKIE], true) : array();
		if (isset($arrCookie['KEY'])) {
			unset($arrCookie['KEY']);
		}

		if (isset($arrCookie['USER'])) {
			unset($arrCookie['USER']);
		}

		setcookie(MOBILE_COOKIE, json_encode($arrCookie), time() + 3600 * 24 * 365, '/');
	}


	static function setSessionUser($arrUser)
	{
		$_SESSION['USER_INFO'] = $arrUser;
	}

	static function getSessionUser()
	{
		if (isset($_SESSION['USER_INFO'])) {
			return $_SESSION['USER_INFO'];
		}

		return false;
	}

	static function delSessionUser()
	{
		$arrUser = self::getSessionUser();
		if ($arrUser) {
			$tbUsers = new Table_Users();

			$arrUsers = array('visit_key' => '');
			$tbUsers->updateUserInfo($arrUsers, $arrUser['id']);

			unset($_SESSION['USER_INFO']);
		}
	}


	static function getUserKey($key, $ip, $user)
	{
		return md5($key . $ip . $user);
	}

	static function getCookieKey($email, $date, $ip)
	{
		return md5($email . $date . $ip . rand(0, time()));
	}

}