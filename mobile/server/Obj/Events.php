<?php
require_once PATH_MOBILE . 'server/Table/Events.php';

class Obj_Events {

	static function getEventsByUserId($intUserID)
	{
		$tbEvents = new Table_Events();

		return $tbEvents->getEventsByUserId($intUserID);
	}

	static function delEventsByUserID($intUserID)
	{
		$tbEvents = new Table_Events();

		return $tbEvents->delEventsByUserId($intUserID);
	}

	static function delEventByID($intID, $intUserID)
	{
		$tbEvents = new Table_Events();

		return $tbEvents->delEventsById($intID, $intUserID);
	}
}