<?php


class newFormatDate extends ModelQuery
{

	function __construct() {
//		$this->sSelectEvent = $this->tb_timeline . '.f_inicio';
		$this->sSelectEvent = $this->tb_timeline . '.`year`';
		parent::__construct();
	}

	public function getDateForSelect () {
		return " floor(" . $this->sSelectEvent . "/10)*10 ";
//		return " floor(year(" . $this->sSelectEvent . ")/10)*10 ";
	}

	public function getWhereForSelect () {
		$century = $this->returnCenturyStart($this->iSearchYear);
		$this->aWhere[] = " (" . $this->sSelectEvent . " >= ". $century ." AND " . $this->sSelectEvent . " <= ". ($century + 99) . ") AND " . $this->tb_timeline . ".scale & 256 = 256";
//		$this->aWhere[] = $this->tb_timeline . ".f_inicio BETWEEN '". $century ."-1-1' AND '". ($century + 99) ."-12-31'";
	}

	private function returnCenturyStart ($date) {
		return ($date != 0) ? floor($date/100) * 100 : 0;
	}

	function getCorrectedSubitem ($subitem) {
		$century = $this->returnCenturyStart(intval($subitem));
		$subitem = $subitem - $century;
		return ($subitem != 0) ? floor($subitem/10) : 0;
	}
}