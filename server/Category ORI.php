<?php

class Category
{
	public $aCat = array();
	public $aCatList = array();
	public $aGroupCat = array();
//	public $aDefaultCat = array(1, 21, 32, 67, 79, 77, 87, 115, 130, 145, 157);
	public $aDefaultCat = array(2,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18, 21, 32, 49, 67, 79, 77, 87, 115, 130, 145, 157);
	public $aDefaultCatEx = array();

	private $sCatName = 'CAT_LIST';
	private $sGroupCatName = 'GROUP_CAT_LIST';

	function __construct ()
	{
		$this->getCatList_Memcache();
	}

	private function getCatList_Memcache ()
	{
		$objMemcache = new Memcache;
		if (@$objMemcache->connect('localhost', 11211)) {
			$this->aCatList = @$objMemcache->get($this->sCatName);
			$this->aGroupCat = @$objMemcache->get($this->sGroupCatName);

			if (! $this->aCatList && count($this->aCatList) < 2) {
				$this->getCatList_DB();
				$objMemcache->set($this->sCatName, $this->aCatList, 3600*3600);
				$objMemcache->set($this->sGroupCatName, $this->aGroupCat, 3600*3600);
			}

			$objMemcache->close();
		} else {
			$this->getCatList_DB();
		}

//		foreach ($this->aGroupCat as $key => $val) {
//
//		}
//		var_dump(array_values($this->aGroupCat));
//		print_r($this->aGroupCat);
	}

	public function getCatList_DB ()
	{
		$query = "SELECT * FROM icons";
		$res = mysql_query($query);

		$aCat = array();
		$aNCat = array();
		$ids_scat = array();
		if (mysql_num_rows($res))
			while ($row = mysql_fetch_array($res)) {

				if (! isset($aNCat[$row['parent_id']])) {
					$aNCat[$row['parent_id']] = array('name' => $row['category'], 'use_id' => $row['use_id'], 'icon' => $row['icon'], 'count' => 0, 'sub_cat' => array());
				}

				if (trim($row['subcategory2']))
					$level = 2;
				else if (trim($row['subcategory']))
					$level = 1;
				else
					$level = 0;

				if ($level) {
					$id = isset($ids_scat[$row['parent_id'].$row['subcategory']]) ? $ids_scat[$row['parent_id'].$row['subcategory']] : null;
					if ($id)
						$aNCat[$row['parent_id']]['sub_cat'][$id]['sub_cat'][$row['id']] = array('id' => $row['id'], 'use_id' => $row['use_id'], 'pid' => $row['parent_id'], 'name' => $row['subcategory2'], 'icon' => $row['icon']);
					else {
						$id = $row['id'];
						$aNCat[$row['parent_id']]['sub_cat'][$id] = array('id' => $id, 'use_id' => $row['use_id'], 'pid' => $row['parent_id'], 'name' => $row['subcategory'], 'icon' => $row['icon'], 'sub_cat' => array());
						if ($level == 2) {
							$aNCat[$row['parent_id']]['sub_cat'][$id]['sub_cat'][$id] = array('id' => $row['id'], 'use_id' => $row['use_id'], 'pid' => $row['parent_id'], 'name' => $row['subcategory2'], 'icon' => $row['icon']);
						}
						$ids_scat[$row['parent_id'].$row['subcategory']] = $id;
					}
					$aNCat[$row['parent_id']]['count']++;
				}

				$aCat[$row['id']] = array(
					'use_id' => $row['use_id'],
					'cat' => $row['category'],
					'icon' => $row['icon'],
					'subcat' => $row['subcategory'],
					'subcat2' => $row['subcategory2'],
					'parent' => $row['parent_id'],
					'level' => $level
				);
			}

		$this->aGroupCat = $aNCat;
		$this->aCatList = $aCat;
		return $aCat;
	}

	public function getSubCategories ($id) {
		$aSubCats = array();
		foreach ($this->aCatList as $key => $cat) {
			if ($cat['parent'] == $id && $cat['level'] == 1)
				$aSubCats[$key] = $cat;
		}
		return $aSubCats;
	}
	public function getSubSubCategories ($id, $subcat_name) {
		$aSubCats = array();
		foreach ($this->aCatList as $key => $cat) {
			if ($cat['parent'] == $id && $subcat_name == $cat['subcat2'] && $cat['level'] == 2)
				$aSubCats[$key] = $cat;
		}
		return $aSubCats;
	}

	public function getParentCatsByID ($id)
	{
		if (! isset($this->aCatList[$id]))
			return array();

		$aParentCats = array();
		foreach ($this->aCatList as $key => $val) {
			if ($key == $id || $val['parent'] == $id) {
				$aParentCats[] = $key;
			} else if (strtolower($this->aCatList[$id]['subcat']) == strtolower($val['subcat']) && $this->aCatList[$id]['subcat'] != '') {
				$aParentCats[] = $key;
			}
		}

		return $aParentCats;
	}

	public function getParentCatsByIDs ($aId = array(), $aExId = array()) {
		$aParentCats = array();

		foreach ($this->aCatList as $key => $val) {
			if (in_array($key, $aExId))
				continue;

			if (in_array($key, $aId) || in_array($val['parent'], $aId)) {
				$aParentCats[] = $key;
			} else  {
				for ($i = 0, $c = count($aId); $i < $c; $i++) {
					if (isset($this->aCatList[$aId[$i]])) {
						$act_cat = $this->aCatList[$aId[$i]];

						if (! trim($act_cat['subcat2']))
							if (strtolower($act_cat['subcat']) == strtolower($val['subcat']) && $val['level'] == 2) {
								$aParentCats[] = $key;
							}
					}

				}
			}
		}

		return $aParentCats;
	}

	public function getIconByID ($id_cat) {
		$id_use_cat = $this->aCatList[$id_cat]['use_id'];
		return $this->aCatList[$id_use_cat]['icon'];
	}

	public function getCatsByName ($sName, $search='cat')
	{
		$aCatsID = array();
		switch ($search) {
			case "all" :
				foreach ($this->aCatList as $key => $val) {
					if ($val['cat'] == $sName || $val['subcat'] == $sName || $val['subcat2'] == $sName)
						$aCatsID[] = $key;
				}
				break;

			default :
				if ($search != 'subcat' && $search != 'subcat2')
					$search = 'cat';

				foreach ($this->aCatList as $key => $val) {
					if ($val[$search] == $sName)
						$aCatsID[] = $key;
				}
		}

		return count($aCatsID) ? $this->getParentCatsByIDs($aCatsID) : array();
	}

	public function defaultCategory () {
		return $this->getParentCatsByIDs($this->aDefaultCat, $this->aDefaultCatEx);
	}


	public function findCat ($cat_id, $finds) {
		if (in_array($cat_id, $finds)) {
			return $this->aGroupCat[$cat_id]['count'];
		}

		$c = 0;
		foreach ($this->aGroupCat[$cat_id]['sub_cat'] as $key => $val) {
			if (in_array($key, $finds)) {
				$c++;
			}
		}
		return $c;
	}
}
