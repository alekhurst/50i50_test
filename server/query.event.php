<?php
require_once 'config.php';

if (! isset($_REQUEST['id']) && ! intval($_REQUEST['id']))
	exit;
$format = array('%m/%d/%Y', '%m/%Y', '%Y');

$query = "SELECT timeline_2012.id_evento
	 , timeline_2012.titulo
     , timeline_2012.descripcion
     , timeline_2012.imagen

     , DATE_FORMAT(timeline_2012.f_inicio, IF (timeline_2012.scale & 8 = 8, '" . $format[0] . "', IF (timeline_2012.scale & 16 = 16, '" . $format[1] . "', '" . $format[2] . "'))) AS f_inicio
	 , TIME_FORMAT(timeline_2012.h_inicio, IF (timeline_2012.scale & 4 = 4, '%H:%i:%S', '')) as h_inicio

	 , DATE_FORMAT(timeline_2012.f_fin, IF (timeline_2012.scale & 8 = 8, '" . $format[0] . "', IF (timeline_2012.scale & 16 = 16, '" . $format[1] . "', '" . $format[2] . "'))) AS f_fin
	 , TIME_FORMAT(timeline_2012.h_fin, IF (timeline_2012.scale & 4 = 4, '%H:%i:%S', '')) as h_fin

     , timeline_2012.f_tipo
     , timeline_2012.etiquetas
     , timeline_2012.lugar
     , timeline_2012.lugar2
     , timeline_2012.source
     , timeline_2012.latitud
     , timeline_2012.longitud
     , timeline_2012.latitud2
     , timeline_2012.longitud2
     , icons.category
     , icons.subcategory
     , icons.subcategory2
     , timeline_2012.fk_user
     , timeline_2012.link_video
     , timeline_2012.link_wiki
     , timeline_2012.link
FROM
  timeline_2012
  LEFT JOIN icons ON timeline_2012.category_id = icons.id
WHERE
  timeline_2012.id_evento = " . (int) $_REQUEST['id'] . " LIMIT 1";

$res = mysql_query($query);
if (mysql_num_rows($res)) {
	$row = mysql_fetch_assoc($res);

	$row['titulo'] = htmlentities($row['titulo'], ENT_COMPAT, 'UTF-8');
	if (trim($row['link']))
		$row['titulo'] = '<a href="' . $row['link'] . '" target="_blank">' . $row['titulo'] . '</a>';
// Date Start
	$date_start = $row['f_inicio'];
	$time_int =  strtotime($row['h_inicio']);
	if ($row['h_inicio'] && $time_int != strtotime('00:00:00')) {
		$date_start .= ' ' . $row['h_inicio'];
	}
	if ($date_start != '00/00/0000' && $date_start != '00/0000' && $date_start != '0000')
		$row['f_inicio'] = '<a href="javascript:void(0)" class="link_set_date">' . $date_start . '</a>';
	else
		$row['f_inicio'] = 'none';
// Date Finish
	$date_finish = $row['f_fin'];
	$time_int =  strtotime($row['h_fin']);
	if ($row['h_fin'] && $time_int != strtotime('00:00:00')) {
		$date_finish .= ' ' . $row['h_fin'];
	}
	if ($date_finish != '00/00/0000' && $date_finish != '00/0000' && $date_finish != '0000')
		$row['f_fin'] = '<a href="javascript:void(0)" class="link_set_date">' . $date_finish . '</a>';
	else
		$row['f_fin'] = 'none';
// Place
	if (trim($row['lugar'])) {
		if (isset($row['latitud']) || isset($row['longitud']))
			$row['lugar'] .= '<input type="hidden" value="' . floatval($row['latitud']) .';' . $row['longitud'] . '" />';
		$row['lugar'] = '<a href="javascript:void(0);" class="link_to_place">' . $row['lugar'] . '</a>';
	}
	if (trim($row['lugar2'])) {
		if (isset($row['latitud2']) || isset($row['longitud2']))
			$row['lugar2'] .= '<input type="hidden" value="' . floatval($row['latitud2']) .';' . $row['longitud2'] . '" />';
		$row['lugar2'] = '<a href="javascript:void(0);" class="link_to_place">' . $row['lugar2'] . '</a>';
	}



	$tags_to_display = array();
	$tags = explode(',', $row['etiquetas'] );
	$count_tags = count($tags);
	for ($i = 0; $i < $count_tags; $i++) {
		$res_teg = explode(';', $tags[$i]);
		for ($j = 0; $j < count($res_teg); $j++)
			$tags_to_display[] = '<a href="javascript:void(0)" class="filter_to_tag">' . trim($res_teg[$j]) . "</a>";
	}
	$row['etiquetas'] = $tags_to_display;
	$row['source'] = $row['source'] ? $row['source'] : "50I50";

	$row['multi_web'] = $row['link_wiki'] ? $row['link_wiki'] : ($row['link'] ? $row['link'] : '');

	echo json_encode($row);
} else
	echo json_encode(array('error'=>'not Event'));