<?php

class ModelView
{
	public $slistEvents;
	public $iWidthSubPeriod = '';
	public $type = 'json';

	private $queryResult = '';

	public $myQuery;

	function __construct($sDate, $sBlock, $iWidthSubPer) {
		$this->slistEvents = '';
		$this->iWidthSubPeriod = $iWidthSubPer;

		$this->myQuery = new newFormatDate();
		$this->myQuery->genDateForSQL($sDate);

		$events = null;


		$this->myQuery->whereCoordinate();
		switch ($sBlock) {
			case 'grid' :
				$this->listFormatForGrid();
				break;

			case 'map' :
				$this->listFormatForSpace();
				break;

			case 'mobile-map' :
				$this->listFormatForMobileSpace();
				break;

			case 'mobile-list' :
				$this->type = 'html';
				$this->listFormatForMobileList();
				break;

			case 'fr' :
				$this->listFormatForFilterResult();
				break;

			case 'fd' :
				$this->myQuery->getWhereForSelect();
				$this->listFormatForFilterResult();
				break;

			default :
				$this->type = 'html';
				$this->myQuery->getWhereForSelect();

				$this->queryResult = $this->myQuery->selectEventsCountForLine(4);
				if ($this->queryResult) {
					$this->listFormatEventsCountForLine();

					$this->queryResult = $this->myQuery->selectEventsForLine(4);
					if ($this->queryResult)
						$this->listFormatEventsForLine();
				}
		}
		return $this->slistEvents;
	}

	function listFormatEventsForLine () {
		$close_block = (isset($user) && $user) ? '<a class="block_close" href="#"></a>' : '';

		while ($row = mysql_fetch_assoc($this->queryResult)) {
			$arr_time = explode(':', $row['h_inicio']);

			$int_time = mktime($arr_time[0], $arr_time[1], $arr_time[2]);
			$time_int = strtotime($row['h_inicio']);
			if ($int_time == mktime(0, 0, 0))
				$row['h_inicio'] = '';
			else
				$row['h_inicio'] = ' - ' . $row['h_inicio'];
// <img src="./server/thumbnails.php?w=55&h=50&q=50&p=1&url='.$row['imagen'].'&p=1&null=1" alt="" class="fr" />
// background-image: url(' . './time_icons/' . $row['icon'] . ');
			$this->slistEvents .= '
<div class="event c-' . $this->myQuery->getCorrectedSubitem($row['subitem']) . '"
	style="background-image: url(' . PROTOCOL . URL_PARENT . 'time_icons/' . $this->myQuery->getCatIconByID($row['cat_id']) . '); top:' . (70*$row['rownum']) . 'px; left: ' . (8+($this->iWidthSubPeriod) * $this->myQuery->getCorrectedSubitem($row['subitem']) ) . 'px;">
	<div class="block">
		<input type="hidden" class="event_id" value="' . $row['id'] . '" />
		' . $close_block . '
		<h1 class="title">
			' . $this->generateImage ($row['imagen'], 55, 50, 50, 1, '', 'fr', 1) . '
			' . get_encoding_utf8($row['title']) . '
		</h1>
		<p class="date">'. $row['start_date'] . ' ' . $row['h_inicio'] .  '</p>
		<div class="clear"></div>
	</div>
</div>';
		}
		$this->queryResult = false;
	}

	function listFormatEventsCountForLine () {
		while ($row = mysql_fetch_assoc($this->queryResult)) {
			if ($row['num'] > 0)
//				$this->slistEvents .= '<div class="claster" style="left:' . (5 + $this->myQuery->getCorrectedSubitem($row['subitem']) * $this->iWidthSubPeriod) . 'px">' . $row['num'] . '</div>' ;
				$this->slistEvents .= '<input class="claster" type="button" style="left:' . (5 + $this->myQuery->getCorrectedSubitem($row['subitem']) * $this->iWidthSubPeriod) . 'px" value="' . $row['num'] . '" />' ;
		}
	}

	function listFormatForGrid () {
		if (! isset($_REQUEST['sEcho']))
			return false;
		$this->slistEvents = array('sEcho' => $_REQUEST['sEcho'],'iTotalRecords' => 0, 'iTotalDisplayRecords' => 0, 'aaData' => array());

		$this->myQuery->getWhereForSelect();

		$events = array();
		$this->queryResult = $this->myQuery->selectEventsForGrid();

		$count = $this->myQuery->selectEventsCountForGrid();
		if (! $count)
			return false;

		if ($this->queryResult)
			while ($row = mysql_fetch_assoc($this->queryResult)) {
				$cat = $row['category'];
				if (trim($row['subcategory'])) {
					$cat .= ' > ' . $row['subcategory'];
					if (trim($row['subcategory2']))
						$cat .= ' > ' . $row['subcategory2'];
				}

				$events[] = array(
					'<a href="' . $row['imagen'] . '" target="_blank">' . $this->generateImage ($row['imagen'], 100, 100, 60, 1) . '</a>',
//					'<a href="' . $row['imagen'] . '" target="_blank"><img alt="' . $row['titulo'] . '" src="./server/thumbnails.php?w=100&h=100&p=1&q=60&url=' . $row['imagen'] . '" width="100" /></a>',
					get_encoding_utf8($row['titulo']),
					get_encoding_utf8($row['descripcion']),
					$row['f_inicio'] . '<br />' . $row['h_inicio'],
					$row['lugar'],
					$cat,
					$row['etiquetas'],
					'<a href="' . $row['link'] . '" target="_blank">' . $row['titulo'] . '</a>'
				);
			}

		$this->slistEvents = array('sEcho' => $_REQUEST['sEcho'], 'iTotalRecords' => count($events), 'iTotalDisplayRecords' => $count, 'aaData' => $events);
	}

	function listFormatForSpace () {
		$this->myQuery->whereCoordinate();
		$this->myQuery->getWhereForSelect();


		$this->queryResult = $this->myQuery->selectEventsForMobileSpace();

		if (isset($_REQUEST['count_events'])) {
			$count = $this->myQuery->selectEventsCountForGrid();
			if (! $count) {
				$this->slistEvents = array('iTotalRecords' => 0, 'events_array' => array());
				return false;
			} else if ($count > 10000) {
				$this->slistEvents = array('iTotalRecords' => $count, 'events_array' => array());
				return false;
			}
		}

		$aEvents = array();
		if ($this->queryResult) {
			while($row = mysql_fetch_assoc($this->queryResult)){
				$row['titulo'] = get_encoding_utf8($row['titulo']);
				$row['imagen'] = $this->generateImage ($row['imagen'], 55, 50, 50, 1, '', 'fr', 1);
				$row['icon'] = $this->myQuery->getCatIconByID($row['category_id']);
				$aEvents[] = $row;
			}
		}

		$this->slistEvents = array('iTotalRecords'=> count($aEvents), 'events_array' => $aEvents);
	}

	function listFormatForMobileSpace () {

		$s = isset($_REQUEST['search']) ? $_REQUEST['search'] : null;

		if ($s) {
			$this->myQuery->aWhere = array(" (MATCH (timeline_2012.titulo,
				timeline_2012.descripcion,
				timeline_2012.etiquetas) AGAINST ('\"" . $this->myQuery->escapeString($s) . "\"' IN BOOLEAN MODE)
				OR (users.username LIKE '" . $_REQUEST['search'] . "%' OR users.email LIKE '" . $_REQUEST['search'] . "%')
				OR (icons.category LIKE '" . $_REQUEST['search'] . "%' OR icons.subcategory LIKE '" . $_REQUEST['search'] . "%' OR icons.subcategory2 LIKE '" . $_REQUEST['search'] . "%')
			)");
		} else {
			$this->myQuery->aWhere = array(' timeline_2012.category_id NOT IN (11) ');
		}

		$where = '';

		$private = (isset($_REQUEST['privacy']) && is_array($_REQUEST['privacy'])) ? $_REQUEST['privacy'] : array();
// Private View
		if (isset($private[1]) && $private[1] == 'true') {
			$user= isset($_SESSION['USER_INFO']) ? $_SESSION['USER_INFO'] : array();



			$where = '(privacy = 1 AND user_id =' . (isset($user['id']) ? $user['id'] : -1) . ')';
		}
// Public View
		if (isset($private[0]) && $private[0] == 'true') {
			$where = ' (privacy = 0 ' . ($where ? ' OR ' . $where : '') . ') ';
		} else if (!$where) {
			$where = ' privacy = 0 ';
		}
		$this->myQuery->aWhere[] = $where;

		$this->myQuery->whereCoordinate();
		$this->myQuery->getMobileWhere();
//		$this->myQuery->mobileFilter();

		$this->queryResult = $this->myQuery->selectEventsForMobileSpace();

		$aEvents = array();
		if ($this->queryResult) {
			while($row = mysql_fetch_assoc($this->queryResult)){
				$row['titulo'] = get_encoding_utf8($row['titulo']);
				$row['imagen'] = $row['imagen'] ? $this->generateImage($row['imagen'], 55, 50, 50, 1, '', 'fr', 1) : '<img width="50" class="fr" src="' . (PROTOCOL . URL_MOBILE . 'img/thumb.png') . '" />';
				$row['icon'] = $this->myQuery->getCatIconByID($row['category_id']);
				$aEvents[] = $row;
			}
		}

		$this->slistEvents = array('events_array' => $aEvents);
	}


	function listFormatForMobileList () {
		$s = isset($_REQUEST['search']) ? $_REQUEST['search'] : null;

		if ($s) {
			$this->myQuery->aWhere = array(" (MATCH (timeline_2012.titulo,
				timeline_2012.descripcion,
				timeline_2012.etiquetas) AGAINST ('\"" . $this->myQuery->escapeString($s) . "\"' IN BOOLEAN MODE)
				OR (users.username LIKE '" . $_REQUEST['search'] . "%' OR users.email LIKE '" . $_REQUEST['search'] . "%')
				OR (icons.category LIKE '" . $_REQUEST['search'] . "%' OR icons.subcategory LIKE '" . $_REQUEST['search'] . "%' OR icons.subcategory2 LIKE '" . $_REQUEST['search'] . "%')
			)");
		} else {
			$this->myQuery->aWhere = array(' timeline_2012.category_id NOT IN (11) ');
		}

		$where = '';

		$private = (isset($_REQUEST['privacy']) && is_array($_REQUEST['privacy'])) ? $_REQUEST['privacy'] : array();
// Private View
		if (isset($private[1]) && $private[1] == 'true') {
			$user= isset($_SESSION['USER_INFO']) ? $_SESSION['USER_INFO'] : array();



			$where = '(privacy = 1 AND user_id =' . (isset($user['id']) ? $user['id'] : -1) . ')';
		}
// Public View
		if (isset($private[0]) && $private[0] == 'true') {
			$where = ' (privacy = 0 ' . ($where ? ' OR ' . $where : '') . ') ';
		} else if (!$where) {
			$where = ' privacy = 0 ';
		}
		$this->myQuery->aWhere[] = $where;

		$this->myQuery->whereCoordinate();
		$this->myQuery->getMobileWhere();

		$this->queryResult = $this->myQuery->selectEventsForMobileList();

		if ($this->queryResult) {
			while($row = mysql_fetch_assoc($this->queryResult)){
				$d = explode('-', $row['f_inicio']);
				$t = explode(':', $row['h_inicio']);

				$time = mktime($t[0], $t[1], $t[2], $d[1], $d[2], $d[0]);
				$date = array($d[0], $d[1], $d[2], $t[0], $t[1], $t[2]);

				echo '<li id="ev_' . $row['id_evento'] . '" data-id="' . $row['id_evento'] . '">
	<a href="#" data-ln="' . $row['latitud'] . '" data-lg="' . $row['longitud'] . '" data-date="' . implode(',', $date) . '">
		<span class="date_sort" style="display: none;">' . date('l, F d, Y', $time) . '</span>
		<img src="' . ($row['imagen'] ? $row['imagen'] : PROTOCOL . URL_MOBILE . 'img/thumb.png') . '">
		<h2>' . $row['titulo'] . '</h2>
		<p><strong>' . $row['lugar'] . '</strong></p><p>' . $row['descripcion'] . '</p>
		<p class="ui-li-aside"><strong>' . date('h:i', $time) . '</strong> ' . date('A', $time) . '</p>
	</a>
</li>';
			}
		}
	}

	function listFormatForFilterResult () {
		if (! isset($_REQUEST['sEcho']))
			return false;

		$events = array();
		$this->slistEvents = array('sEcho' => $_REQUEST['sEcho'],'iTotalRecords' => 0, 'iTotalDisplayRecords' => 0, 'aaData' => $events);

		$this->queryResult = $this->myQuery->selectFormatForFilterResult();

		$count = $this->myQuery->selectEventsCountForGrid();
		if (! $count)
			return false;

		if ($this->queryResult)
			while ($row = mysql_fetch_assoc($this->queryResult)) {
				$title = get_encoding_utf8($row['titulo']);
				$desc = get_encoding_utf8($row['descripcion']);
				if (strlen($row['descripcion']) > 150)
					$desc = substr($desc, 0, 150) . ' (...)';

				$start = 'none';
				if ($row['start_date'] != '00/00/0000' && $row['start_date'] != '00/0000' && $row['start_date'] != '0000')
					$start = '<a class="link_set_date" href="#">' . $row['start_date'] . ($row['h_inicio'] != '00:00:00' ? (' ' . $row['h_inicio']) : '') .  '</a>';

				if (trim($row['lugar'])) {
					$lugar = $row['lugar'];
					if (isset($row['latitud']) || isset($row['longitud']))
						$lugar .= '<input type="hidden" value="' . floatval($row['latitud']) .';' . $row['longitud'] . '" />';
					$start .= ', <a href="#" class="link_to_place">' . $lugar . '</a>';
				}


				$finish = 'none';
				if ($row['finish_date'] != '00/00/0000' && $row['finish_date'] != '00/0000' && $row['finish_date'] != '0000')
					$finish = '<a class="link_set_date" href="#">' . $row['finish_date'] . ($row['h_fin'] != '00:00:00' ? (' ' . $row['h_fin']) : '') . '</a>';
//
				if (trim($row['lugar2'])) {
					$lugar2 = $row['lugar2'];
					if (isset($row['latitud2']) || isset($row['longitud2']))
						$lugar2 .= '<input type="hidden" value="' . floatval($row['latitud2']) .';' . $row['longitud2'] . '" />';
					$finish .= ', <a href="#" class="link_to_place">' . $lugar2 . '</a>';
				}
				$cat = $this->myQuery->getCatByID($row['category_id']);
				$sub1cat = $cat['subcat'] != '' ? '<p> Sub1category: ' . $cat['subcat'] . '</p>' : '';
				$sub2cat = $cat['subcat2'] != '' ? '<p> Sub2category: ' . $cat['subcat2'] . '</p>' : '';

				$tags_to_display = array();
				if (trim($row['etiquetas'])) {
					$tags = explode(',', $row['etiquetas'] );
					$count_tags = count($tags);
					for ($i = 0; $i < $count_tags; $i++) {
						$res_teg = explode(';', $tags[$i]);
						for ($j = 0; $j < count($res_teg); $j++)
							if (trim($res_teg[$j]))
								$tags_to_display[] = '<a href="javascript:void(0)" class="filter_to_tag">' . trim($res_teg[$j]) . "</a>";
					}
				}

				$events[] = array(0 => '<div class="ev">
<h1><a class="link_set_date_title" href="#">' . $title . '</a></h1><p>' . $desc . '</p>
				' . $this->generateImage ($row['imagen'], 75, 70, 50, 1, $title, 'fr') . '
<p>Date type: ' . $row['f_tipo'] . '</p>
<p>Start: ' . $start . '</p>
<p>End: <a class="link_set_date" href="javascript:void(0);">' . $finish .  '</a></p>
<img class="fl" src="' . PROTOCOL . URL_PARENT . 'time_icons/' . $cat['icon']  .'" alt="' . $cat['cat'] . '"/>
<p>Category: ' . $cat['cat'] . '</p>' . $sub1cat . $sub2cat . '<div class="clear"></div>
<p>Tags: ' . implode(', ', $tags_to_display) . '</p>
<p><b>Source:</b> ' . (trim($row['source']) ? $row['source'] : "50I50") . '</p>
<p class="fr"><a class="more" href="javascript:OpenEvent(' . $row['id_evento'] . ')">more...</a></p>
<p class="clear"></p></div>');

			}

		$this->slistEvents = array('sEcho' => $_REQUEST['sEcho'],'iTotalRecords' => count($events), 'iTotalDisplayRecords' => $count, 'aaData' => $events);

		write_arr_log(($this->slistEvents));
	}

	function listHTML() {
		if ($this->type == 'json') {
			if (isset($_GET['callback'])) {
				echo $_GET['callback'] . '(' . json_encode($this->slistEvents) . ')';
			} else {
				echo json_encode($this->slistEvents);
			}
		} else {
			echo $this->slistEvents;
		}
	}

	function generateImage ($url, $w, $h, $q=50, $p=1, $title='', $class='', $null = 0) {
		$rand_server = 'img' . rand(1,10) . '.';

		return '<img class="' . $class . '" src="' . PROTOCOL . $rand_server . URL_PARENT . '/server/thumbnails.php?w=' . $w . '&h=' . $h . '&q=' . $q . '&p=' . $p . '&url=' . urlencode($url) . '&null=' . $null . '" alt="' . $title . '"/>';
	}
}
//<img class="fr" src="./server/thumbnails.php?w=75&h=70&q=50&url='.$row['imagen'].'" alt="' . $title . '"/>
//	<img class="fr" src="' . PROTOCOL . 'img' . $rand_server . '.' . SITE_URL . '/server/thumbnails.php?w=75&h=70&q=50&p=1&url='.$row['imagen'].'" alt="' . $title . '"/>