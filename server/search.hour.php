<?php
class newFormatDate extends ModelQuery
{
	function __construct()
	{
		$this->sSelectEvent = $this->tb_timeline . '.h_inicio';
		parent::__construct();
	}

	public function getDateForSelect()
	{
		return " MINUTE(" . $this->sSelectEvent . ") ";
	}

	public function getWhereForSelect()
	{

		if (isset($this->aDate[3]))
			$this->aDate[3] = $this->aDate[3] < 9 ? '0' . strval($this->aDate[3]) : $this->aDate[3];
		else
			$this->aDate[3] = "00";

		if ($this->iSearchYear < 0 || $this->iSearchYear > 9999)
			$this->aWhere[] = " (" . $this->tb_timeline . ".year = " . $this->iSearchYear . "
				AND MONTH(" . $this->tb_timeline . ".f_inicio) = " . $this->aDate[1] . "
				AND DAY(" . $this->tb_timeline . ".f_inicio) = " . $this->aDate[2] . ") ";
		else
			$this->aWhere[] = $this->tb_timeline . ".f_inicio = '" . $this->yearToStr() . '-' . $this->aDate[1] . '-' . $this->aDate[2] . "'";

		$this->aWhere[] = "(timeline_2012.h_inicio LIKE '" . $this->aDate[3] . ":__:__' OR timeline_2012.h_inicio LIKE '" . $this->aDate[3] . ":__') AND " . $this->tb_timeline . ".scale & 4 = 4";

	}

	/**
	 * Search events =
	 * if (event_time_start < search_time_end) && (event_time_end >= search_time_start)
	 *
	 *
	 */
	public function getMobileWhere()
	{
		$date = $this->aDate[0] . '-' . $this->aDate[1] . '-' . $this->aDate[2];
//		$time = $this->aDate[3] . ':' . $this->aDate[4] . ':' . $this->aDate[5];
		$time_start = $this->aDate[3] . ':00:00';
		$time_end = ($this->aDate[3] + 1) . ':00:00';
		$mktime = mktime($this->aDate[3], $this->aDate[2], $this->aDate[3], $this->aDate[1], $this->aDate[2], $this->aDate[0]);
		$days_in_year = date("L", $mktime) ? 367 : 366;

//		// Timezone
//		if (isset($this->aDate[6])) {
////			$mktime = $this->aDate[6] * 3600;
//		}

//		var_dump(date('Y-m-d H:i:s', $mktime));
		$this->aWhere[] = "
(
	(
		`repeat` = 0 AND
		(
			(timeline_2012.f_inicio < '" . $date . "' AND timeline_2012.f_fin > '" . $date . "') OR
			((timeline_2012.f_inicio = '" . $date . "' OR timeline_2012.`repeat` = 1) AND timeline_2012.h_inicio < '" . $time_end . "')
		)
		AND
		(
			timeline_2012.f_fin > '" . $date . "' OR
			((timeline_2012.f_fin = '" . $date . "' OR timeline_2012.`repeat` = 1) AND timeline_2012.h_fin >= '" . $time_start . "')
		)
	)
	OR
	(
		timeline_2012.f_fin = '0000-00-00'
		AND (
			(timeline_2012.f_inicio = '" . $date . "' OR timeline_2012.`repeat` = 1)
			AND
			(
				(timeline_2012.h_inicio >= '" . $time_start . "' AND timeline_2012.h_inicio < '" . $time_end . "')
				OR timeline_2012.h_inicio = '00:00:00'
			)
		)
	) OR (
		start_time <= '$mktime'  AND (
			(`repeat` = 1 AND ($mktime - start_time) % 86400 < live_time)
			OR (`repeat` = 2 AND ($mktime - start_time) % 604800 < live_time)
			OR (`repeat` = 3 AND ($mktime - start_time) % " . 86400 * date('t', $mktime) . " < live_time)
			OR (`repeat` = 4 AND ($mktime - start_time) % " . 86400 * $days_in_year . " < live_time)
		)
	)
)";
		$this->aWhere[] = $this->tb_timeline . ".scale & 4 = 4";
	}

}
