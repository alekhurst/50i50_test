<?php
require_once 'config.php';

$_SESSION['TIME_START'] = getmicrotime();
$_SESSION['TIME_START_PERIOD'] = $_SESSION['TIME_START'];
write_log("\n\nStart=" . date('H:i:s:u'));


$date = isset($_GET['date']) ? trim($_GET['date']) : date('Y,m,d,h,i,s');
$block = isset($_GET['block']) ? $_GET['block'] : 'line';

$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'day';


$search = isset($_REQUEST['search']) ? $_REQUEST['search'] : '';
$width = isset($_REQUEST['width']) ? $_REQUEST['width'] + 6 : 40;



$events = false;

require_once 'ModelQuery.php';

switch ($type) {
	case 'century' : require_once 'search.century.php'; break;
	case 'year' : require_once 'search.year.php'; break;
	case 'decade' : require_once 'search.decade.php'; break;
	case 'month' : require_once 'search.month.php'; break;
	case 'hour' : require_once 'search.hour.php'; break;
	default : require_once 'search.day.php';

}

require_once 'ModelView.php';
require_once 'Category.php';

$view = new ModelView($date, $block, $width);

$view->listHTML();
mysql_close($db);

write_log("End=" . date('H:i:s u'));
write_log("\nAll Time =" . number_format((getmicrotime() - $_SESSION['TIME_START']), 5, '.', ','));
exit;









// +++++++++++++++++++++++++++++++++++++++++++++++++++++++ For Test
/* $_SESSION['TIME_START'] = getmicrotime(); $_SESSION['TIME_START_PERIOD'] = $_SESSION['TIME_START']; write_log('Start'); */
//write_log('All Time =' . number_format((getmicrotime() - $_SESSION['TIME_START']), 5, '.', ','));
//function _d ($param = false) {
//	echo "<xmp>";
//	var_dump($param);
//	echo "</xmp>";
//
//	return false;
//}
