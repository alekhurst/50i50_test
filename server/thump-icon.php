<?php
$icon_name = isset($_GET['icon']) ? $_GET['icon'] : false;
$icon_dir = '../img/icons/';

if (! $icon_name || ! file_exists($icon_dir . $icon_name))
	$icon_name = 'default.png';

$file = @file_get_contents($icon_dir . $icon_name);
header("Content-type: image/png");
echo $file;
exit;
