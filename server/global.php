<?php
ini_set('date.timezone', 'Europe/Helsinki');
session_set_cookie_params(3600*24);
session_start();

define('SITE_VERSION', 'beta');

define('FOLDER_GEOKRONO', 'geokrono');
define('FOLDER_MOBILE', 'mobile');

define('PROTOCOL', 'http://');
define('URL_PARENT',    '50i50.org/website/');
//define('URL_PARENT',    'timeline.work/');
define('URL_GEOKRONO',  URL_PARENT . FOLDER_GEOKRONO . '/');
define('URL_MOBILE',    URL_PARENT . FOLDER_MOBILE . '/');

define('ABS_PATH', dirname(__FILE__) . '/../');
define('PATH_GEOKRONO', ABS_PATH . FOLDER_GEOKRONO . '/');
define('PATH_MOBILE', ABS_PATH . FOLDER_MOBILE . '/');

define('MOBILE_COOKIE', '50I50_MOBILE_PARAMS');

define('PUBLIC_FILE_DIR', ABS_PATH . 'public/images/');
define('PUBLIC_FILE_URL', PROTOCOL . URL_PARENT . 'public/images/');

define('SCALED_IMG_WIDTH', 240);
define('SCALED_IMG_HEIGHT', 240);
define('SCALED_IMG_CROP', true);

define('GIGYA_KEY', '3_ooDLQLi5i8R8U7x7zx3Rcdw1QE1ftqJ-WZCbidDBi9satiIONDyqng39HYTms_wL');
//define('GIGYA_KEY', '3_MjFNjn1kitYW6BvD2WZeguXZ7Id03JHiq69Qw5kjFRD_p9JALQehhlNnth8fckeh');


define('SLIDESHOW_COOKIE', 'SLIDESHOW_STATUS');

// Default Text in Page
define('FILTER_TITLE_VAL_DEF', 'Title');
define('FILTER_DESC_VAL_DEF', 'Description');

# MAil SMTP
define('SMTP_USERNAME', 'info@50i50.org');
define('SMTP_PASSWORD', 'y6dRNW_khFuYhq53xpKVXA');
define('SMTP_HOST', 'smtp.mandrillapp.com');
define('SMTP_PORT', 587);

// Data Base
$config['db']['host'] = 'localhost';
$config['db']['username'] = 'pavel';
$config['db']['password'] = 'cTqP9XvpFj2QrHmz';
$config['db']['database'] = '50i50_timespace';

global $MOBILE_VIEW_CATEGORIES, $MOBILE_CATEGORIES_HIDE;
$MOBILE_CATEGORIES_SHOW = range(1,11600);
$MOBILE_CATEGORIES_HIDE = range(1000,1279);
