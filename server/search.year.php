<?php

class newFormatDate extends ModelQuery
{

	function __construct() {
		$this->sSelectEvent = $this->tb_timeline . '.f_inicio';
		parent::__construct();
	}

	public function getDateForSelect () {
		return " MONTH(" . $this->sSelectEvent . ")-1 ";
	}

	public function getWhereForSelect () {
		if ($this->iSearchYear < 0 || $this->iSearchYear > 9999)
			$this->aWhere[] = " (" . $this->tb_timeline . ".year = " . $this->iSearchYear . ") ";
		else
			$this->aWhere[] = $this->tb_timeline . ".f_inicio BETWEEN  '". $this->yearToStr() ."-1-1' AND '". $this->yearToStr() ."-12-31'";
		$this->aWhere[] = $this->tb_timeline . ".scale & 64 = 64";
	}

}