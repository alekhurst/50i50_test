<?php
class newFormatDate extends ModelQuery
{

	function __construct()
	{
		$this->sSelectEvent = $this->tb_timeline . '.h_inicio';
		parent::__construct();
	}

	public function getDateForSelect()
	{
		return " HOUR(" . $this->sSelectEvent . ") ";
	}

	public function getWhereForSelect()
	{
		if ($this->iSearchYear < 0 || $this->iSearchYear > 9999)
			$this->aWhere[] = " (" . $this->tb_timeline . ".year = " . $this->iSearchYear . "
				AND MONTH(" . $this->tb_timeline . ".f_inicio) = " . $this->aDate[1] . "
				AND DAY(" . $this->tb_timeline . ".f_inicio) = " . $this->aDate[2] . ") ";
		else {
			$date = $this->yearToStr() . '-' . $this->aDate[1] . '-' . $this->aDate[2];
			$this->aWhere[] = $this->tb_timeline . ".f_inicio = '" . $date . "'";
		}
		$this->aWhere[] = $this->tb_timeline . ".scale & 8 = 8";
	}

	public function getMobileWhere()
	{
		$date = $this->aDate[0] . '-' . $this->aDate[1] . '-' . $this->aDate[2];
		$mktime = mktime(0,0,0,$this->aDate[1],$this->aDate[2],$this->aDate[0]);
		$day_of_week = date('w', $mktime) + 1;
		$day_of_month = date('j', $mktime);
		$day_of_year = date('z', $mktime);
		$days_in_month = date("t");
		$days_in_year = date("L") ? 367 : 366;

		$this->aWhere[] = "(
	(timeline_2012.f_inicio <= '" . $date . "' AND (timeline_2012.f_fin >= '" . $date . "' OR (timeline_2012.f_fin = '0000-00-00' AND timeline_2012.f_inicio = '" . $date . "')))
#DAY
	OR (timeline_2012.`repeat` = 1)
#WEEK
	OR (timeline_2012.`repeat` = 2 AND (
		(DAYOFWEEK(f_inicio) <= $day_of_week AND (DAYOFWEEK(f_inicio) + TIMESTAMPDIFF(DAY, f_inicio, f_fin)) >= $day_of_week)
		OR (DAYOFWEEK(f_inicio) > $day_of_week AND (DAYOFWEEK(f_inicio) + TIMESTAMPDIFF(DAY, f_inicio, f_fin) - 7) >= $day_of_week)
	))
#MOUNTH
	OR (timeline_2012.`repeat` = 3 AND (
		(DAYOFMONTH(f_inicio) <= $day_of_month AND (DAYOFMONTH(f_inicio) + TIMESTAMPDIFF(DAY, f_inicio, f_fin)) >= $day_of_month)
		OR (DAYOFMONTH(f_inicio) > $day_of_month AND (DAYOFMONTH(f_inicio) + TIMESTAMPDIFF(DAY, f_inicio, f_fin) - $days_in_month) >= $day_of_month)
	))
#YEAR
	OR (timeline_2012.`repeat` = 4 AND (
		(DAYOFYEAR(f_inicio) <= $day_of_year AND (DAYOFYEAR(f_inicio) + TIMESTAMPDIFF(DAY, f_inicio, f_fin)) >= $day_of_year)
		OR (DAYOFYEAR(f_inicio) > $day_of_year AND (DAYOFYEAR(f_inicio) + TIMESTAMPDIFF(DAY, f_inicio, f_fin) - $days_in_year) >= $day_of_year)
	))
)";

		$this->aWhere[] = $this->tb_timeline . ".scale & 8 = 8";
	}

}