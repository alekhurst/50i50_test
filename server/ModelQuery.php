<?php

class ModelQuery
{
	private $oCategory;

// Query Settings
	public $aWhere = array();
	public $aDate = array();

	public $iSearchYear;
	public $sSelectEvent;

// Table Name
	public $tb_timeline = 'timeline_2012';
	public $tb_icons = 'icons';

	function __construct()
	{
		$this->rand = rand(0, 10000) . '_' . rand(0, 10000);

		$this->oCategory = new Category();

		$this->filterWhere();
	}

	public function getDateForSelect()
	{
		return "";
	}

	public function getWhereForSelect()
	{
		return "";
	}

	public function getCorrectedSubitem($sub)
	{
		return $sub;
	}

	/*
	 *
	 * Functions - Search Events for Line
	 * */
	public function selectEventsCountForLine()
	{
		$query = "SELECT " . $this->getDateForSelect() . " AS subitem, COUNT(id_evento) as num FROM " . $this->tb_timeline . $this->whereQuery() . " GROUP BY subitem";

		return $this->returnResult($query);
	}

	public function selectEventsForLine($iLimitEventsPerSubper)
	{

		$query = "SELECT id, start_date, h_inicio, title, imagen, descripcion, cat_id, subitem, rownum
FROM (
  SELECT
	CASE WHEN @si != (" . $this->getDateForSelect() . ") THEN @row_num := 1 ELSE @row_num := @row_num + 1 END AS rownum,
	" . $this->tb_timeline . ".id_evento as id,
	" . $this->formatDateForQuery() . " AS start_date,
	" . $this->formatTimeForQuery() . " as h_inicio,
	" . $this->tb_timeline . ".titulo as title,
	" . $this->tb_timeline . ".imagen,
	" . $this->tb_timeline . ".descripcion,
	" . $this->tb_timeline . ".category_id AS cat_id,
	@si := (" . $this->getDateForSelect() . ") as subitem
  FROM " . $this->tb_timeline . "
  JOIN (SELECT @si := NULL, @row_num := 0) a
  " . $this->whereQuery()
			. $this->orderByQuery() . "
) r
WHERE
  rownum <= " . intval($iLimitEventsPerSubper) . '
ORDER BY subitem';

		return $this->returnResult($query);
	}

	/*
	 *
	 * Functions - Search Count Events for All Grid and Map
	 * */
	public function selectEventsCountForGrid()
	{
		$query = 'SELECT FOUND_ROWS();';

//		$query = "SELECT COUNT(id_evento) AS CountEvent FROM " . $this->tb_timeline . $this->whereQuery();
		$result = $this->returnResult($query);
		if (!$result)
			return false;
//		$result = mysql_fetch_array($result);
//		var_dump($result);
		list($count) = mysql_fetch_array($result);

		return $count;
	}

	/*
	 *
	 * Functions - Search Events for Grid
	 * */
	public function selectEventsForGrid()
	{

		$aColumns = array('imagen', 'titulo', 'descripcion', 'f_inicio', 'lugar', 'category', 'etiquetas', 'link');

		/* Paging */
		$iDisplayStart = isset($_REQUEST['iDisplayStart']) ? (int)$_REQUEST['iDisplayStart'] : 0;
		$iDisplayLength = isset($_REQUEST['iDisplayLength']) ? (int)$_REQUEST['iDisplayLength'] : 10;

		$sLimit = $this->limitQuery($iDisplayStart, $iDisplayLength);

		$sOrder = '';
		/* Ordering */
		if (isset($_REQUEST['iSortCol_0'])) {
			$sOrder = " ORDER BY  ";
			for ($i = 0; $i < intval($_REQUEST['iSortingCols']); $i++) {
				if ($_REQUEST['bSortable_' . intval($_REQUEST['iSortCol_' . $i])] == "true") {
					$sOrder .= $aColumns[intval($_REQUEST['iSortCol_' . $i])] . " " . mysql_real_escape_string($_REQUEST['sSortDir_' . $i]) . ", ";
				}
			}

			$sOrder = substr_replace($sOrder, "", -2);
			if ($sOrder == "ORDER BY") {
				$sOrder = "";
			}
		}

		$query = "SELECT SQL_CALC_FOUND_ROWS " . $this->tb_timeline . ".titulo
     , " . $this->tb_timeline . ".descripcion
     , " . $this->tb_timeline . ".imagen
     ," . $this->formatDateForQuery() . " AS f_inicio
	 ," . $this->formatTimeForQuery() . " AS h_inicio
     , " . $this->tb_timeline . ".lugar
     , " . $this->tb_timeline . ".etiquetas
     , " . $this->tb_icons . ".category
     , " . $this->tb_icons . ".subcategory
     , " . $this->tb_icons . ".subcategory2
     , " . $this->tb_timeline . ".link
FROM
  " . $this->tb_timeline . "
  LEFT JOIN " . $this->tb_icons . " ON (" . $this->tb_timeline . ".category_id = " . $this->tb_icons . ".id) " . $this->whereQuery() . $sOrder . $sLimit;

		return $this->returnResult($query);
	}

	/*
	 *
	 * Functions - Search Events for Map
	 * */
	public function selectEventsForSpace()
	{
		$iDisplayStart = isset($_REQUEST['iDisplayStart']) ? (int)$_REQUEST['iDisplayStart'] : 0;
		$iDisplayLength = isset($_REQUEST['iDisplayLength']) ? (int)$_REQUEST['iDisplayLength'] : 10000;
//		$pne = isset($_REQUEST['pne']) ? $_REQUEST['pne'] : array(0,0);
//		$psw = isset($_REQUEST['psw']) ? $_REQUEST['psw'] : array(0,0);
//
//		$this->aWhere[] = "(" . $this->tb_timeline . ".latitud < " . $pne[0] . " AND " . $this->tb_timeline . ".longitud < " . $pne[1] . ")";
//		$this->aWhere[] = "(" . $this->tb_timeline . ".latitud > " . $psw[0] . " AND " . $this->tb_timeline . ".longitud > " . $psw[1] . ")";

		$query = "SELECT SQL_CALC_FOUND_ROWS
  " . $this->tb_timeline . ".id_evento as id
  ," . $this->formatDateForQuery() . " AS f_inicio
  ," . $this->formatTimeForQuery() . " AS h_inicio
  , " . $this->tb_timeline . ".titulo
  , " . $this->tb_timeline . ".imagen
  , " . $this->tb_timeline . ".latitud
  , " . $this->tb_timeline . ".longitud
  , " . $this->tb_timeline . ".category_id
FROM
  " . $this->tb_timeline . "
" . $this->whereQuery() . "
" . (($iDisplayLength < 0) ? $this->limitQuery($iDisplayStart, $iDisplayLength) : "");

		return $this->returnResult($query);
	}

	/*
	 *
	 * Functions - Search Events for Map
	 * */
	public function selectEventsForMobileSpace()
	{
		$query = "SELECT
  " . $this->tb_timeline . ".id_evento as id
  ," . $this->formatDateForQuery() . " AS f_inicio
  ," . $this->formatTimeForQuery() . " AS h_inicio
  , " . $this->tb_timeline . ".titulo
  , " . $this->tb_timeline . ".imagen
  , " . $this->tb_timeline . ".latitud
  , " . $this->tb_timeline . ".longitud
  , " . $this->tb_timeline . ".category_id
FROM
  " . $this->tb_timeline . "
  INNER JOIN icons ON (" . $this->tb_timeline . ".category_id = icons.id)
  LEFT JOIN users ON (" . $this->tb_timeline . ".user_id = users.id)
" . $this->whereQuery();

//		print_r($query); die();
		return $this->returnResult($query);
	}

	public function selectEventsForMobileList()
	{
		$query = "SELECT *
FROM
  " . $this->tb_timeline . "
  INNER JOIN icons ON (" . $this->tb_timeline . ".category_id = icons.id)
  LEFT JOIN users ON (" . $this->tb_timeline . ".user_id = users.id)
" . $this->whereQuery();

//		print_r($query); die();
		return $this->returnResult($query);
	}
	/*
	 *
	 * Functions - Search Events for Grid, in Filter Block
	 * */
	public function selectFormatForFilterResult()
	{

		$iDisplayStart = isset($_REQUEST['iDisplayStart']) ? (int)$_REQUEST['iDisplayStart'] : 0;
		$iDisplayLength = isset($_REQUEST['iDisplayLength']) ? (int)$_REQUEST['iDisplayLength'] : 10;
		$sLimit = $this->limitQuery($iDisplayStart, $iDisplayLength);

		$query = "SELECT SQL_CALC_FOUND_ROWS " . $this->tb_timeline . ".titulo
     , " . $this->tb_timeline . ".id_evento
     , " . $this->tb_timeline . ".descripcion
     , " . $this->tb_timeline . ".imagen

	 ," . $this->formatDateForQuery('f_inicio', 'm/d/Y') . " AS start_date
	 ," . $this->formatTimeForQuery() . " AS h_inicio

	 ," . $this->formatDateForQuery('f_fin', 'm/d/Y') . " AS finish_date
	 ," . $this->formatTimeForQuery('h_fin') . " AS h_fin

     , " . $this->tb_timeline . ".lugar
     , " . $this->tb_timeline . ".lugar2
     , " . $this->tb_timeline . ".etiquetas
     , " . $this->tb_timeline . ".latitud
     , " . $this->tb_timeline . ".longitud
     , " . $this->tb_timeline . ".latitud2
     , " . $this->tb_timeline . ".longitud2
     , " . $this->tb_timeline . ".f_tipo
     , " . $this->tb_timeline . ".category_id
     , " . $this->tb_timeline . ".source
FROM
  " . $this->tb_timeline . "
" . $this->whereQuery() . ' ' . $sLimit;
// ORDER BY f_inicio DESC, h_inicio DESC
		return $this->returnResult($query);
	}

	function convertToMatchFilter($search)
	{
		return preg_replace('/([A-Za-z0-9-]+)/', '+$1', $search);
	}


	function whereCoordinate()
	{

		$this->aWhere[] = "(" . $this->tb_timeline . ".latitud <> 0 OR " . $this->tb_timeline . ".longitud <> 0)";

		$pne = isset($_REQUEST['pne']) ? $_REQUEST['pne'] : array(0, 0);
		$psw = isset($_REQUEST['psw']) ? $_REQUEST['psw'] : array(0, 0);
		$this->aWhere[] = "(" . $this->tb_timeline . ".latitud < " . $pne[0] . " AND " . $this->tb_timeline . ".longitud < " . $pne[1] . ")";
		$this->aWhere[] = "(" . $this->tb_timeline . ".latitud > " . $psw[0] . " AND " . $this->tb_timeline . ".longitud > " . $psw[1] . ")";
	}

	public function filterWhere()
	{

		if (!isset($_COOKIE['GLOBAL_FILTER']) || !$_COOKIE['GLOBAL_FILTER']) {
			return $this->defaultFilter();
		}

		$aWhere = array();

		parse_str($_COOKIE['GLOBAL_FILTER'], $aParams);

		if (isset($aParams['clear']))
			$aWhere[] = '1=0';
		if (isset($aParams['All']) && trim($aParams['All'])) {
			$aWhere[] = " MATCH (" . $this->tb_timeline . ".titulo,
				" . $this->tb_timeline . ".descripcion,
				" . $this->tb_timeline . ".etiquetas) AGAINST ('\"" . $this->escapeString($aParams['All']) . "\"' IN BOOLEAN MODE) ";

		} else if (isset($aParams['Timeline']) && trim($aParams['Timeline'])) {
			$sTimeline = "Timeline of " . trim(str_replace('Timeline of', '', $aParams['Timeline']));
			$aWhere[] = " MATCH (" . $this->tb_timeline . ".etiquetas) AGAINST ('\"" . $this->escapeString($sTimeline) . "\"' IN BOOLEAN MODE) ";

		} else if (isset($aParams['Tag']) && trim($aParams['Tag'])) {
			$aParams['Tag'] = $this->convertToMatchFilter($aParams['Tag']);
			$aWhere[] = " MATCH (" . $this->tb_timeline . ".etiquetas) AGAINST ('\"" . $this->escapeString($aParams['Tag']) . "\"' IN BOOLEAN MODE) ";

		} else if (isset($aParams['Category']) && trim($aParams['Category'])) {

			$aCats = $this->oCategory->getCatsByName($aParams['Category']);

			if (count($aCats) > 1)
				$aWhere[] = $this->tb_timeline . ".category_id IN (" . implode(',', $aCats) . ")";
			else if (count($aCats) == 1)
				$aWhere[] = $this->tb_timeline . ".category_id = " . $aCats[0] . "";

		} else {
			$t_where = null;

			if (isset($aParams['Title'])) {
				$aParams['Title'] = trim($aParams['Title']);

				if ($aParams['Title'] != 'Title' && $aParams['Title'] != 'Search' && $aParams['Title']) {
					$aParams['Title'] = $this->convertToMatchFilter($aParams['Title']);
					$t_where = "MATCH(" . $this->tb_timeline . ".titulo) AGAINST ('" . $this->escapeString($aParams['Title']) . "' IN BOOLEAN MODE)";
				}
			}

			if (isset($aParams['desc']) && $aParams['desc'] != 'Description') {
				$aParams['desc'] = $this->convertToMatchFilter($aParams['desc']);
				$aWhere[] = " (MATCH(" . $this->tb_timeline . ".descripcion) AGAINST ('\"" . $this->escapeString($aParams['desc']) . "\"' IN BOOLEAN MODE)" . ($t_where ? ' OR ' . $t_where : '') . ') ';

			} else if ($t_where) {
				$aWhere[] = $t_where;

			}


			if (isset($aParams['cat_filter']) && count($aParams['cat_filter'])) {
				$aCats = $this->oCategory->getParentCatsByIDs($aParams['cat_filter']);

				if (count($aCats) > 1)
					$aWhere[] = $this->tb_timeline . ".category_id IN (" . implode(',', $aCats) . ")";
				else if (count($aCats) == 1)
					$aWhere[] = $this->tb_timeline . ".category_id = " . $aCats[0] . "";
			}

			if (isset($aParams['tag_filter']) && count($aParams['tag_filter'])) {
				$aWhereTags = array();

				foreach ($aParams['tag_filter'] as $val)
					$aWhereTags[] = $this->tb_timeline . ".etiquetas LIKE '%" . $this->escapeString($val) . "%' ";

				$aWhere[] = ' (' . implode(' AND ', $aWhereTags) . ') ';

			}

		}

		if (!count($aWhere))
			return $this->defaultFilter();
		else
			$this->aWhere = array_merge($this->aWhere, $aWhere);

	}

	function mobileFilter()
	{
		$s = isset($_REQUEST['search']) ? $_REQUEST['search'] : null;

		if ($s) {
			$this->aWhere[] = " (MATCH (" . $this->tb_timeline . ".titulo,
				" . $this->tb_timeline . ".descripcion,
				" . $this->tb_timeline . ".etiquetas) AGAINST ('\"" . $this->escapeString($s) . "\"' IN BOOLEAN MODE)
				OR (users.username LIKE '" . $_REQUEST['search'] . "%' OR users.email LIKE '" . $_REQUEST['search'] . "%')
				OR (icons.category LIKE '" . $_REQUEST['search'] . "%' OR icons.subcategory LIKE '" . $_REQUEST['search'] . "%' OR icons.subcategory2 LIKE '" . $_REQUEST['search'] . "%')
			)";

//			$aCats = array(5, 6);
//			$this->aWhere[] = $this->tb_timeline . ".category_id NOT IN (" . implode(',', $aCats) . ")";
		} else {
//			$aCats = array(32, 34);
//			$this->aWhere[] = $this->tb_timeline . ".category_id IN (" . implode(',', $aCats) . ")";
		}


	}

	function defaultFilter()
	{
		$aCats = $this->oCategory->defaultCategory();

		$this->aWhere[] = $this->tb_timeline . ".category_id IN (" . implode(',', $aCats) . ")";

		return $aCats;
	}

	private function whereQuery()
	{
		if (count($this->aWhere))
			return " WHERE " . implode(' AND ', $this->aWhere);
	}

	private function orderByQuery($aOrderBy = null)
	{
		if ($aOrderBy === null && $this->sSelectEvent !== null)
			return " ORDER BY " . $this->escapeString($this->sSelectEvent);
		else if (count($aOrderBy))
			return " ORDER BY " . implode(',', $aOrderBy);
	}

	private function limitQuery($displayStart = 0, $displayLength = 10)
	{
		if ($displayLength < 0)
			return false;
		return " LIMIT " . intval($displayStart) . ", " . intval($displayLength);
	}


	function escapeString($string)
	{
		return mysql_real_escape_string(trim($string));
	}

	/*
	 * Operation in Time and Date
	 * */
	public function genDateForSQL($sDate)
	{
		$this->aDate = explode(',', $sDate);
		if (isset($this->aDate[0]))
			$this->iSearchYear = (int)$this->aDate[0];
	}

	public function yearToStr()
	{
		return substr('0000' . $this->iSearchYear, -4);
	}

	public function formatDateForQuery($field = "f_inicio", $type = 'M d, Y')
	{
		switch ($type) {
			case 'm/d/Y':
				$format = array('%m/%d/%Y', '%m/%Y', '%Y');
				break;
			case 'd/m/Y':
				$format = array('%d/%m/%Y', '%m/%Y', '%Y');
				break;
			case 'M d, Y':
			default:
				$format = array('%b %d, %Y', '%b %Y', '%Y');
		}

		return "DATE_FORMAT(
		  " . $this->tb_timeline . "." . $field . ",
		  IF (" . $this->tb_timeline . ".scale & 8 = 8, '" . $format[0] . "',
		    IF (" . $this->tb_timeline . ".scale & 16 = 16, '" . $format[1] . "', '" . $format[2] . "')
		  )
		)";
	}

	public function formatTimeForQuery($field = "h_inicio")
	{
		return "TIME_FORMAT(" . $this->tb_timeline . "." . $field . ", '%H:%i:%S')"; // IF (" . $this->tb_timeline . ".scale & 4 = 4, '%H:%i:%S', '')
	}

	/*
	 * Operation in Category icon
	 * */
	public function getCatIconByID($id_cat)
	{
		$id_use_cat = $this->oCategory->aCatList[$id_cat]['use_id'];
		return $this->oCategory->aCatList[$id_use_cat]['icon'];
	}

	public function getCatByID($id_cat)
	{
		return $this->oCategory->aCatList[$id_cat];
	}


	/*
	 *  For Test and Debug
	 * */
	public $bStartQuery = true;
	public $rand = 0;

	private function returnResult($sQuery)
	{
		global $db;

		if ($this->bStartQuery) {
			write_log('sql_' . $this->rand . '_START');
			write_log("\n\nSQL_" . $this->rand . " = \n" . $sQuery, 'log_sql');
			$result = mysql_query($sQuery, $db) or die(mysql_error());
			write_log('sql_' . $this->rand . '_END');
			return mysql_num_rows($result) ? $result : false;
		} else
			return false;
	}
	/* For Test and Debug */
}

/*
 *
 *
(
	(
		`repeat` = 0 AND
		(
			(timeline_2012.f_inicio < '2013-9-20' AND timeline_2012.f_fin > '2013-9-20') OR
			((timeline_2012.f_inicio = '2013-9-20' OR timeline_2012.`repeat` = 1) AND timeline_2012.h_inicio < '9:00:00')
		)
		AND
		(
			timeline_2012.f_fin > '2013-9-20' OR
			((timeline_2012.f_fin = '2013-9-20' OR timeline_2012.`repeat` = 1) AND timeline_2012.h_fin >= '8:00:00')
		)
	)
	OR
	(
		timeline_2012.f_fin = '0000-00-00'
		AND (
			(timeline_2012.f_inicio = '2013-9-20' OR timeline_2012.`repeat` = 1)
			AND
			(
				(timeline_2012.h_inicio >= '8:00:00' AND timeline_2012.h_inicio < '9:00:00')
				OR timeline_2012.h_inicio = '00:00:00'
			)
		)
	) OR (
		f_inicio <= '2013-9-20' AND (
			(`repeat` = 1 AND (1379690408 - start_time) % 86400 < live_time)
			OR (`repeat` = 2 AND (1379690408 - start_time) % 604800 < live_time)
			OR (`repeat` = 3 AND (1379690408 - start_time) % 2592000 < live_time)
			OR (`repeat` = 4 AND (1379690408 - start_time) % 31622400 < live_time)
		)
	)
) AND
*/