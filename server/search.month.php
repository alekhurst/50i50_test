<?php

class newFormatDate extends ModelQuery
{

	function __construct() {
		$this->sSelectEvent = $this->tb_timeline . '.f_inicio';
		parent::__construct();
	}

	public function getDateForSelect () {
		return " DAY(" . $this->sSelectEvent . ") ";
	}

	public function getWhereForSelect () {
		if ($this->iSearchYear < 0 || $this->iSearchYear > 9999)
			$this->aWhere[] = " (" . $this->tb_timeline . ".year = " . $this->iSearchYear . "
				AND MONTH(" . $this->tb_timeline . ".f_inicio) = " . $this->aDate[1] . ") ";
		else {
			$date = $this->yearToStr() . '-' . $this->aDate[1];

			$this->aWhere[] = "(" . $this->tb_timeline . ".f_inicio BETWEEN  '" . $date . "-1' AND '" . $date ."-31') AND " . $this->tb_timeline . ".scale & 32 = 32";
		}

	}

	function getCorrectedSubitem ($subitem) {
		return $subitem - 1;
	}
}