<?php
require_once ('config.php');
require_once ('Category.php');

$cat_id = isset($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;

$oCat = new Category();
if (! isset($oCat->aGroupCat[$cat_id]))
	echo json_encode(array('error' => true));

echo json_encode($oCat->aGroupCat[$cat_id]['sub_cat']);
