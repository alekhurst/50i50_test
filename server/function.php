<?php


function getmicrotime () {
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

function write_log ($string, $file='log') {
//	return false;
	if (! file_exists('../log/'))
		return false;
	$file = '../log/' . $file . '_' . date('dmy') . '.txt';

	$count_second = getmicrotime() - $_SESSION['TIME_START_PERIOD'];
	$string = $string . " -------------- " . number_format($count_second, 5, '.', ',') . " ---\n";
	$f = @fopen($file, 'a+');
	if (! $f)
		return false;
	@fwrite($f, $string);
	@fclose($f);
	$_SESSION['TIME_START_PERIOD'] = getmicrotime();
}

function write_arr_log ($arr, $file='log') {
//	return false;
	if (! file_exists('../log/'))
		return false;
	$file = '../log/' . $file . '_' . date('dmy') . '.txt';

	$count_second = getmicrotime() - $_SESSION['TIME_START_PERIOD'];
	ob_start();
	print_r($arr);
	$string = ob_get_contents();
	ob_clean();
	$string = $string . " -------------- " . number_format($count_second, 5, '.', ',') . " ---\n";
	$f = @fopen($file, 'a+');
	if (! $f)
		return false;
	@fwrite($f, $string);
	@fclose($f);
	$_SESSION['TIME_START_PERIOD'] = getmicrotime();
}


function getCatList () {
	$oMem = new Memcache;
	$oMem->connect('127.0.0.1', 11211) or die("Could not connect");

	$aCat = @$oMem->get('CatList');
	if (empty($aCat)) {
		$query = "SELECT icons.id
		 , icons.category
		 , icons.subcategory
		 , icons.subcategory2
		 , icons.parent_id
	FROM
	  icons";

		$res = mysql_query($query);

		$aCat = array();
		if (mysql_num_rows($res))
			while ($row = mysql_fetch_assoc($res))
				$aCat[$row['id']] = array(
					'cat' => $row['category'],
					'subcat' => $row['subcategory'],
					'subcat2' => $row['subcategory2'],
					'parent' => $row['parent_id']
				);


		$oMem->set('CatList', $aCat, false, 30);
	}
	return $aCat;
}
function _d($p) {
	echo "<pre>";
	print_r($p);
	echo "</pre>";
}

function getBrowser () {
	$u_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	if (preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) { } else return 100;

	$ub = "MSIE";

	$known = array('Version', $ub, 'other');
	$pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	if (! preg_match_all($pattern, $u_agent, $matches)) {
		return 100;
	}

	$i = count($matches['browser']);
	if ($i != 1) {
		if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
			$version= $matches['version'][0];
		}
		else {
			$version= $matches['version'][1];
		}
	}
	else {
		$version= $matches['version'][0];
	}
	return $version;
}

function get_encoding_utf8 ($string) {
	return htmlentities($string, ENT_COMPAT, 'UTF-8');
}
