<?php


class newFormatDate extends ModelQuery
{

	function __construct() {
//		$this->sSelectEvent = $this->tb_timeline . '.f_inicio';
		$this->sSelectEvent = $this->tb_timeline . '.`year`';
		parent::__construct();
	}

	public function getDateForSelect () {
//		return " YEAR(" . $this->sSelectEvent . ") ";
		return " " . $this->sSelectEvent . " ";
	}

	public function getWhereForSelect () {
//		$this->aWhere[] = $this->sSelectEvent . " BETWEEN ". $decade ." AND ". ($decade + 9) ." ";
//		$this->aWhere[] = $this->tb_timeline . ".f_inicio BETWEEN '". $decade ."-1-1' AND '". ($decade + 9) ."-12-31'";

		$decade = $this->returnDecadeStart($this->iSearchYear);
		$this->aWhere[] = " (" . $this->sSelectEvent . " >= ". $decade ." AND " . $this->sSelectEvent . " <= ". ($decade + 9) . ")  AND " . $this->tb_timeline . ".scale & 128 = 128";
	}

	private function returnDecadeStart ($date) {
		return ($date != 0) ? floor($date/10) * 10 : 0;
	}

	function getCorrectedSubitem ($subitem) {
		$decade = $this->returnDecadeStart(intval($subitem));
		return $subitem - $decade;
	}
}
