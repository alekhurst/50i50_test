CREATE TABLE `50i50_timespace`.users (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`uid` varchar(100),
	`provider_uid` int(11),
	`provider` varchar(100),
	`name` varchar(255),
	`email` varchar(150) NOT NULL,
	`create_date` datetime NOT NULL,
	`visit_date` datetime NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=`MyISAM`;

ALTER TABLE `50i50_timespace`.`timeline_2012` ADD COLUMN `user_id` int(11) NOT NULL DEFAULT '0' AFTER `repeat`;