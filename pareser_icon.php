<?php
require_once './server/config.php';
require_once './server/Category.php';

$obj_cat = new Category();

$cat = $obj_cat->aGroupCat[115];
//foreach ($obj_cat->aGroupCat as $key => $cat) {
	$folder = './time_icons/';
	$new_folder = '../time_icons_for_sprite/';

	if (file_exists($folder . $cat['icon'])) {
		copy($folder . $cat['icon'], $new_folder . 'cat/' . $cat['icon']);
	}

	foreach ($cat['sub_cat'] as $sub_key => $sub_cat) {

		if (file_exists($folder . $sub_cat['icon'])) {
			copy($folder . $sub_cat['icon'], $new_folder . 'subcat/' . $sub_cat['icon']);
		}

		if (count($sub_cat['sub_cat'])) {

			foreach ($sub_cat['sub_cat'] as $sub2_kay => $sub2_cat) {

				if (file_exists($folder . $sub2_cat['icon'])) {
					copy($folder . $sub2_cat['icon'], $new_folder . 'sub2cat/' . $sub2_cat['icon']);
				}

			}

		}

	}

//}