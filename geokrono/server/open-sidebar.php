<?php
if (!isset($_REQUEST['sidebar'])) {
	exit;
}

require_once('../../server/config.php');

$template_link = ABS_PATH . FOLDER_GEOKRONO . '/templates/' . $_REQUEST['sidebar'] . '.phtml';

//var_dump($template_link); die();
require_once ($template_link);
