$(function () {

	window.Options = Backbone.Model.extend({

		defaults:function () {
			var today = new Date();

			return {eventsPerColumn:10,
				showBubbles:(!isNaN(parseInt($('#show_bubbles:checked').val()))) ? parseInt($('#show_bubbles:checked').val()) : 0,
				categories:{},
				categoriesApply:[],
				search:false,
				searchText:'',
				date:new Date(today.setHours(today.getHours() - 9)),
				subitems:0,

				selectedPeriodIndex:$('#current-period option').index($('#current-period option:selected').get(0))

			}
		},

		initialize:function () {

		},

		periodIndex:function () {
			var old_index = this.get('selectedPeriodIndex');
			this.set({selectedPeriodIndex:$('#current-period option').index($('#current-period option:selected').get(0))});

			return old_index > this.get('selectedPeriodIndex');
		},

		category:function () {
			var categories = this.get('categories');
			if (arguments.length == 1) {
				return categories[arguments[0]];
			}

			if (arguments.length == 2) {
				var apply = this.get('categoriesApply');

				categories[arguments[0]] = arguments[1];
				this.set({categories:categories});
				if (arguments[1]) apply.push(arguments[0]);
				else {
					var i = jQuery.inArray(arguments[0], apply);
					if (i > -1) apply.splice(i, 1);
				}
				this.set({categoriesApply:apply});

			}
		}

	});

	window.AppOptions = new Options();

	window.TimelineView = Backbone.View.extend({

		el:$("#timeline"),

		collection:Timeline,


		mousedown:'false',
		mousedownposition:'0',
		//length from timeline left corner to window left corner
		leftborder:'0',
		//length from timeline right corner to window right corner
//        rightborder: '0',

		//what side we are moving timeline
		movingdirection:1,


		windowWidth:0,
		timelineWidth:0,

		events:{
			'mousedown':'mouseDownEvent',
			'mouseup':'mouseUpEvent',
			'mousemove':'mouseMoveEvent'
		},

		initialize:function () {
			//find visible window width
			this.windowWidth = $('#right-block').width();

			//bind body for mouse up event (need releasse mouse)
			_.bindAll(this, 'initializePeriod');

			$('body').delegate('', 'mouseup', this.mouseUpEvent);
			$('body').delegate('', 'mousemove', this.mouseMoveEvent);
			$('#current-period').delegate('', 'change', function () {
				if (!Interface.countCollection()) {
					App.initializePeriod();
				}
			});

			$('#timeline .event').live('click', this.eventClick);
			$('#map .infoBox').live('click', this.eventMouseClick);


			//if we add new item to collection, need draw this item
			this.collection.bind('add', this.drawItem, this);
			this.collection.bind('reset', this.destroyAll, this);


			map.changeMap();
//            window.map = new TimelineMap();
			this.initializePeriod();

		},
// TODO: Positions events to Timeline ad CSS for not resize this JS;
// TODO: Test Bug in time - 1 to 0 ad a month,
		iniPeriod:function () {
			var subitems = this.setDateForInitialize();

			if (this.collection.length) {
				var middle = $('#right-block').width() / 2;

				$('#timeline .event').each(function () {
					var z = $(this).position().left;

					z = (z - 8) / SUBITEM_WIDTH;

					$(this).css('left', (z * (R_SUBITEM_WIDTH + 6) + 8) + 'px');
				});
				$('#timeline .claster').each(function () {
					var z = $(this).position().left;

					z = (z - 5) / SUBITEM_WIDTH;

					$(this).css('left', (z * (R_SUBITEM_WIDTH + 6) + 5) + 'px');
				});

				SUBITEM_WIDTH = R_SUBITEM_WIDTH + 6;

				var time_line = $('#timeline .item');
				var size = 0, leftsize = 0;
				var middle_s = Math.floor(COLLECTION_LENGTH / 2);

				for (var i = 0; i < COLLECTION_LENGTH; i++) {
					size += time_line.eq(i).find('.subitems .si').length * SUBITEM_WIDTH;
					if (i < middle_s) leftsize += time_line.eq(i).find('.subitems .si').length * SUBITEM_WIDTH;
				}

				leftsize += SUBITEM_WIDTH * subitems - middle;

				this.timelineWidth = size;
				$('#timeline').css({'width':size, 'left':(-1) * leftsize});

				Interface.countCollection();

				this.collection.recalculateItemsSize();
				this.recalculateBorders();
			}
		},

		initializePeriod:function () {
			this.collection.reset();
			$(this.el).css({ 'left':'0px', 'width':'0px'});
			this.timelineWidth = 0;
			this.leftborder = 0;
			this.movingdirection = 1;

			var selected = $('#current-period').val();
			AppPeriod.change(selected);

			for (var i = 0; i < COLLECTION_LENGTH; i++) {
				this.collection.push();
			}

			this.reviseTypeLoadPage(this.collection.at(Math.floor(COLLECTION_LENGTH / 2)));
			//scroll timeline
			this.scrollTodayToCenter();

		},

		setDateForInitialize:function () {
			var subitems = 0,
				date = new Date();

			if (this.collection.length) {
				var middle = $('#right-block').width() / 2,
					item_offset = this.collection.leftsize - this.leftborder;

				var num_items = this.collection.at(Math.floor(COLLECTION_LENGTH / 2)).get('subitems') * SUBITEM_WIDTH + item_offset;


				if (num_items <= middle) {
					date = new Date(this.collection.at(Math.ceil(COLLECTION_LENGTH / 2)).get('date'));
					subitems = (middle - num_items) / SUBITEM_WIDTH;
				} else {

					date = new Date(this.collection.at(Math.floor(COLLECTION_LENGTH / 2)).get('date'));
					subitems = (middle - item_offset) / SUBITEM_WIDTH;
				}

				if (subitems < 0)
					subitems = AppPeriod.current.getSubperiod(date);
				date = AppPeriod.current.setSelectDate(date, subitems);
			}
			AppOptions.set({date:date, subitems:Math.floor(subitems)});

			$('#center_time_line').html(
				date.getFullYear() + '<br />'
					+ window.MonthsShort[date.getMonth()] + ' ' + date.getDate() + '<br />'
					+ date.getHours() + 'h' + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()));
			return subitems;
		},

		mouseDownEvent:function (event) {
			MOUSE_ACTIVE_EVENT = this;
			MOUSE_ACTIVE_EVENT.mousedownposition = event.clientX;
			MOUSE_ACTIVE_EVENT.mousedown = true;
		},

		mouseUpEvent:function (event) {
			if (MOUSE_ACTIVE_EVENT)
				MOUSE_ACTIVE_EVENT.mousedown = false;
//            event.stopPropagation();
		},

		mouseMoveEvent:function (event) {
			if (MOUSE_ACTIVE_EVENT && MOUSE_ACTIVE_EVENT.mousedown == true) {
				MOUSE_ACTIVE_EVENT.scrollTo((event.clientX - MOUSE_ACTIVE_EVENT.mousedownposition) * 2);
				MOUSE_ACTIVE_EVENT.mousedownposition = event.clientX;
			}
		},

		scrollTo:function (x) {
			var position = $('#timeline').position();

			$('#timeline').css('left', position.left + x);

			this.setDateForInitialize();
			this.recalculateBorders();
		},

		scrollToNewDate:function (date) {
			var count_date = AppPeriod.current.oddsDate(date, AppOptions.get('date'));
			this.scrollTo(-count_date * SUBITEM_WIDTH);
		},

		scrollTodayToCenter:function () {
			var middle_s = Math.floor(COLLECTION_LENGTH / 2),
				leftsize = 0,
				middle = ($(window).width() - 13 - parseInt($('#content').css('left'))) / 2,
				to_day = new Date(AppOptions.get('date'));

			for (var i = 0; i < middle_s; i++) {
				leftsize += this.collection.at(i).get('subitems') * SUBITEM_WIDTH;
			}
			leftsize += SUBITEM_WIDTH * AppPeriod.current.getSubperiod(to_day) - middle + SUBITEM_WIDTH / 2;
			this.scrollTo(-leftsize);
//	put events of middle day to map
		},

//	need to calculate what is the width from window to timeline
		recalculateBorders:function () {
			var item_center_number = Math.floor(COLLECTION_LENGTH / 2);
			var left_size = 0;

			this.leftborder = -$(this.el).position().left;

			for (var i = 0; i < item_center_number; i++) {
				left_size += this.collection.at(i).get('subitems') * SUBITEM_WIDTH;
			}

//			left_size += AppOptions.get('subitems') * SUBITEM_WIDTH;

			var current_width = this.collection.at(item_center_number).get('subitems') * SUBITEM_WIDTH;
			var item_offset = current_width - this.leftborder - (this.windowWidth / 2);
			var item_offset_correct = left_size - this.leftborder - (this.windowWidth / 2);

//			console.log('Zero Date', this.collection.at(0).get('date'), this.leftborder, left_size, AppOptions.get('subitems'), (this.windowWidth/2));
//			console.log('Item Offset', item_offset_correct, item_offset);
			if (item_offset_correct < -current_width) {
				this.collection.at(item_center_number).set({placeOnMap:false});
				this.movingdirection = 1;
				this.collection.push();
				this.reviseTypeLoadPage(this.collection.at(item_center_number));
			}


			if (item_offset_correct > 0) {
				this.collection.at(item_center_number).set({placeOnMap:false});
				this.movingdirection = -1;
				this.collection.unshift();

				this.reviseTypeLoadPage(this.collection.at(item_center_number));
			}
		},

		reviseTypeLoadPage:function (currentBlock) {
			switch (OptionsInterface.get('activeWindow')) {
				case 'grid' :
					currentBlock.loadGridEvents();
					break;
				case  'space' :
					currentBlock.loadMapEvents();
					break;
			}

			if (OptionsInterface.get('activeFilter')) {
				currentBlock.loadFilterEvent('fd');
			}
		},

		drawItem:function (item) {
			var view = new ItemView({model:item});
			view.bind('destroy', this.destroyDay, this);
			this.bind('filterEvents', view.filterEvents, view);

			var el = $(view.render().el);
			this.timelineWidth += $('.si', el).length * SUBITEM_WIDTH;
			$(this.el).width(this.timelineWidth);

			if (this.movingdirection > 0) {
				$(this.el).append(el);
			} else {
				$(this.el).prepend(el);
			}


			this.leftborder = -$(this.el).position().left;
		},

		destroyDay:function (itemWidth) {
			this.scrollTo(itemWidth * this.movingdirection);
			this.timelineWidth -= itemWidth;
			$(this.el).width(this.timelineWidth);
		},

		destroyAll:function () {
			$(this.el).children().remove();
			for (var event_q in TL_Ajax_Object) {
				if (typeof TL_Ajax_Object[event_q] != 'undefined')
					TL_Ajax_Object[event_q].abort();
			}
		},

		updateEventByFilter:function () {
			for (var i = 0; i < COLLECTION_LENGTH; i++) {
				this.collection.at(i).loadEvents();
			}

			var currentBlock = this.collection.at(Math.floor(COLLECTION_LENGTH / 2));

			if (OptionsInterface.get('activeFilter'))
				currentBlock.loadFilterEvent('fr');
			this.reviseTypeLoadPage(currentBlock);
		},


		eventClick:function (click) {
//			Interface.getPageForLeftSidebar('events-description', $(click.currentTarget).find('.event_id').val());
//
//			Interface.getContentForSidebar({
//				sidebar: 'events-description',
//				custom_param: $(click.currentTarget).find('.event_id').val(),
//				size: 300
//			});
			Interface.getEventByID($(click.currentTarget).find('.event_id').val());
		},

		eventMouseOver:function (click) {
			$(click.currentTarget).css('z-index', 3);
		},

		eventMouseOut:function (click) {
			$(click.currentTarget).css('z-index', 2);
		},

		eventMouseClick:function (click) {
//			Interface.getContentForSidebar({
//				sidebar: 'events-description',
//				custom_param: $(click.currentTarget).find('.block').attr('id').replace('map_event_', ''), // $(click.currentTarget).find('.event_id').val(),
//				size: 300
//			});
			Interface.getEventByID($(click.currentTarget).find('.block').attr('id').replace('map_event_', ''));
		}

	});

});