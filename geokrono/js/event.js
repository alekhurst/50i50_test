$(function(){
    window.EventMapView = Backbone.View.extend({

        tagName: "div",

//        template: _.template($('#event-template').html()),
        template: function (event) {
			var img = this.model.imagen.length ? this.model.imagen : '';
			var time = event.h_inicio ? event.h_inicio : '';

			return '<div id="map_event_' + event.id + '" class="block" style="display: block">' +
				'<h1 class="title">' + img + event.titulo +  '</h1>' +
				'<p class="date">' + event.f_inicio + ' ' + time + '</p><div class="clear"></div></div>';
		},

        initialize: function(event){
            this.model = event;
        },

        render: function (showBlock, map) {
//            var block = $(this.template({id: this.id, event: this.model, image : this.model.imagen.length ? this.model.imagen : ''}));
//            var block = $(this.template(this.model)); // {id: this.id, event: this.model, image : this.model.imagen.length ? this.model.imagen : ''}));
//            block.toggle(showBlock);

            $(this.el).append($(this.template(this.model)).toggle(showBlock));

			var image = new google.maps.MarkerImage(
				URL_PARENT + 'time_icons/' + this.model.icon,
				new google.maps.Size(36, 41),
				new google.maps.Point(0,0),
				new google.maps.Point(0, 41)
			);

			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(this.model.latitud, this.model.longitud) ,
				icon: image,
				visible : false
			});

			var ib = new InfoBox({
				content: $(this.el).html(),
				disableAutoPan: true,
				maxWidth: 0,
				pixelOffset: new google.maps.Size(35, -35),
				zIndex: 2,
				closeBoxURL: "",
				infoBoxClearance: new google.maps.Size(1, 1),
				isHidden: false,
				pane: "floatPane",
				enableEventPropagation: true
			});
/* Bind Visible change for InfoBox */
			google.maps.event.addListener(marker, "visible_changed", function () {
				if (marker.getVisible())
					ib.open(map, marker);
				else
					ib.close();
			});
/* Bind Click for Event Marker */
			var id = this.model.id;
			google.maps.event.addListener(marker, 'click', function() {
//				Interface.getContentForSidebar({
//					sidebar: 'events-description',
//					custom_param: id,
//					size: 300
//				});
				Interface.getEventByID(id);
			});

            return marker;
        }
    });

});