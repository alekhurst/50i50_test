
$(function () {
	window.TimelineMap = Backbone.View.extend({

		gMap:null,
		Point_North_East:[0,0],
		Point_South_West:[0,0],

		initialize:function () {
			var self = this;
//			self.defaultCoordinates();
			var mapOptions = {
				center:new google.maps.LatLng(37.69065350120003, -122.18050947109373),
				zoom:10,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			};

			this.gMap = new google.maps.Map(document.getElementById('map'), mapOptions);

			google.maps.event.addListener(this.gMap, 'tilesloaded', function () {
				self.updateMapCoordinate();
			});

		},

		changeMap:function () {
			var self = this;
			google.maps.event.addListener(this.gMap, 'tilesloaded', function () {
				self.updateMapCoordinate();
				App.reviseTypeLoadPage(App.collection.at(Math.floor(COLLECTION_LENGTH / 2)));
			});
		},

		updateMapCoordinate:function () {
			var m = this.gMap.getBounds();
			this.Point_North_East = [m.getNorthEast().lat(), m.getNorthEast().lng()];
			this.Point_South_West = [m.getSouthWest().lat(), m.getSouthWest().lng()];
		},

		placeEvents:function (events) {

			var periods = events.length;
			var event_id = 0;
			var markers = [];
			var minimumClusterSize = 14;

			for (var i = 0; i < periods; i++) {
				if (events[i].latitud != 0 && events[i].longitud != 0)
					markers[event_id++] = (new EventMapView(events[i])).render(true, this.gMap);
			}

			this.markerCluster = new MarkerClusterer(this.gMap, markers, {minimumClusterSize:minimumClusterSize});

			google.maps.event.addListener(this.markerCluster, "clusteringend", function (cluster) {
				var clusters = cluster.getClusters();

				for (var j in clusters) {
					var cluster_marker = clusters[j].getMarkers();

					for (var i in cluster_marker) {
						if (cluster_marker[i].getMap())
							cluster_marker[i].setVisible(true);
						else
							cluster_marker[i].setVisible(false);
					}
				}

			});

		},

		removeEvents:function () {
			$('#map div.infoBox').remove();

			if (!this.markerCluster)
				return false;

			var events = this.markerCluster.getMarkers();
			this.markerCluster.setMap(null);

			for (var i in events)
				events[i].setMap(null);

			this.markerCluster = null;

		},

		filterEvents:function (events) {
			var periods = events.length;
			var event;
			var event_id = 0;
			for (var i = 0; i < periods; i++) {
				var num_events = events[i].length;

				for (var j = 0; j < num_events; j++) {
					event = events[i][j];
					if (event.latitud != 0 && event.longitud != 0) {

						this.events[event_id].setVisible(event.visible);
						event_id++;
					}
				}

			}

		},

		changeShowBubbles:function (showBubbles) {

			if (! showBubbles) {
				$('#map').addClass('close_info_box');
				return false;
			}

			if (! this.markerCluster)
				return false;

			$('#map').removeClass('close_info_box');

			var clusters = this.markerCluster.getClusters();
			for (var j in clusters) {
				var cluster_marker = clusters[j].getMarkers();
				for (var i in cluster_marker) {
					if (cluster_marker[i].getMap())
						cluster_marker[i].setVisible(true);
					else
						cluster_marker[i].setVisible(false);
				}
			}

		},

		defaultCoordinates:function () {
			var self = this;
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(

					function (position) {
						self.setCoordinates({location:new google.maps.LatLng(position.coords.latitude, position.coords.longitude)});
					},

					function (error) {
						switch (error.code) {
							case error.TIMEOUT:
								//alert ('Timeout');
								break;
							case error.POSITION_UNAVAILABLE:
								//alert ('Position unavailable');
								break;
							case error.PERMISSION_DENIED:
								//alert ('Permission denied');
								break;
							case error.UNKNOWN_ERROR:
								//alert ('Unknown error');
								break;
						}

					}
				);

			}
		},

		setCoordinates:function (address) {
			var self = this;
			var geo = new google.maps.Geocoder();

			geo.geocode(address, function (res) {
				var address = '';
				var address_row = '';
				for (var i = 0; i < res[0].address_components.length; i++) {
					address_row = res[0].address_components[i];
					for (var j = 0; j < address_row.types.length; j++) {
						if (address_row.types[j] == 'locality') {
							address += address_row.long_name + ', '
						}
						if (address_row.types[j] == 'administrative_area_level_1') {
							address += address_row.long_name + ', '
						}
						if (address_row.types[j] == 'country') {
							address += address_row.long_name
						}
					}
				}

				geo.geocode({address:address}, function (res) {
					self.gMap.setCenter(res[0].geometry.location);
					self.gMap.fitBounds(res[0].geometry.viewport);
				});
			});
		}

	});


});