var COLLECTION_LENGTH = 7;
var EVENTS_IN_SUBPERIOD = 4;
var SUBITEM_HEIGHT = 475;
var App;
var MOUSE_ACTIVE_EVENT;
var GRID_OBJ;
var LOC_FORM_OBJ;

var DEF_SUBITEM_WIDTH = 250;
var SUBITEM_WIDTH = 250;
var R_SUBITEM_WIDTH = 244;

var AUTOCOMPLETE_ARR = Array();
/*
 *Function for search end day to Month
 *
 * */
Date.prototype.isLeapYear = function () {
	var y = this.getFullYear();
	return y % 4 == 0 && y % 100 != 0 || y % 400 == 0;
};
Date.prototype.getDaysInMonth = function () {
	return arguments.callee[this.isLeapYear() ? 'L' : 'R'][this.getMonth()];
};
Date.prototype.getDaysInMonth.R = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
// durations of months for the leap year
Date.prototype.getDaysInMonth.L = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

//(cookies = {
//	data: {},
//	init: function() {
//		var c = document.cookie.split(";"), l = c.length, i, t;
//		for( i=0; i<l; i++) {
//			t = c[i].split("=");
//			cookies.data[t.shift()] = t.join("=");
//		}
//	},
//	read: function(key) {
//		return cookies.data[key];
//	},
//	set: function(key,value) {
//		document.cookie = key+"="+value;
//		cookies.data[key] = value;
//	}
//}).init();
/*
 * Cookie Function And Settings
 *
 * */
var COOKIE_LIFE = 948672000000; // 3 year

function setCookie(name, value, expires, path, domain, secure) {
	document.cookie = name + "=" + value +
		((expires) ? ";expires=" + expires : "") +
		((path) ? ";path=" + path : "") +
		((domain) ? ";domain=" + domain : "") +
		((secure) ? ";secure" : "");
	console.log('Cookie', document.cookie);
	console.log('Query', name + "=" + value +
		((expires) ? ";expires=" + expires : "") +
		((path) ? ";path=" + path : "") +
		((domain) ? ";domain=" + domain : "") +
		((secure) ? ";secure" : ""));
}

function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset);
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}
function deserialize(data) {
	if (!data)
		return [];

	var splits = (data).split('&'),
		i = 0,
		split,
		key,
		value = null,
		splitParts = null,
		kv = {};

	while (split = splits[i++]) {
		splitParts = split.split('=');
		key = splitParts[0] || '';
		value = decodeURIComponent((splitParts[1] || '').replace(/\+/g, ' '));

		if (key != '') {
			if (key in kv) {
				if ($.type(kv[key]) !== 'array')
					kv[key] = [kv[key]];

				kv[key].push(value);
			} else
				kv[key] = value;
		}
	}
	return kv;
}
/*
 *
 * year, month, date, hour, minute, second
 * */
function date_to_time(arr_date) {
	var selectDate = (AppOptions) ? AppOptions.get('date') : new Date();

	arr_date[0] = (Number(arr_date[0]) ? Number(arr_date[0]) : selectDate.getFullYear()) + 100; // year
	arr_date[1] = (Number(arr_date[1]) ? Number(arr_date[1]) : selectDate.getMonth()) - 1;     // month
	arr_date[2] = Number(arr_date[2]) ? Number(arr_date[2]) : selectDate.getDay();             // date
//	arr_date[3]  = (arr_date[3] ? arr_date[3] : selectDate.getDay();            // hour
//	hour   = parseInt(hour, 10);// ? parseInt(hour) : selectDate.getHours();
//	minute = parseInt(minute, 10);// ? parseInt(minute) : selectDate.getMinutes();
//	second = parseInt(second, 10);// ? parseInt(second) : selectDate.getSeconds();

	var new_date_ob = new Date(arr_date[0], arr_date[1], arr_date[2], arr_date[3], arr_date[4], arr_date[5]);

	new_date_ob.setFullYear(new_date_ob.getFullYear() - 100);
	return new_date_ob;
}

function add_name_period(slider) {
	$('#current-period-view').html($('#current-period option:selected').html());
//	slider.find('.ui-corner-all').html('<span id="name-period"><span>' + $('#current-period option:selected').html() + '</span></span>');
//	$('#name-period span').css({'left':'-' + ($('#name-period span').width() / 2) + 'px' });
}
//
//var arrScriptSponsors = [
//	'<script type="text/javascript" language="javascript" src="http://www.jdoqocy.com/placeholder-6203350?target=_blank&mouseover=Y"></script>',
//	'<script type="text/javascript" src="http://publisher.monster.com/Services/WidgetHandler.ashx?WidgetID=EAAQLMYpint7Dyb0O.m1mh.f4g--&Verb=Initialize"></script><noscript><a id="monsterBrowseLink13410" class="fnt4" href="http://jobsearch.monster.com/jobs/">Search For Jobs on Monster</a></noscript>',
//	'<a href="http://www.sparkol.com?aid=10604" target="_blank"><img border="0" src="http://www.sparkol.com/affiliate/VS_SquarePopUp.jpg" /></a>',
//	'<script type="text/javascript" language="javascript" src="http://www.anrdoezrs.net/placeholder-6203346?target=_top&mouseover=N"></script>',
//	'<script type="text/javascript" language="javascript" src="http://www.kqzyfj.com/placeholder-6203352?target=_blank&mouseover=Y"></script>',
//	'<script type="text/javascript" language="javascript" src="http://www.jdoqocy.com/placeholder-6203371?target=_blank&mouseover=Y"></script>',
//	'<script type="text/javascript" language="javascript" src="http://www.jdoqocy.com/placeholder-6203378?target=_blank&mouseover=Y"></script>',
//	'<script type="text/javascript" language="javascript" src="http://www.jdoqocy.com/placeholder-6203382?target=_blank&mouseover=Y"></script>',
//	'<script type="text/javascript" language="javascript" src="http://www.anrdoezrs.net/placeholder-6203390?target=_blank&mouseover=Y"></script>',
//	'<script type="text/javascript" language="javascript" src="http://www.anrdoezrs.net/placeholder-6204449?target=_blank&mouseover=Y"></script>',
//	'<script type="text/javascript" language="javascript" src="http://www.dpbolvw.net/placeholder-6204443?target=_blank&mouseover=Y"></script>'
//];

