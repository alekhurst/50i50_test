(function ($) {
	var struct = {
		history: [''],
		block_h: 32,
		block_max: 5,
		filter_w: 250,
		filter_h: 0,
		cat_visible: false,
		parent: {id: 0, count: 0},
		sub_parent: {id: 0, count: 0},

		self: {},
		visible_cat: function () {
			if (this.cat_visible) {
				$('#CategorySelected').addClass('act');
				this.self.find('a').hide();
				this.self.find('.act').show();
			} else {
				$('#CategorySelected').removeClass('act');
				this.self.find('a').show();
			}
			this.count_elements();
		},

		count_elements: function () {

		},
		global_update: function () {},

		update_cat: function () {
			this.self.find('a').removeClass('act');

			$('#categories_use input').each(function () {
				var parent_id = $(this).data('pid');
				var id = $(this).val();
				var count = Number($(this).attr('data-count'));;

				$('#cid_' + parent_id).addClass('act');
				$('.pid_' + id).addClass('act');

				$('#cid_' + id).addClass('act');

				if (count) {
					console.log('update', count, id, parent_id);
					$('#cid_' + id + ' span').html(count);
				}

			});

			$('#cat_history').html(this.history.join(' > '));

			this.visible_cat();
		},

		select_all: function (a) {
			var id = a.data('id').split(';');
			if (a.hasClass('act')) {
				a.removeClass('act')
					.find('span').html(0);
				this.remove_all(id[0]);
			} else {
				this.add_all(id[0]);
				a.addClass('act')
					.find('span').html(a.find('span').data('max'));
			}

			if (this.cat_visible) {
				this.self.find('ul').css({top:0}).addClass('active');
				if (! a.parents('ul').find('a.act').length) {
					this.cat_visible = false;
				}
			}

			this.visible_cat();
			return false;
		},
		select_cat: function (a) {
			var id = a.data('id').split(';');
			if (a.hasClass('act')) {
				a.removeClass('act');
				this.remove_cat(id[0]);
				this.parent.count--;
				if (this.sub_parent.id) {
					this.sub_parent.count--;
					if (this.sub_parent.count <= 0)
						this.remove_cat(this.sub_parent.id);
				}
			} else {
				a.addClass('act');
				this.add_cat(id[0], id[1]);
				this.parent.count++;
				if (this.sub_parent.id) {
					this.sub_parent.count++;
					this.add_cat(this.sub_parent.id, id[1]);
				}
			}

			console.log('F',struct.sub_parent);

			$('#cid_' + this.parent.id + ' span').html(this.parent.count);
			if (this.sub_parent.id) {
				$('#cid_' + this.sub_parent.id + ' span').html(this.sub_parent.count);
				$('#categories_use .cid_' + this.sub_parent.id).attr('data-count', this.sub_parent.count);
			}
			this.visible_cat();
			return false;
		},
		select_subcat: function (a) {
			$('#ul_ss_cat_list').html('');

			var id = a.data('id').split(';');
			var max = Number(a.find('span').data('max'));

//			console.log(a.parent().find('.subsub a'));
			if (a.hasClass('act')) {
				a.removeClass('act');

				this.remove_cat(id[0]);
				a.parent().find('.subsub a').each(function () {
					var sub_id = $(this).data('id').split(';');
					console.log('a', sub_id);
					struct.remove_cat(sub_id[0]);
				});
				this.parent.count -= max;
			} else {
				a.addClass('act');
				this.add_cat(id[0], id[1]);
				a.parent().find('.subsub a').each(function () {
					var sub_id = $(this).data('id').split(';');
					console.log('a', sub_id);
					struct.add_cat(sub_id[0], sub_id[1]);
				});
				this.parent.count += max;
			}

			a.find('span').html(max);
			$('#cid_' + this.parent.id + ' span').html(this.parent.count);

			this.visible_cat();
			return false;
		},
		remove_all: function (id) {
			$('#categories_use input.pid_' + id).remove();
		},
		add_all: function (id) {
			$('#categories_use').append('<input type="hidden" class="cid_' + id + ' pid_' + id +  '" data-pid="' + id + '" name="cat_filter[]" value="' + id + '" />');
		},
		remove_cat: function (id) {
			$('#categories_use input.cid_' + id).remove();
		},
		add_cat: function (id, pid) {
			var cat = $('#categories_use');
			if (!cat.find('input').hasClass('cid_' + id))
				cat.append('<input type="hidden" class="cid_' + id + ' pid_' + pid +  '" data-pid="' + pid + '" name="cat_filter[]" value="' + id + '" />');
		}
	};


	var bind = {
		activeCat: function () {
			struct.cat_visible = ! struct.cat_visible;
			struct.visible_cat();
			struct.self.find('ul').css({top:0});
		},

		scrollDown: function () {
			var cat = struct.self.find('ul.active');
			var max_scroll = -1 * (cat.find('li a:visible').length - 1) * struct.block_h + struct.filter_h;

			var top = parseInt(cat.css('top'));
			if (top < max_scroll)
				return false;

			cat.stop(true, true).animate({top: '-=' + struct.block_h});
			return false;
		},
		scrollUp: function () {
			var cat = struct.self.find('ul.active');

			var top = parseInt(cat.css('top'));
			if (-1 * struct.block_h < top)
				return false;

			cat.stop(true, true).animate({top: '+=' + struct.block_h});
			return false;
		},

		active: false,
		start: function () {
			if (this.active == false)
				this.active = true;
			return false;
		},
		finish: function () {
			this.active = false;
		},

		back: function () {
			if (bind.start())
				return false;

			var wrapper = struct.self.find('.categories_list_wrapper');

			if (!wrapper.find('ul.active').prev().length) {
				$('#cat_bakc_button').hide();
				return false;
			}

			struct.sub_parent.id = 0;
			struct.sub_parent.count = 0;
			struct.history = (function () {
				var history = [];
				for (var i = 0, count = (struct.history.length - 1); i < count; i++) {
					history[history.length] = struct.history[i];
				}
				return history;
			})();

			wrapper.stop(true, true).animate({left: '+=' + struct.filter_w }, function () { struct.update_cat(); bind.finish(); })
				.find('ul.active').removeClass('active').prev().addClass('active');

			if (!wrapper.find('ul.active').prev().length)
				$('#cat_bakc_button').hide();

			return false;
		},

		checkCat: function (el) {
			if (bind.start())
				return false;

			var self = $(this);
			var act = $(this).hasClass('act');
			var id = self.attr('id').replace('cid_', '');

			if (el.target.localName == 'img')
				return struct.select_all(self);

			$('#ul_s_cat_list').load('server/open-sidebar.php', {sidebar: 'sub-cat',cat_id: id}, function () {
				if (! $(this).html())
					return struct.select_cat(self);

				struct.parent.id = id;
				struct.parent.count = act ? parseInt(self.find('span').html()) : 0;
				struct.history[1] = self.find('img').attr('alt');
				struct.self.find('.categories_list_wrapper').stop(true, true).animate({left: - struct.filter_w + 'px'}, function () { bind.finish(); });
//										.find('ul.active').removeClass('active').next().css({top:0}).addClass('active');

				struct.update_cat();

				$('#ul_cat_list').removeClass('active');
				$('#ul_s_cat_list').css({top:0}).addClass('active');

				if (act && $('#categories_use input.cid_' + id).length) {
					struct.remove_all(id);
					$(this).find('a').each(function () {
						var id = $(this).data('id').split(';');
						struct.add_cat(id[0], id[1]);
					});
				}

				$('#cat_bakc_button').show();
				return false;
			});

			return false;
		},
		checkSubCat: function (el) {
			if (bind.start())
				return false;

			var sub_box = $(this).parent().find('.subsub');
			var sub = sub_box.html();

			if (el.target.localName == 'img')
				return struct.select_subcat($(this));

			if (sub) {
				struct.sub_parent.id = $(this).attr('id').replace('cid_', '');
				struct.sub_parent.count = $(this).hasClass('act') ? parseInt($(this).find('span').html()) : 0;
				struct.history[2] = $(this).find('img').attr('alt');

				$('#ul_ss_cat_list').html(sub).css({top:0}).addClass('active');

				sub_box.html('');
				struct.update_cat();
				sub_box.html(sub);

				struct.self.find('.categories_list_wrapper').stop(true, true).animate({left: (- struct.filter_w * 2) + 'px'}, function () { bind.finish(); });
				//		.find('ul.active').removeClass('active').next().css({top:0}).addClass('active');

				$('#ul_s_cat_list').removeClass('active');
//								$('#ul_ss_cat_list').css({top:0}).addClass('active');
			} else
				return struct.select_cat($(this));

			return false;
		},

		defaultCat: function () {
			bind.start();
			func.clear('', 0);
			$('#categories_use').load('server/open-sidebar.php', {sidebar: 'default-cat'}, function () {
				struct.update_cat();
				bind.finish();
			});
			return false;
		},

		checkSubSubCat: function () {
			return struct.select_cat($(this));
		}
	};

	var func = {
		init: function () {
			struct.self = this;
			struct.filter_h = struct.block_h * struct.block_max;
			this.css({height: (struct.filter_h) + 'px'})
				.find('ul').each(function (i) {
					$(this).css({left: i * struct.filter_w, width: struct.filter_w + 'px'});
				});
			$('#category_scroll_down').bind('click', bind.scrollDown);
			$('#category_scroll_up').bind('click', bind.scrollUp);
			$('#cat_bakc_button').bind('click', bind.back);
			$('#CategorySelected').bind('click', bind.activeCat);
			$('#ul_cat_list a').bind('click', bind.checkCat);
			$('#ul_s_cat_list a').live('click', bind.checkSubCat);
			$('#ul_ss_cat_list a').live('click', bind.checkSubSubCat);

			$('#default-filter').bind('click', bind.defaultCat);
			$('#clear-all-filter').click(function () { return func.clear('clear=all', COOKIE_LIFE); });
			struct.update_cat();
		},

		clear: function (val, live) {
			$('#title_filter').val(def_title_filter_val);
			$('#desc_filter').val(def_desc_filter_val);

			$('#categories_use').html('');
			$('#categories_list_box a').removeClass('act');

			$('#result_tags_filters label').remove();

			setCookie('GLOBAL_FILTER', val, live, '/');
			App.updateEventByFilter();

			return false;
		}
	};
	$.fn.catList = function () {
		return func.init.apply(this);
	};
})(jQuery);



var def_title_filter_val = 'Title';
var def_desc_filter_val = 'Description';
