var Ajax_Object = {
	'fr': null,
	'fd': null,
	'Space': null,
	'Grid': null
};
var GRID_FILTER = {
	'fr': {},
	'fd': {}
};
var TL_Ajax_Object = {};
$.ajaxSetup({
	url: URL_GEOKRONO + 'server/index.php',
	type: 'post',
	dataType: 'json',
	global: false,
	error: function () {
		console.log('Bud Request');
	}
});


$(function () {
    window.Item = Backbone.Model.extend({

        events : null,

        defaults: function () {
            return {sort : 0};
        },

        initialize: function () {
            this.set({
				sort : this.get('date').getTime(),
                placeOnMap : false,
				events : [],
                subitems : (AppPeriod.current.maxSubperiod(this.get('date')) - AppPeriod.current.minSubperiod(this.get('date'))+ 1),
                itemId : AppPeriod.current.itemId(this.get('date'))
            });

            this.loadEvents();
        },

        loadEvents : function () {
			var self = this;
	        var obj_date = self.getDateForQuery(self.get('sort'));

	        var params = {
				width: R_SUBITEM_WIDTH,
				type: AppPeriod.current.get('type'),
				search: AppOptions.searchText,
				pne: map.Point_North_East,
				psw: map.Point_South_West
			};

	        $('#right-block').LoadingBox('start', obj_date.int);

			TL_Ajax_Object[self.get('itemId')] = $.ajax({
				url: URL_GEOKRONO + 'server/index.php?block=timeline&date=' + obj_date.str,
				data: params,
				dataType: 'text',
				success: function (response) {
					var item_EL = $('#' + self.get('itemId'));
					if (!item_EL.length) {
						return false;
					}

					item_EL.html(response);
					item_EL.find('.claster').button();

					var bubblesActive = OptionsInterface.get('activeShowBubbles');

					if (bubblesActive) {
						$('#timeline .block').show();
					} else {
						$('#timeline .block').hide();
					}

					if ('line' == OptionsInterface.get('activeWindow'))
						$('#right-block').LoadingBox('finish', obj_date.int);

					delete TL_Ajax_Object[self.get('itemId')];
				}
			});
        },

		loadMapEvents : function () {
			if (Ajax_Object['Space'])
				Ajax_Object['Space'].abort();

			var limit_isset = getCookie('MAP_LIMIT_EVENTS');

			if (limit_isset) {
				this.pagingMapEvents(0, limit_isset);
				return false;
			}

			var self = this;
			var obj_date = self.getDateForQuery(self.get('sort'));
			var params = {
				count_events: true,
				type: AppPeriod.current.get('type'),
				search: AppOptions.searchText,
				pne: map.Point_North_East,
				psw: map.Point_South_West
			};

			$('#right-block').LoadingBox('start', obj_date.int);

			Ajax_Object['Space'] = $.ajax({
				url: URL_GEOKRONO + 'server/index.php?block=map&date=' + obj_date.str,
				data: params,
				success: function (response) {
					window.map.removeEvents();

					var total_events = parseInt(response.iTotalRecords);
					if (total_events > 10000) {
						if (confirm("There are more than 10.000 events to be loaded. This may temporarily freeze your browser.\n Do you wish to continue?")) {
							self.pagingMapEvents(0, -1);
							setCookie('MAP_LIMIT_EVENTS', -1, COOKIE_LIFE, '/');
						} else {
							self.pagingMapEvents(0, 10000);
							setCookie('MAP_LIMIT_EVENTS', 10000, COOKIE_LIFE, '/');
						}
					} else {
						window.map.placeEvents(response.events_array);
					}

					if (OptionsInterface.get('activeWindow') == 'space')
						$('#right-block').LoadingBox('finish', obj_date.int);

					Ajax_Object['Space'] = null;
				}
			});
		},

		pagingMapEvents: function (iDisplayStart, iDisplayLength) {
			var self = this;
			var obj_date = self.getDateForQuery(self.get('sort'));
			var params = {
				type: AppPeriod.current.get('type'),
				search: AppOptions.searchText,
				iDisplayStart: iDisplayStart,
				iDisplayLength: iDisplayLength,
				pne: map.Point_North_East,
				psw: map.Point_South_West
			};

			$('#right-block').LoadingBox('start', obj_date.int);

			Ajax_Object['Space'] = $.ajax({
				url: URL_GEOKRONO + 'server/index.php?block=map&date=' + obj_date.str,
				data: params,
				success: function(response){
					window.map.removeEvents();

					$total_events = parseInt(response.iTotalRecords);
					if ($total_events > 0) {
						window.map.placeEvents(response.events_array);
					}

					if (OptionsInterface.get('activeWindow') == 'space')
						$('#right-block').LoadingBox('finish', obj_date.int);

					Ajax_Object['Space'] = null;
				}
			});
		},

		loadGridEvents: function () {
			var self = this;
			var obj_date = self.getDateForQuery(self.get('sort'));

			$('#right-block').LoadingBox('start', obj_date.int);

			if (typeof(GRID_OBJ) != 'undefined')
				GRID_OBJ.fnDestroy();

			GRID_OBJ = $('#grid_mode_table').dataTable({
				"aoColumnDefs": [{ "bSortable": false, "aTargets": [0,7]}],
				"aaSorting": [[ 3, "desc" ]],

				"bFilter": false,

				"sDom": '<"top"Slp<"clear">>rt<"bottom"ilp<"clear">>',
				"bJQueryUI": true,
				"bProcessing": true,
				"bServerSide": true,
				"sAjaxSource": URL_GEOKRONO + 'server/index.php?block=grid&date=' + obj_date.str,
				"fnServerData": function( sUrl, aoData, fnCallback ) {
					if (Ajax_Object['Grid'])
						Ajax_Object['Grid'].abort();

					aoData.push({name: 'type', value: $('#current-period option:selected').val()});
					aoData.push({name: 'pne[]', value: map.Point_North_East[0]});
					aoData.push({name: 'psw[]', value: map.Point_South_West[0]});
					aoData.push({name: 'pne[]', value: map.Point_North_East[1]});
					aoData.push({name: 'psw[]', value: map.Point_South_West[1]});

					Ajax_Object['Grid'] = $.ajax({
						url: sUrl,
						data: aoData,
						dataType: "json",
						success: function(response){
							fnCallback(response);

//							model.loadingClose(date_int, 'grid');
							if (OptionsInterface.get('activeWindow') == 'grid')
								$('#right-block').LoadingBox('finish', obj_date.int);

							Ajax_Object['Grid'] = null;
						}
					});
				}
			});
		},

		loadFilterEvent : function (type) {

			if (GRID_FILTER[type].length)
				GRID_FILTER[type].fnDestroy();

			var self = this;
			var obj_date = self.getDateForQuery(self.get('sort'));


			GRID_FILTER[type] = $('#grid_' + type).dataTable({
				"sPaginationType": "full_numbers",
				"aoColumnDefs": [{ "sType": "html", "aTargets": [ 0 ] }],
				"bFilter": false,

				"sDom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
				"bJQueryUI": true,
				"bProcessing": true,
				"bServerSide": true,
				"sAjaxSource": URL_GEOKRONO + 'server/index.php?block=' + type + '&date=' + obj_date.str,
				"fnServerData": function( sUrl, aoData, fnCallback ) {

					$('#left-block .lf-content').hide();
					$('#left-block').LoadingBox('start', type + obj_date.int);

					if (Ajax_Object[type])
						Ajax_Object[type].abort();

					aoData.push({name: 'type', value: $('#current-period option:selected').val()});
					aoData.push({name: 'activeWindow', value: OptionsInterface.get('activeWindow')});

					aoData.push({name: 'pne[]', value: map.Point_North_East[0]});
					aoData.push({name: 'psw[]', value: map.Point_South_West[0]});
					aoData.push({name: 'pne[]', value: map.Point_North_East[1]});
					aoData.push({name: 'psw[]', value: map.Point_South_West[1]});

					Ajax_Object[type] = $.ajax({
						url: sUrl,
						data: aoData,
						dataType: "json",
						success: function(response){
							fnCallback(response);

							$('#left-block').LoadingBox('finish', type + obj_date.int);

							if ($('#left-block').width() > 15)
								$('#left-block .lf-content').show();

							Ajax_Object[type] = null;
						}
					});
				}
			});

		},

	    getDateForQuery: function (date) {
		    var date_obj = new Date(date);
		    var date_str = date_obj.getFullYear() + ',' + (date_obj.getMonth() + 1) + ',' + date_obj.getDate() + ',' + date_obj.getHours() + ',' + date_obj.getMinutes() + ',' + date_obj.getSeconds();

		    return {int: date, obj: date_obj, str: date_str};
	    }

    });

    window.WeekDaysLong = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    window.WeekDaysShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ];
    window.MonthsLong = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    window.MonthsShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];



    window.Day = Backbone.Model.extend({


        defaults : function(){
            return {type : 'day'};
        },

        showDate : function(date){
            return WeekDaysShort[date.getDay()] +' '+ MonthsShort[date.getMonth()] + ' ' + date.getDate() + ' ' + date.getFullYear();
        },

        minSubperiod : function(){
            return 0;
        },

        maxSubperiod : function(){
            return 23;
        },

		getSubperiod : function (date) {
			return date.getHours();
		},

		setSelectDate: function (date, subperiod) {
			date.setHours(subperiod);
			return date;
		},

        nextDate : function(date){
            return new Date (date.setDate(date.getDate() + 1));
        },

        prevDate : function (date){
            return new Date (date.setDate(date.getDate() - 1));
        },

		oddsDate: function (date1, date2) {
			return (date1 - date2)/(1000*60*60); // 60 * 60 * 1000
		},

        firstDate : function(){
            var date = AppOptions.get('date');

//            if(AppOptions.periodIndex()) {
//                date.setDate(AppOptions.get('subitems') + 1);
//            }
            return new Date(date.setDate(date.getDate() - Math.floor(COLLECTION_LENGTH / 2)));
        },

        itemId : function (date){
            return 'day' + date.getYear() + '_' + date.getMonth() + '_' + date.getDate();
        }
    });

    window.Month = Backbone.Model.extend({

        defaults : function(){
            return {type : 'month'};
        },

        showDate : function(date){
            return MonthsLong[date.getMonth()] + ' ' + date.getFullYear();
        },

        minSubperiod : function(date){
            return 1;
        },

        maxSubperiod : function(date){
			return date.getDaysInMonth();
//            return 32 - new Date(date.getFullYear(), date.getMonth(), 32).getDate();
        },

		getSubperiod : function (date) {
			return date.getDate() - 1;
		},

		setSelectDate: function (date, subperiod) {
			date.setDate(subperiod + 1);
			return date;
		},

        nextDate : function(date){
			var new_date = new Date (date);
			var correct_day = this.correctDay(new_date, new_date.getMonth() + 1);

			if (correct_day < new_date.getDate())
				new_date.setDate(correct_day);
			new_date.setMonth(new_date.getMonth() + 1);

			return new_date;
        },

        prevDate : function (date){
			var new_date = new Date (date);
			var correct_day = this.correctDay(new_date, new_date.getMonth() - 1);

			if (correct_day < new_date.getDate())
				new_date.setDate(correct_day);
			new_date.setMonth(new_date.getMonth() - 1);

			return new_date;
        },

		oddsDate: function (date1, date2) {
			return (date1 - date2)/(1000*60*60*24); // 60 * 60 * 1000 * 24
		},

        firstDate : function(){
			var date = AppOptions.get('date');
//			if(AppOptions.periodIndex()) {
//				date.setMonth(AppOptions.get('subitems') );
//			}
            return new Date(date.setMonth(date.getMonth() - Math.floor(COLLECTION_LENGTH / 2)));
        },

        itemId : function (date){
            return 'month' + date.getYear() + '_' + date.getMonth();
        },

		correctDay: function (date, number_month) {
			var correct_date = new Date(date.getFullYear(), number_month, 1);
			return correct_date.getDaysInMonth();
		}
    });

	window.Year = Backbone.Model.extend({

		defaults : function(){
			return {type : 'year'};
		},

		showDate : function(date){
			return date.getFullYear();
		},

		minSubperiod : function(date){
			return 1;
		},

		maxSubperiod : function(date){
			return 12;
		},

		getSubperiod : function (date) {
			return date.getMonth();
		},

		setSelectDate: function (date, subperiod) {
			var new_date = new Date(date.getFullYear(), subperiod, 1);

			var maxDate = new_date.getDaysInMonth();
			if (maxDate < date.getDate())
				date.setDate(maxDate);

			date.setMonth(subperiod);
			return date;
		},

		nextDate : function(date){
			return new Date (date.setFullYear(date.getFullYear() + 1));
		},

		prevDate : function (date){
			return new Date (date.setFullYear(date.getFullYear() - 1));
		},

		firstDate : function(){
			var date = AppOptions.get('date');
//			AppOptions.periodIndex();
//			if(AppOptions.periodIndex()) {
//				date.setFullYear(AppOptions.get('subitems'));

//			}
			return new Date(date.setFullYear(date.getFullYear() - Math.floor(COLLECTION_LENGTH / 2)));
		},

		oddsDate: function (dateFinish, dateStart) {
			var months = 0;
			months = (dateFinish.getFullYear() - dateStart.getFullYear()) * 12;
			months -= dateFinish.getMonth();
			months += dateStart.getMonth();
			return months; // 60 * 60 * 1000 * 24 * 12
		},

		itemId : function (date){
			return 'year' + date.getYear();
		}
	});

    window.Hour = Backbone.Model.extend({

        defaults : function(){
            return {type : 'hour'};
        },

        showDate : function(date){
            return date.getHours() + ':00:00' + ' '+ WeekDaysShort[date.getDay()] +' '+ MonthsShort[date.getMonth()] + ' ' + date.getDate();
        },

        minSubperiod : function(date){
            return 0;
        },

        maxSubperiod : function(date){
            return 59;
        },

		getSubperiod : function (date) {
			return date.getMinutes();
		},

		setSelectDate: function (date, subperiod) {
			date.setMinutes(subperiod);
			return date;
		},

        nextDate : function(date){
            return new Date (date.setHours(date.getHours() + 1));
        },

        prevDate : function (date){
            return new Date (date.setHours(date.getHours() - 1));
        },

        firstDate : function(){
            var date = AppOptions.get('date');

            if(AppOptions.periodIndex()) {
                date.setHours(AppOptions.get('subitems'));
            }
            return new Date(date.setHours(date.getHours() - Math.floor(COLLECTION_LENGTH / 2)));
        },

		oddsDate: function (date1, date2) {
			return (date1 - date2)/(1000 * 60); // 60 * 1000
		},

        itemId : function (date){
            return 'hour' + date.getYear() + '_' + date.getMonth() + '_' + date.getDate() + '_' + date.getHours();
        }
    });

	window.Decade = Backbone.Model.extend({

		defaults : function(){
			return {type : 'decade'};
		},

		showDate : function(date){
			return this.returnDecade(date) + "'s";
		},

		minSubperiod : function() {
			return 0;
		},

		maxSubperiod : function() {
			return 9;
		},

		getSubperiod : function (date) {
			var decade = this.returnDecade(date);

			return date.getFullYear() - decade;
		},

		setSelectDate: function (date, subperiod) {
			var decade = this.returnDecade(date);
			date.setFullYear(decade + subperiod);
			return date;
		},

		nextDate : function(date) {
			return new Date (date.setFullYear(date.getFullYear() + 10));
		},

		prevDate : function (date) {
			return new Date (date.setFullYear(date.getFullYear() - 10));
		},

		firstDate : function () {
			var date = AppOptions.get('date');
//			if(AppOptions.periodIndex()) {
//				date.setFullYear(AppOptions.get('subitems') );
//			}
			return new Date(date.setFullYear(date.getFullYear() - (Math.floor(COLLECTION_LENGTH / 2)) * 10));
		},

		oddsDate: function (dateFinish, dateStart) {
			return (dateFinish.getFullYear() - dateStart.getFullYear()); // 60 * 60 * 1000 * 24 * 12
		},

		itemId : function (date){
			return 'decade' + this.returnDecade(date);
		},

		returnDecade: function (date) {
			return Math.floor(date.getFullYear()/10) * 10;
		}
	});

	window.Century = Backbone.Model.extend({

		defaults : function(){
			return {type : 'century'};
		},

		showDate : function (date) {
			$s = 'th';
			var date = (this.returnCentury(date)/100 + 1);
			var dec = Math.floor(date/10) * 10;
			var cen = Math.floor(dec/100)*100 - dec;

			if (Math.abs(cen) != 10)
				dec = date - dec;
			else
				dec = 0;
			switch (Math.abs(dec)) {
				case 1:
				case -1:
					$s = 'st';
					break;
				case 2:
				case -2:
					$s = 'nd';
					break;
				case 3:
				case -3:
					$s = 'rd';
					break;
				default:
					$s = 'th';
			}
			return date + $s + ' C.';
		},

		minSubperiod : function() {
			return 0;
		},

		maxSubperiod : function() {
			return 9;
		},

		getSubperiod : function (date) {
			var decade = this.returnCentury(date);
			return Math.floor((date.getFullYear() - decade)/10);
		},

		setSelectDate: function (date, subperiod) {
			var decade = Math.floor(date.getFullYear()/10) * 10;
			var centry = this.returnCentury(date);
			var year = date.getFullYear() - decade;

			date.setFullYear(centry + year + subperiod * 10);
			return date;
		},

		nextDate : function(date) {
			var d = new Date (date.setFullYear(date.getFullYear() + 100));
			console.log('Next', d);
			return d;
		},

		prevDate : function (date) {
			var d = new Date (date.setFullYear(date.getFullYear() - 100));
			console.log('Prev', d);
			return d;
		},

		firstDate : function () {
			var date = new Date(AppOptions.get('date'));
			var d = new Date(date.setFullYear(date.getFullYear() - (Math.floor(COLLECTION_LENGTH / 2)) * 100));
			console.log('fd', date);
			console.log('fd', d);
//			if(AppOptions.periodIndex()) {
//				date.setFullYear(AppOptions.get('subitems') );
//			}
			return d;
		},

		oddsDate: function (dateFinish, dateStart) {
			return (dateFinish.getFullYear() - dateStart.getFullYear())/10; // 60 * 60 * 1000 * 24 * 12
		},

		itemId : function (date){
			return 'decade' + this.returnCentury(date);
		},

		returnCentury: function (date) {
			return Math.floor(date.getFullYear()/100) * 100;
		}
	});
});