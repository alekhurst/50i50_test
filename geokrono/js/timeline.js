/**
 * Created by JetBrains WebStorm.
 * User: pavel
 * Date: 12/27/11
 * Time: 12:46 PM
 * To change this template use File | Settings | File Templates.
 */

$(function(){

    window.TimelineCollection = Backbone.Collection.extend({
        model: Item,

        //length in px of left elements in collection
        leftsize :0,

        //length in px of left elements in collection
//        rightsize : 0,

        initialize: function(models, args){

        },


        push : function () {
            if (this.length) {
                var model = this.last();
                var date = new Date (model.get('date').getTime());
                date = AppPeriod.current.nextDate(date);
            }else{
                var date = AppPeriod.current.firstDate();
            }
            this.add({date: date});

            if(this.length > COLLECTION_LENGTH){
                this.first().destroy();
            }

            this.recalculateItemsSize();

        },

        unshift : function () {

            if(this.length){
                var model = this.at(0);

                var date = new Date(model.get('date').getTime());
                date = AppPeriod.current.prevDate(date);

            }else{
                var date = AppPeriod.current.firstDate(date);
            }
            this.add({date: date});

            if(this.length > COLLECTION_LENGTH){
                this.last().destroy();
            }

            this.recalculateItemsSize();
        },

        comparator : function (model){
            return model.get('sort');
        },

        //allow to calculate width of items left and right of active element
        recalculateItemsSize : function(){

            if (this.length == COLLECTION_LENGTH){
                this.leftsize = 0;
//                this.rightsize = 0;

                var middle = Math.floor(COLLECTION_LENGTH / 2);

                for (var i = 0; i < middle; i++){
					this.leftsize += this.at(i).get('subitems') * SUBITEM_WIDTH;
                }
            }
        }

    });


    window.Timeline = new TimelineCollection;



    window.ItemView = Backbone.View.extend({

        tagName: "div",

        template: _.template($('#subitem-template').html()),

        events: {
        },

        visibleEvents : [],

        initialize: function() {
            this.model.bind('destroy', this.remove, this);
            this.model.bind('showEvents', this.renderEvents, this);

        },


        render: function() {
            var period = AppPeriod.current;
            var currentDate = this.model.get('date');
            var min = period.minSubperiod(currentDate);
            var max = this.model.get('subitems') + min;

            var topDiv = $('<div class="subitems"></div>');

            for (var i=min; i< max; i++){
				topDiv.append(this.template({number: i, width: R_SUBITEM_WIDTH}));
            }
            topDiv.append('<div class="clear" />');

            var events = $('<div class="events" style="height:'+SUBITEM_HEIGHT+'px" id="'+ this.model.get('itemId') +'">');

            $(this.el).addClass('item').append('<div class="date">'+ period.showDate(currentDate) +'</div>').append(topDiv).append(events);

            return this;
        },

        renderEvents : function(model_events) {
            var events, num_events, subperiod_events, visible_events;

            for (var subperiod = 0; subperiod < model_events.length; subperiod++){
                events = model_events[subperiod];

                num_events = events.length;

                subperiod_events = $('<div class="events"></div>');

                $('.hour:eq(' + subperiod + ') .top', $(this.el)).append(subperiod_events);

                visible_events = 0;

                for (var i = 0; i < num_events; i++) {
                    if (this.applyFilter(events[i])) {
                        visible_events++;

                        if(visible_events <= EVENTS_IN_SUBPERIOD){
                            var event = new EventView(events[i]);
							Interface.updateEventShowBubbles();
                            if (!this.visibleEvents[subperiod])
								this.visibleEvents[subperiod] = [];
                            this.visibleEvents[subperiod].push(event.id);
                        }

                        events[i]['visible'] = true;
                    } else {
                        events[i]['visible'] = false;
                    }
                }

                if (visible_events > EVENTS_IN_SUBPERIOD){
                    var claster = $('<div class="claster">'+(visible_events - EVENTS_IN_SUBPERIOD)+'</div>');
                    $('.hour:eq('+subperiod+') .top', $(this.el)).append(claster);
                }

                subperiod_events.css({'top' : subperiod_events.parent().height() / 2  - subperiod_events.height() / 2 + 'px', left :'3px'});
                model_events[subperiod] = events;
            }


            if(this.model.get('placeOnMap')){
                //window.map.placeEvents(this.model.get('events'));
            }

        },

        applyFilter : function(event){
            return AppOptions.category(event.icono) && (AppOptions.search ? (event.titulo.search(AppOptions.searchText) > -1 ) : true);
        },


        remove: function() {
			if (typeof TL_Ajax_Object[this.model.get('itemId')] != 'undefined')
				TL_Ajax_Object[this.model.get('itemId')].abort();
            this.trigger('destroy', $(this.el).width());
            $(this.el).remove();
        },

        filterEvents : function(){
            var events = this.model.get('events');

            var subperiods_number = events.length;

            var visible_index = -2;
            var was_visible = 0;
            var showBubbles = AppOptions.get('showBubbles');

            for(var hour=0; hour< subperiods_number; hour++){

                var num_events = events[hour].length;
                if(num_events > 0){
                    var visible_events = 0;

                    new_visible_events = [];
                    for(var i=0; i< num_events; i++){
                        var event = events[hour][i];
                        //was event before visible
                        visible_index = this.visibleEvents[hour].indexOf(event.id);
                        was_visible = ( visible_index == -1 ) ? false : true;

                        if(this.applyFilter(event)){
                            visible_events++;

                            if(visible_events <= EVENTS_IN_SUBPERIOD){

                                if (! was_visible){
                                    var ev = new EventView(event);
                                    $('.hour:eq('+hour+') .top .events ', $(this.el)).append( $(ev.render(showBubbles)) );
                                    if(!this.visibleEvents[hour]) this.visibleEvents[hour] = [];
                                    this.visibleEvents[hour].push(event.id);
                                }
                            }else{
                                if (was_visible)  {
                                    $('.hour:eq('+hour+') .top .events #'+event.id, $(this.el)).remove();
                                    //remove from visible array
                                    this.visibleEvents[hour].splice(visible_index, 1);
                                }
                            }
                            events[hour][i].visible = true;
                        }else{
                            if (was_visible)  {
                                $('.hour:eq('+hour+') .top .events #'+event.id, $(this.el)).remove();
                                //remove from visible array
                                this.visibleEvents[hour].splice(visible_index, 1);
                            }
                            events[hour][i].visible = false;
                        }
                    }



                    var claster = $('.hour:eq('+hour+') .top .claster', $(this.el));

                    if (visible_events > EVENTS_IN_SUBPERIOD){

                        if (!claster.length){
                            claster = $('<div class="claster"></div>');
                            $('.hour:eq('+hour+') .top', $(this.el)).append(claster);
                        }

                        claster.text(visible_events - EVENTS_IN_SUBPERIOD);
                    }else{
                        claster.remove();
                    }

                    var top = $('.hour:eq('+hour+') .top', $(this.el));

                    $('.events', top).css({'top' : top.height() / 2  -  $('.events', top).height() / 2 + 'px', left :'3px'});
                }

            }

            if(this.model.get('placeOnMap')){
                window.map.filterEvents(events);
            }

        }

    });




});
