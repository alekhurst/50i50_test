// TODO: correct Time Ruler scroller bug
$(function () {

	window.Interface = Backbone.View.extend({

		initialize:function () {
			$(window).resize(function () {
				Interface.ResizeWindow();
			});
//			$('#costume_settings_show input').tzCheckbox({labels:['Enable','Disable']});
//			$('.popup-settings').css({  visibility: 'visible', display: 'none' });
			$('#costume_settings input').each(function () {
				if ($(this).attr("checked")) {
					$(this).parent().addClass('act');
					$($(this).val()).show();
				} else {
					$($(this).val()).hide();
					$(this).parent().removeClass('act');
				}
			});
//			$("#type_window").buttonset();

			var global_filter = getCookie('GLOBAL_FILTER');
			global_filter = deserialize(global_filter);

			for (var key in global_filter) {
				if (key == 'All' || key == 'Title' || key == 'Category' || key == 'Timeline' || key == 'Tag') {
					$('#type_search').val(key);
					$('#search_term').val(global_filter[key]);
				}
			}

			var type_page = $('#type_window input:checked').val();
			this.EventsButtonGenerate(type_page);

			this.interface_events();

			this.updateEventShowBubbles();
		},

		ResizeWindow:function () {
			var topMenuHeight = OptionsInterface.get('topMenuHeight');

			if (topMenuHeight > 0)
				OptionsInterface.set({ 'topMenuHeight':0 });

			var right_width = $(window).width() - OptionsInterface.get('leftSidebarWidth') - 13;
			var height = $(window).height() - $('#menu').height() - 13 + OptionsInterface.get('timeRollerHeight') + topMenuHeight;
			SUBITEM_HEIGHT = height - 50;

//			$('#timeline').css({height: 50 + OptionsInterface.get('controlPanelHeight')});

			$('.right-block-shadow').width(right_width).height(height);
			$('#left-block').height(height - 5);

			this.countCollectionLength();

			if (!App)
				App = new TimelineView();
//				$('#right-block').LoadingBox('start', 0);

			var control_panel = OptionsInterface.get('controlPanelHeight');
			if (control_panel > 13)
				control_panel = $('.timeline_settings').parent().height() - 13;

			if (OptionsInterface.get('activeWindow') == 'line')
				$('#right-block #timeline').height(height);
			else
				$('#right-block #timeline').height(50 + control_panel);

			$('#right-block').width(right_width).height(height);
			$('#right-block #map, #right-block #grid_mode_new, #right-block #multimedia').width(right_width).height(SUBITEM_HEIGHT);
			google.maps.event.trigger(map.gMap, "resize");
			$('#timeline .events').height(SUBITEM_HEIGHT);
			$('#right-block').LoadingBox('resize');

			if (right_width != App.windowWidth) {
				App.windowWidth = right_width;
				App.recalculateBorders();
			}

// type_window box to center
			var right_width_tw = $('#costume_settings').width() + 23;
//			var left_width_tw = $('#right-block .timeline_settings .zoom-block').width() + $('#current-period-view').width() + $('#right-block .timeline_settings .add-set').width() + 114;
			var q_width = $('#type_window').width() / 2;
			var margin_left_tw = right_width / 2 - right_width_tw - q_width;
			if (margin_left_tw < 0 ) {
				$('#type_window').css('margin-right', '10px');
			} else {
				$('#type_window').css('margin-right', margin_left_tw);
			}

		},

		ResizeAllEvents:function () {
			var current_period = $('#current-period option:selected'),
				direction = false,
				events_block = [],
				events_block_size = 0,
				time_line = $('#timeline .item');

			for (var i = 0; i < COLLECTION_LENGTH; i++) {
				var el = time_line.eq(i).find('.subitems .si').length;
				events_block[events_block.length] = el;
				events_block_size += el * R_SUBITEM_WIDTH;
			}
//TODO: Edit Resize, flexible
			if (R_SUBITEM_WIDTH < 4) {
				direction = current_period.next();
				if (!direction.html()) {
					R_SUBITEM_WIDTH = 4;
					return false;
				}

				R_SUBITEM_WIDTH = 304;
			} else if (R_SUBITEM_WIDTH > 304) {
				direction = current_period.prev();
				if (!direction.html()) {
					R_SUBITEM_WIDTH = 304;
					return false;
				}

				R_SUBITEM_WIDTH = 4;
			}

			$('#timeline .item .subitems .si').css({'width':R_SUBITEM_WIDTH});
			App.iniPeriod();

			if (direction) {
				current_period.attr('selected', false);
				direction.attr('selected', true).trigger('change');
			}

		},


//TODO: Optimization, not download events if they there is
		countCollection:function () {
			var collection = COLLECTION_LENGTH;
			this.countCollectionLength();
			if (COLLECTION_LENGTH == collection)
				return false;

			App.initializePeriod();
			return true;
		},

		countCollectionLength:function () {
			var count = 24;
			switch ($('#current-period option:selected').val()) {
				case 'day' :
					count = 24;
					break;
				case 'hour' :
					count = 60;
					break;
				case 'month' :
					count = 30.4;
					break;
				case 'year' :
					count = 12;
					break;
				case 'decade' :
					count = 10;
					break;
				case 'century' :
					count = 10;
					break;
			}

			var right_width = $(window).width() - OptionsInterface.get('leftSidebarWidth') - 13;
			var collection = Math.ceil(right_width / (SUBITEM_WIDTH * count) + 1);
			if (collection == 1)
				collection += 2;
			else if (collection % 2 == 0)
				collection++;
			COLLECTION_LENGTH = collection;
		},

		interface_events:function () {
			var self = this;

			//	Zoom button And set Date And "to day"
			$('#zoom_plus').click(function () {
				R_SUBITEM_WIDTH += 49;
				self.ResizeAllEvents();
			});
			$('#timeline').dblclick(function () {
				R_SUBITEM_WIDTH += 49;
				self.ResizeAllEvents();
			});
			$('#zoom_minus').click(function () {
				R_SUBITEM_WIDTH -= 49;
				self.ResizeAllEvents();
			});

//	TODO: verification today and scrolling today if date isset.
			$('#set_date').click(function () {
				Interface.getContentForSidebar({
					sidebar:'set_date',
					custom_param:null, // $(click.currentTarget).find('.event_id').val(),
					size:300
				});
//				self.getPageForLeftSidebar('set_date');
			});

			$('#jump_to_now').click(function () { AppOptions.set({'date':new Date()}); App.initializePeriod(); });
			$('#jump_to_here').click(function () { if (OptionsInterface.get('activeWindow') != 'space') $('#space').click(); map.defaultCoordinates(); });
			$('#jump_to_address').submit(function () {
				var val = $(this).find('input[type=text]').val();

				if (val != 'Search address' && val)
					map.setCoordinates({'address':val });

				if (OptionsInterface.get('activeWindow') != 'space')
					$('#space').click();

				return false;
			});
			$('#jump_to_address input[type=text]').focus(function () { if ($(this).val() == 'Search address') $(this).val(''); })
				.blur(function () { if ($(this).val() == '') $(this).val('Search address'); });

			$('a.link_set_date_title').live('click', function () {
				self.setToDayRes($(this).parents('.ev').find('a.link_set_date').html());

				if ((OptionsInterface.get('activeWindow') == 'space') && $(this).parents('.ev').find('.link_to_place').length) {
					var loc = $(this).parents('.ev').find('.link_to_place input').val().split(';');
					if (loc.length != 2)
						loc = $(this).parents('.ev').find('.link_to_place').html();
					self.setToMapCoordinate(loc);
				}

				return false;
			});
			$("#jump_to_time").AnyTime_picker({
				format: "%Y,%m,%d,%H,%i,%s",
				formatUtcOffset: "%: (%@)",
				hideInput: true,
				placement: "inline"
			}).change(function () {
				var date = $(this).val().split(',');

				var to_date = date_to_time ([date[0], date[1], date[2], date[3], date[4], date[5]]);

				AppOptions.set({'date': to_date});
				App.initializePeriod();
				return false;
			});

			$('a.link_set_date').live('click', function () {
				self.setToDayRes($(this).html());
				return false;
			});

			$('.filter_to_tag').live('click', function () {
				setCookie('GLOBAL_FILTER', $.param({Tag:$(this).html() }), COOKIE_LIFE, '/');
				App.updateEventByFilter();

				if ($('#sidebar_filter').length && (!$('#a_result_tabss').parent().hasClass('ui-state-active') || !$('#a_display_tabss').parent().hasClass('ui-state-active'))) {
					$('#a_result_tabss').click();
				} else
					Interface.getContentForSidebar({
						sidebar:'filter',
						custom_param:'search',
						size:300
					});
				return false;
			});
			$('.link_to_place').live('click', function () {

				var loc = $(this).find('input').val().split(';');
				if (loc.length != 2)
					loc = $(this).html();
				self.setToMapCoordinate(loc);

				if (OptionsInterface.get('activeWindow') != 'space')
					$('#space').click();

				return false;
			});




			//	Type Block (Space, Line or Grid) and type events
			$('#type_window input').change(function () {
				$('#type_window label').removeClass('act');
				$(this).parent().addClass('act');
				self.EventsButtonGenerate($(this).val());
			});
			$('#show_bubbles').change(function () {
				OptionsInterface.set({'activeShowBubbles': ($(this).attr('checked')) ? true : false});
//				var bubbles = ($(this).attr('checked')) ? 1 : -1;
				self.updateEventShowBubbles()
			});
//			$('#type_bubbles input').live('change', function () {
//				self.updateShowBubbles()
//			});

			//	Search Input script( Focus, Blur, Keyup and Clear button )
			var default_val = 'Search';
			var def_fil_val = 'Title';
			$('#search_term').focus(function () {
				var val = $(this).val();
				if (val == default_val || val == def_fil_val)
					$(this).val('');

			}).blur(function () {
					var val = $(this).val();
					if (val == '' || val == def_fil_val)
						$(this).val(default_val);

				}).keyup(function () {
					var val = $(this).val();
					if (val != '')
						$(this).parent().find('a.clear-search').fadeIn('fast');
					else
						$(this).parent().find('a.clear-search').fadeOut('fast');

				}).parent().find('a.clear-search').click(function () {
					$('#search_term').val(default_val);
					$(this).fadeOut('fast');
					setCookie('GLOBAL_FILTER', $(this).parents('form').serialize(), COOKIE_LIFE, '/');

					App.initializePeriod();

				});

			//	Close Header block
			$('#header_close .close_box_button').click(function () {
				var close_container = $('#menu .menu_content');
				OptionsInterface.set({ 'topMenuHeight':$('#menu').height() });

				if (close_container.is(':visible')) {
					self.ResizeWindow();
					close_container.slideUp('show');
					$(this).addClass('off');
				} else {
					close_container.slideDown('show', function () {
						self.ResizeWindow();
					});
					$(this).removeClass('off');
				}
				return false;
			});

			//	Close Time Ruler block
			$('#close_time_rules .close_box_button').click(function () {
				var close_container = $('#right-block');
				var timeRollerHeightD = OptionsInterface.get('timeRollerHeightD');

				if (close_container.hasClass('left_open')) {
					OptionsInterface.set({ 'timeRollerHeight': 0 });

					close_container.animate({'top':'0'}, 'show',function () {
						self.ResizeWindow();
					}).removeClass('left_open');
					$(this).removeClass('off');
				} else {
					OptionsInterface.set({ 'timeRollerHeight':timeRollerHeightD });

					self.ResizeWindow();
					close_container.animate({'top':'-=' + timeRollerHeightD}, 'show').addClass('left_open');
					$(this).addClass('off');
				}
				return false;
			});

			$('#close_control_panel .close_box_button').click(function () {
				var close_container = $('.timeline_settings');

				if ($(this).hasClass('off')) {
					close_container.slideDown('slow', function () {
						OptionsInterface.set({ 'controlPanelHeight': $('.timeline_settings').parent().height() });
						self.ResizeWindow();
					});
					$(this).removeClass('off');
				} else {
					OptionsInterface.set({ 'controlPanelHeight': 13 });
					self.ResizeWindow();
					close_container.slideUp('slow');
					$(this).addClass('off');
				}
				return false;
			});

			$('#costume_settings input').change(function () {
				if ($(this).attr("checked")) {
					$(this).parent().addClass('act');
					$($(this).val()).show();
				} else {
					$($(this).val()).hide();
					$(this).parent().removeClass('act');
				}
			});

			//	Button for remove filter category or tag
			$('.filter-remove').live('click', function () {
				$(this).parent().remove();
				return false;
			});

			$('#search_form').submit(function () {
				var search_type = $('#type_search').val();
				var search_term = encodeURIComponent($("#search_term").val());

				var global_filter = {};
				/*
				 = getCookie('GLOBAL_FILTER');
				 if (global_filter && search_type == 'Title') {
				 global_filter = deserialize(global_filter);
				 } else
				 global_filter =
				 * */
				global_filter[search_type] = search_term;

				setCookie('GLOBAL_FILTER', $.param(global_filter), COOKIE_LIFE, '/');
				App.updateEventByFilter();

				if ($('#sidebar_filter').length) {
					if (search_type == 'Title' || search_type == 'All')
						$('#title_filter').val(search_term);

					App.collection.at(1).loadFilterEvent('fr');
					$('#a_result_tabss').click();
				} else
//					self.getPageForLeftSidebar('filter', 'search');
					Interface.getContentForSidebar({
						sidebar:'filter',
						custom_param:'search', // $(click.currentTarget).find('.event_id').val(),
						size:300
					});
				return false;
			});

			//	Scroll Time Ruler an ruler Button
			$('#right-block .arrow-ruler').click(function () {
				var day = new Date(AppOptions.get('date'));
				if ($(this).hasClass('fr')) {
					day = AppPeriod.current.nextDate(day);
				} else {
					day = AppPeriod.current.prevDate(day);
				}
				App.scrollToNewDate(day);
				return false;
			});

			//	Open or Close Left Sidebar.
			$('#left-block .close_box_button').click(function () {
//				self.getPageForLeftSidebar(null, null, LEFT_BLOCK);
				Interface.getContentForSidebar({
					sidebar:null,
					custom_param:null,
					size:null
				});
			});
			$('#Help').click(function () {
				Interface.getContentForSidebar({
					sidebar:'help',
					custom_param:null,
					size:620
				});
//				self.getPageForLeftSidebar('help', null, 600);
			});
			$('#filter_button').click(function () {
				if ($('#sidebar_filter').length && !$('#a_filter_tabss').parent().hasClass('ui-state-active')) {
					$('#a_filter_tabss').click();
					$('#a_filter_tabss').click();
				} else
					Interface.getContentForSidebar({
						sidebar:'filter',
						custom_param:'filter',
						size:300
					});
			});

			$('#search_type_dropdown li').live('click', function () {
				if ($('#search_type_dropdown').hasClass('open')) {
					self.searchDropDown($(this));
				} else {
					$('#search_type_dropdown li').removeClass('active');
					var count_li = $('#search_type_dropdown').find('li').length * 28;
					$('#search_type_dropdown').addClass('open')
						.stop(true, true).animate({ height:count_li + 'px'});
				}
			});
// Multimedia
			$('#multimedia .multimedia-close').click(function () {
				self.EventsButtonGenerate($('#type_window input:checked').val());
				$('.timeline_settings').show();
//				$('#multimedia').hide();

			});

			$('#multimedia .multimedia-menu .multimedia-button').click(function () {
				var block = $(this).data('block');
				$('#multimedia .multimedia-page').hide();
				$('#mm_' + block).show();

				return false;
			});

		},

		EventsButtonGenerate:function (type_window) {
			var time_line_h = OptionsInterface.get('timeRollerHeightD');
//			$('.timeline_settings').show();
			$('.box_5050').hide();

			switch (type_window) {
				case 'space' :
					$('#map').show();
					break;

				case 'grid' :
					$('#grid_mode_new').show();
					break;

				case 'multi' :
					$('#multimedia').show();
//					$('.timeline_settings').hide();
					break;

				case 'line':
				default :
					time_line_h += SUBITEM_HEIGHT;
			}
			$('#timeline').height(time_line_h);
			OptionsInterface.set({ 'activeWindow':type_window });


			var temp_button = OptionsInterface.get('mainPageButton');
			if (typeof(temp_button[type_window]) != 'undefined') {
//				var sub_button_html = '';

//				var checked = OptionsInterface.get('activeShowBubbles');
//				for (var i in temp_button[type_window]) {
//					var check = (typeof(checked[i]) != 'undefined' && checked[i]) ? ' checked="checked"' : '';
//
//					sub_button_html += '<input type="checkbox"' + check + ' class="show_bubbles" name="show_bubbles[]" value="' +
//						i + '" id="show_' + temp_button[type_window][i] + '" /><label for="show_' + temp_button[type_window][i] + '">' +
//						temp_button[type_window][i] + '</label>';
//				}

//				$('#type_bubbles').html(sub_button_html);
//				$("#type_bubbles .show_bubbles").button();

				if (App)
					App.initializePeriod();
			}

			this.ResizeWindow();

		},

//		updateShowBubbles: function () {
//			var bubblesActive = [];
////			$('#type_bubbles input:checked').each(function () {
////				bubblesActive[$(this).val()] = true;
////			});
//
//			OptionsInterface.set({'activeShowBubbles':bubblesActive});
//
//			this.updateEventShowBubbles(bubblesActive);
//		},
//TODO: Optimization, open bubbles in visible area window.
		updateEventShowBubbles:function () {
//			if (! bubblesActive)
			var bubblesActive = OptionsInterface.get('activeShowBubbles');
//			var showBub = false;

			if (bubblesActive > 0) {
				$('#timeline .block').show();
//				showBub = true;
			} else
				$('#timeline .block').hide();

			if (OptionsInterface.get('activeWindow') == 'space')
				window.map.changeShowBubbles(bubblesActive);
		},

		getEventByID: function (id) {
			$('#left-block').LoadingBox(id);

			if (!$('#sidebar_filter').length) {
				Interface.getContentForSidebar({
					sidebar:'filter',
					custom_param:'filter',
					size:300
				}, this.getInfo, id);
			} else {
				if (!$('#content').hasClass('left_open')) {
					this.leftSidebar_Open(300);
				}
				this.getInfo(id);
			}



		},

		getInfo:function (id) {
			$.ajax({
				url: 'server/query.event.php',
				data: {id: id},
				success: function (res) {
					$('#left-block').LoadingBox('finish', id);
					$('#sidebar_filter').hide();

					$('#ep_title').html(res.titulo);
					$('#ep_desc').html(res.descripcion);
					$('#ep_dataType').html(res.f_tipo);
					$('#ep_dateStart').html(res.f_inicio);

					$('#ep_img').html('<img class="fr" src="./server/thumbnails.php?w=70&h=70&p=1&q=60&url='+ res.imagen +'" alt=""/>');
					if (res.f_fin) {
						$('#ep_dateEnd').html(res.f_fin);
						$('#ep_dateEnd').parent().show();
					} else {
						$('#ep_dateEnd').parent().hide();
					}
					if (res.lugar) {
						$('#ep_placeStart').html(res.lugar);
						$('#ep_placeStart').parent().show();
					} else {
						$('#ep_placeStart').parent().hide();
					}
					if (res.lugar2) {
						$('#ep_placeEnd').html(res.lugar2);
						$('#ep_placeEnd').parent().show();
					} else {
						$('#ep_placeEnd').parent().hide();
					}
					if (res.category) {
						$('#ep_category').html(res.category);
						$('#ep_category').parent().show();
					} else {
						$('#ep_category').parent().hide();
					}

					if (res.subcategory) {
						$('#ep_subCategory').html(res.subcategory);
						$('#ep_subCategory').parent().show();
					} else {
						$('#ep_subCategory').parent().hide();
					}

					if (res.subcategory2) {
						$('#ep_subCategory2').html(res.subcategory2);
						$('#ep_subCategory2').parent().show();
					} else {
						$('#ep_subCategory2').parent().hide();
					}


					$('#ep_tags').html(res.etiquetas.join(', '));
					$('#ep_source').html(res.source);
					$('#mm_web_input').html(res.multi_web);
					$('#mm_video_input').html(res.link_video);
//					$('#ep_').html(res.);

					$('#event_preview').show();
				}
			});
		},

		getContentForSidebar:function (settings, back_function, func_param) {
			var sidebar = $('#left-block .lf-content');
			var close_bar = false;

			var LeftSidebarBack = OptionsInterface.get('LeftSidebar');
			var LeftSidebarNew = {};

			for (var i in LeftSidebarBack) {
				if (typeof settings[i] != 'undefined' && settings[i] !== null)
					LeftSidebarNew[i] = settings[i];
				else
					LeftSidebarNew[i] = LeftSidebarBack[i];
			}

			if (LeftSidebarNew.sidebar == LeftSidebarBack.sidebar) {
				close_bar = true;
			}

			switch (LeftSidebarNew.sidebar) {
				case 'events-description' :
					if (LeftSidebarNew.custom_param == LeftSidebarBack.custom_param && close_bar)
						close_bar = true;
					else
						close_bar = false;
					break;
				case 'filter' :
					if (LeftSidebarNew.custom_param == LeftSidebarBack.custom_param && close_bar)
						close_bar = true;
					else
						close_bar = false;
				case 'help' :
//					LeftSidebarNew.size = 600;
					break;
			}


			if (close_bar && sidebar.html()) {
				this.leftSidebar_Close(LeftSidebarNew.size);
				return false;
			}

			sidebar.html('');
			$('#left-block').LoadingBox(1000);
			OptionsInterface.set({'activeFilter':false});
			sidebar.load('server/open-sidebar.php', LeftSidebarNew, function () {
				$('#left-block').LoadingBox('finish', 1000);
				if (typeof back_function == 'function') {
					return back_function(func_param);
				}
			});


			this.leftSidebar_Open(LeftSidebarNew.size);

			OptionsInterface.set({'LeftSidebar':LeftSidebarNew});

		},


		leftSidebar_Open:function (size) {
			var self = this;
			var parent_box = $('#content');
			var sidebar_button = $('#left-block .close_box_button');
			var back_size = OptionsInterface.get('leftSidebarWidth');

			OptionsInterface.set({ 'leftSidebarWidth':size });

			if (size < back_size)
				self.ResizeWindow();

			parent_box.animate({'left':size + 'px' }, 'show',function () {
				self.ResizeWindow();
			}).addClass('left_open');
			sidebar_button.addClass('off');

			$('#left-block').css({'left':-size + 'px', 'width':size + 'px'})
				.find('.lf-content').show();

			$('#left-block').LoadingBox('resize');
			$('#left-block').LoadingBox('show');
		},


		leftSidebar_Close:function (size) {
			var parent_box = $('#content');
			var sidebar_button = $('#left-block .close_box_button');

			OptionsInterface.set({ 'leftSidebarWidth':0 });

			if (!parent_box.hasClass('left_open')) {
				this.leftSidebar_Open(size);
				return false;
			}

			this.ResizeWindow();
			parent_box.animate({'left':'0'}, 'show').removeClass('left_open');
			sidebar_button.removeClass('off');

			$('#left-block').css({'left':0, 'width':0})
				.find('.lf-content').hide();

			$('#left-block').LoadingBox('hide');
		},

		setToDayRes:function (date_str) {
			var date_obj,
				current_date,
				current_period,
				arr_date_time = ($.trim(date_str)).split(' ');

			var arr_date = arr_date_time[0].split('/');
			switch (arr_date.length) {
				case 1:
					current_date = [Number(arr_date[0]), 0, 0, 0, 0, 0];
					current_period = 'decade';
					break;
				case 2:
					current_date = [Number(arr_date[1]), Number(arr_date[0]), 0, 0, 0, 0];
					current_period = 'year';
					break;
				case 3:
					current_date = [Number(arr_date[2]), Number(arr_date[0]), Number(arr_date[1]), 0, 0, 0];
					current_period = 'month';
			}


			if (arr_date_time.length == 2) {
				var arr_time = arr_date_time[1].split(':');
				current_date[3] = Number(arr_time[0]);
				current_date[4] = Number(arr_time[1]);
				current_date[5] = Number(arr_time[2]);
			}
			date_obj = date_to_time(current_date);

			AppOptions.set({'date':date_obj});

			App.initializePeriod();

		},


		searchDropDown:function (this_el) {
			$('#search_type_dropdown li').removeClass('select');

			this_el.addClass('select active');
			var type = this_el.find('a').html();
			$('#search_type_dropdown').prepend(this_el.clone());
			this_el.remove();

			var prev_search = '';
			$('#type_search').val(type);
			switch (type) {
				case 'Category' :
					break;
				case 'Tag' :
					break;
				case 'Timeline' :
					type = 'tag';
					prev_search = 'Timeline of ';
					break;
				default:
					type = false;
//					$("#search_term").autocomplete('destroy');
					return false;
			}


		},

		setToMapCoordinate:function (coord) {
			var val = false;
			if (typeof coord != 'string')
				val = {location:new google.maps.LatLng(parseFloat(coord[0]), parseFloat(coord[1]))};
			else
				val = {'address':$(this).html() };

			map.setCoordinates(val);
		},

		updateMultimediaBox:function (title) {
			console.log(title);
			var block = {'web':'mm_web', 'video':'mm_video', 'img':'mm_img', 'link':'mm_link', 'sponsor':'mm_sponsor'};

			$('#' + block.web + ', #' + block.video + ', #' + block.img + ', #' + block.link).html();

			var iframe_link = $('#' + block.web + '_input').val();

			if (iframe_link) {
				$('#' + block.web).html('<iframe width="100%" height="100%" src="' + iframe_link + '"></iframe>');
			} else {
				webSearchControl.addSearcher(new google.search.WebSearch());
				webSearchControl.draw(document.getElementById(block.web), drawOptions);
				webSearchControl.execute(title);
			}

			videoSearchControl.addSearcher(new google.search.VideoSearch());
			videoSearchControl.draw(document.getElementById(block.video), drawOptions);
			videoSearchControl.execute(title);

			imgSearchControl.addSearcher(new google.search.ImageSearch());
			imgSearchControl.draw(document.getElementById(block.img), drawOptions);
			imgSearchControl.execute(title);


			var pageOptions = {
				'pubId':'pub-7599157858866370',
				'query':title
			};

			var adblock1 = {
				'container':'adcontainer1'
			};
			new google.ads.search.Ads(pageOptions, adblock1);
		}

	});


});

/*
 *
 *
 *  Loading Box
 */
(function ($) {
	var methods = {
		class_name:'map_shadow',

		start:function (id) {
			var div_class = '.' + methods.class_name;

			if (this.find(div_class).length) {
				if (this.find(div_class).data('id') == id)
					return methods.resize.apply(this, []);
				else
					return methods.refresh.apply(this, [id]);
			}

			var top = this.height() / 2;
			var left = this.width() / 2;

			this.append('<div class="' + methods.class_name + '" data-id="' + id + '"><div style="left: ' + left + 'px; top: ' + top + 'px;"></div></div>');

			var self = this;
			$(window).resize(function () {
				return methods.resize.apply(self, []);
			});

			return 'start';
		},

		finish:function (id) {
			var div_class = '.' + methods.class_name;
			var current_shadow = this.find(div_class);

			if (current_shadow.length && this.find(div_class).data('id')) {
				this.find(div_class).remove();
			}

			return 'finish';
		},

		refresh:function (id) {
			var div_class = '.' + methods.class_name;
			var current_shadow = this.find(div_class);

			current_shadow.data('id', id);

			return 'refresh';
		},

		resize:function () {
			var width = 300;
			this.find('.' + methods.class_name).find('div').css({'left':this.width() / 2, 'top':this.height() / 2, 'width':width + 'px'});

			return 'resize';
		},

		show:function () {
			this.find('.' + methods.class_name).show();
		},

		hide:function () {
			this.find('.' + methods.class_name).hide();
		}
	};

	$.fn.LoadingBox = function (method, id) {
		if (methods[method])
			return methods[method].apply(this, [id]);
		else
			return methods.start.apply(this, [method]);
	};


	$.fn.tzCheckbox = function(options){

		// Default On / Off labels:

		options = $.extend({
			labels : ['ON','OFF']
		},options);

		return this.each(function () {
			var originalCheckBox = $(this),
				labels = [];

			// Checking for the data-on / data-off HTML5 data attributes:
			if(originalCheckBox.data('on')){
				labels[0] = originalCheckBox.data('on');
				labels[1] = originalCheckBox.data('off');
			}
			else labels = options.labels;

			// Creating the new checkbox markup:
			var checkBox = $('<span>',{
				className	: 'tzCheckBox '+(this.checked?'checked':''),
				html:	'<span class="tzCBContent">'+labels[this.checked?0:1]+
					'</span><span class="tzCBPart"></span>'
			});

			// Inserting the new checkbox, and hiding the original:
			checkBox.insertAfter(originalCheckBox.hide());

			checkBox.click(function(){
				checkBox.toggleClass('checked');

				var isChecked = checkBox.hasClass('checked');

				// Synchronizing the original checkbox:
				originalCheckBox.attr('checked',isChecked);
				checkBox.find('.tzCBContent').html(labels[isChecked?0:1]);
			});

			// Listening for changes on the original and affecting the new one:
			originalCheckBox.bind('change',function(){
				checkBox.click();
			});
		});
	};
})(jQuery);
