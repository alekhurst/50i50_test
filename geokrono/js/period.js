$(function(){

    Period = Backbone.Model.extend({

//        viewClass : null,
//        modelClass : null,
//        type : null,

        current : null,
//        defaults : function(){
//            return { viewClass: 'DayView', modelClass : Day}
//        },

        change : function(viewPeriod){



            switch(viewPeriod){
                case 'month': this.current = new Month();break;
                case 'hour': this.current = new Hour();break;
				case 'year': this.current = new Year();break;
				case 'decade': this.current = new Decade();break;
				case 'century': this.current = new Century();break;
                default:
                    this.current = new Day();
            }
        }


    });

    AppPeriod = new Period();
});