$(function(){

	window.SearchFilter = Backbone.View.extend({
		initialize: function () {

		}
	});


});

$(document).ready(function () {
	$("#search_term").focus(function () { if ($('#type_search').val() != 'Title') { $('#search-res-block').show(); } });

	$("#search_term").keyup(function () {
		$('#search-res-block').html();
		if ($(this).val().length < 1)
			return false;

		var prev_search = '';
		var type = $('#type_search').val();
		switch (type) {
			case 'Category' :
				break;
			case 'Tag' :
				break;
			case 'Timeline' :
				type = 'tag';
				prev_search = 'Timeline of ';
				break;
			default:
				type = false;
				return false;
		}

		$('#search-res-block .search-res-status').html('Searching...');

		$.get('server/open-sidebar.php', {
			'sidebar': type.toLowerCase() + '-list',
			'tag': prev_search + $(this).val()
		}, function (resp) {
			if (resp && resp.length) {
				$('#search-res-block .search-result').html('');
				var i = 0;
				AUTOCOMPLETE_ARR = Array();
				for (id in resp) {
					if (i > 10) {
						AUTOCOMPLETE_ARR[id] = resp[id];
						$('.search-res-more').show();
					} else {
						$('#search-res-block .search-result').append('<li>' + resp[id].tag + '</li>');
						$('.search-res-more').hide();
					}
					i++;
				}
				$('#search-res-block .search-res-status').html('');


			} else {
				$('#search-res-block .search-res-status').html('No suggested matches');
				$('#search-res-block .search-result').html('');
				AUTOCOMPLETE_ARR = Array();
			}
		}, 'json');


	});

	$('#search-res-block .search-res-more').click(function () {
		$('#search_term').focus();
		var arr = AUTOCOMPLETE_ARR;
		AUTOCOMPLETE_ARR = Array();
		var i = 0;
		for (id in arr) {
			if (i > 10) {
				AUTOCOMPLETE_ARR[id] = arr[id];
				$('.search-res-more').show();
			} else {
				$('#search-res-block .search-result').append('<li>' + arr[id].tag + '</li>');
				$('.search-res-more').hide();
			}
			i++;
		}

	});

	$(window).click(function (el) {
		if ($('#type_search').val() != 'Title') {
			if ($(el.target).attr('id') == 'search_term')
				$('#search-res-block').show();
			else if ($(el.target).parents('div#search-res-block').length)
				$('#search-res-block').show();
			else
				$('#search-res-block').hide();
		} else
			$('#search-res-block').hide();
	});
	$('#search-res-block .search-result li').live('click', function () {
		var decoded = $("<div/>").html($(this).html()).text();
		$('#search_term').val(decoded);
		$(this).addClass('active');
	});
});