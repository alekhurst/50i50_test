��    )      d  ;   �      �  W   �     �     �               $     5     9     @     N     c     l     �     �     �     �     �     �     �     �  8   �                    &     -     :  !   G     i     r  
   z     �     �     �     �     �     �     �     �     �  '    �   /     �  5   �       #        9     U     ^     m  '   �     �  ;   �     	  *    	     K	     X	  
   s	     ~	  
   �	     �	  s   �	     
     )
     6
  
   E
     P
  %   g
  J   �
  
   �
  $   �
          "     ;  ,   [  0   �     �  "   �  #   �          ,         
          #              &       	   $   !   )             (      %                                              '                                                        "                             Are you sure you want to delete your account with all the information you have created? Cancel Center in Time-Space Close Confirm Password Current Password Day Delete Delete Event? Delete your account? Discover Enter Username or Email Forgot your password? Go to Login Help Help & Feedback Here Hour Login New Password No account yet? <a href="#user_signup">Register here</a> Now Password Private Public Registration Save Changes Search (movie, news, event, etc.) Sign Out Sign Up Time scale User Username Username or Email What's happening around you? Your Account Your Information confirm password new password username Project-Id-Version: 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 03002013-08-15 12:04+
PO-Revision-Date: 2013-08-15 12:04+
Last-Translator: Pavel <test@test.com>
Language-Team: ru_RU <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Вы уверены, что хотите удалить свой аккаунт со всей информацией что высоздали? Отменить Центр в пространстве-времени Закрыть Подтвердите пароля Текущий пароль День Удалить Удалить событие? Удалить свой аккаунт? Обнаружить Введите Имя пользователя или Email Забыли пароль? Вернутся к Авторизации Помощь Помощь и связь Здесь Час Войти Новый пароль Еще нет учетной записи? <a href="#user_signup">Зарегистрируйтесь здесь</a> Сейчас Пароль Частный Общий Регистрация Сохранить изминения Искать (фильмы, новости, события, другое.) Выйти Зарегистрироваться Шкала времени Пользователь Имя пользователя Имя пользователя или Email Что происходит вокруг вас? Ваш Акаунт Информация про вас Подтвердите пароля новый пароль имя пользователя 