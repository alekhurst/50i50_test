<?php
require_once '../../server/global.php';
if (empty($_FILES)) {
	exit;
}

$files = array();
foreach ($_FILES as $key => $file) {

	if (empty($file["error"])) {
		$folder = date('dmy');
		if (!is_dir(PUBLIC_FILE_DIR . $folder)) {
			if (!mkdir(PUBLIC_FILE_DIR . $folder, 0777, true)) {
				exit;
			}
		}

		$tmp_name = $file["tmp_name"];
		$extension = pathinfo($file["name"], PATHINFO_EXTENSION);
		$name = substr(md5($file["name"] . date('His')), 0, 20) . '.' . $extension;

//		var_dump(move_uploaded_file($tmp_name, PUBLIC_FILE_DIR . $folder . '/' . $name));
		if (move_uploaded_file($tmp_name, PUBLIC_FILE_DIR . $folder . '/' . $name)) {
			$option_scaled = array(
				'max_width' => SCALED_IMG_WIDTH,
				'max_height' => SCALED_IMG_HEIGHT,
				'crop' => SCALED_IMG_CROP
			);
			create_scaled_image(PUBLIC_FILE_DIR . $folder . '/' . $name, $extension, $option_scaled);

			$files[] = array('url' => PUBLIC_FILE_URL . $folder . '/' . $name);
		}

	}
}

echo json_encode($files);
exit;

function create_scaled_image($file_path, $extension, $options)
{
	$new_file_path = $file_path;

	if (!function_exists('getimagesize')) {
		error_log('Function not found: getimagesize');
		return false;
	}
	list($img_width, $img_height) = @getimagesize($file_path);
	if (!$img_width || !$img_height) {
		return false;
	}
	$max_width = $options['max_width'];
	$max_height = $options['max_height'];
	$scale = min(
		$max_width / $img_width,
		$max_height / $img_height
	);
	if ($scale >= 1) {
		if ($file_path !== $new_file_path) {
			return copy($file_path, $new_file_path);
		}
		return true;
	}
	if (!function_exists('imagecreatetruecolor')) {
		error_log('Function not found: imagecreatetruecolor');
		return false;
	}
	if (empty($options['crop'])) {
		$new_width = $img_width * $scale;
		$new_height = $img_height * $scale;
		$dst_x = 0;
		$dst_y = 0;
		$new_img = @imagecreatetruecolor($new_width, $new_height);
	} else {
		if (($img_width / $img_height) >= ($max_width / $max_height)) {
			$new_width = $img_width / ($img_height / $max_height);
			$new_height = $max_height;
		} else {
			$new_width = $max_width;
			$new_height = $img_height / ($img_width / $max_width);
		}
		$dst_x = 0 - ($new_width - $max_width) / 2;
		$dst_y = 0 - ($new_height - $max_height) / 2;
		$new_img = @imagecreatetruecolor($max_width, $max_height);
	}
	switch (strtolower($extension)) {
		case 'jpg':
		case 'jpeg':
			$src_img = @imagecreatefromjpeg($file_path);
			$write_image = 'imagejpeg';
			$image_quality = isset($options['jpeg_quality']) ?
				$options['jpeg_quality'] : 75;
			break;
		case 'gif':
			@imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
			$src_img = @imagecreatefromgif($file_path);
			$write_image = 'imagegif';
			$image_quality = null;
			break;
		case 'png':
			@imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
			@imagealphablending($new_img, false);
			@imagesavealpha($new_img, true);
			$src_img = @imagecreatefrompng($file_path);
			$write_image = 'imagepng';
			$image_quality = isset($options['png_quality']) ?
				$options['png_quality'] : 9;
			break;
		default:
			$src_img = null;
	}
	$success = $src_img && @imagecopyresampled(
		$new_img,
		$src_img,
		$dst_x,
		$dst_y,
		0,
		0,
		$new_width,
		$new_height,
		$img_width,
		$img_height
	) && $write_image($new_img, $new_file_path, $image_quality);
	// Free up memory (imagedestroy does not delete files):
	@imagedestroy($src_img);
	@imagedestroy($new_img);
	return $success;
}