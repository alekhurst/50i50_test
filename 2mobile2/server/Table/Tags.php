<?php

class Table_Tags extends DB
{
	static private $tb = 'tags';
	function addTag($tag)
	{
		$query = "INSERT INTO " . self::$tb . "(tag) VALUE('" . $this->escape($tag) . "')";
		$res = mysql_query($query);

		if ($res) {
			return mysql_insert_id();
		}

		return false;
	}

	function addTags($tags)
	{
		$values = '';
		for ($i = 0, $c = count($tags); $i < $c; $i++) {
			$values .= "('" . $this->escape($tags[$i]) . "')";
			if ($i < $c-1) {
				$values .= ',';
			}
		}
		$query = "INSERT INTO " . self::$tb . "(tag) VALUES" . $values;

		$res = mysql_query($query);

		if ($res) {
			return true;
		}

		return false;
	}

	function getTagsByTags($tags)
	{
		$in = '';
		for ($i = 0, $c = count($tags); $i < $c; $i++) {
			$in .= "'" . $this->escape($tags[$i]) . "'";

			if ($i < $c-1) {
				$in .= ',';
			}
		}

		$tags = array();

		$query = "SELECT tag FROM " . self::$tb . ' WHERE tag IN (' . $in . ')';
		$res = mysql_query($query);
		if (mysql_num_rows($res)) {

			while ($row = mysql_fetch_assoc($res)) {
				$tags[] = $row['tag'];
			}
		}

		return $tags;
	}
}
