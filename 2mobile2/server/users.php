<?php

require_once '../../server/config.php';
require_once PATH_MOBILE . 'server/Controller/Users.php';

$action = isset($_REQUEST['action']) ? trim($_REQUEST['action']) : null;
$result_type = isset($_REQUEST['json']) ? 'json' : 'html';

$user = new Controller_Users($result_type);
echo $user->action($action);
exit;