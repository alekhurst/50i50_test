<?php
require_once PATH_MOBILE . 'server/Table/Tags.php';

class Obj_Tags {
	static function addNewTags($str)
	{
		if (empty($str)) {
			return false;
		}
		$tags = explode(';', $str);

		$tbTags = new Table_Tags();
		$arrTags = $tbTags->getTagsByTags($tags);

		for ($i = 0, $c = count($arrTags); $i < $c; $i++) {
			$tag = strtolower(trim($arrTags[$i]));

			foreach ($tags as $key => $val) {
				if (strtolower(trim($val)) == $tag) {
					unset($tags[$key]);
				}
			}
		}

		$tags = array_values($tags);
		if (count($tags)) {
			$tbTags->addTags($tags);
		}

		return true;
	}
}