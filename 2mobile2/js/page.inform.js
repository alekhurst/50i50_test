var UPLOAD_WARNING = '';

/* ----- Data/Time JS ----- */
function changeTypeDate(el) {
	$('#time_start, #time_end, #loc_start_time, #loc_end_time').val('');

	var time = ($(el.target).val() == 'hour') ? 24 : 60;

	$('#time_slider').attr('max', time).slider('refresh');
}

function changeDateTime(el) {
	var date = $(el.target).mobiscroll('getDate');

	if ($(el.target).attr('id') == 'time_end') {
		$('#time_start').mobiscroll().datetime({
				theme: 'jqm',
				display: 'modal',
				animate: 'slidevertical',
				mode: 'scroller',
				maxDate: date
			});
		$('#loc_end_time').val(date.getHours() + ',' + date.getMinutes() + ',' + date.getSeconds() + ',' + (date.getMonth() + 1) + ',' + date.getDate() + ',' + date.getFullYear());
	} else {
		$('#loc_start_time').val(date.getHours() + ',' + date.getMinutes() + ',' + date.getSeconds() + ',' + (date.getMonth() + 1) + ',' + date.getDate() + ',' + date.getFullYear());
		$('#time_end').mobiscroll().datetime({
			theme: 'jqm',
			display: 'modal',
			animate: 'slidevertical',
			mode: 'scroller',
			minDate: date
		});
	}
}

/* ----- Location JS ----- */
function setCoordinates(address) {
	var geo = new google.maps.Geocoder();

	geo.geocode(address, function (res) {
		if (!res.length) {1
			$('#message_inform').html('<p class="warning">Location: Position unavailable</p>').popup('open');
			$('#loc_custom_coordinate, #loc_custom_address').val('');
		} else {
			$('#loc_custom_address').val(res[0].formatted_address);
			$('#loc_custom_coordinate').val(res[0].geometry.location.lat() + ',' + res[0].geometry.location.lng());
		}
	});
}

function defaultCoordinates() {

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(
			function (position) {
				setCoordinates({location: new google.maps.LatLng(position.coords.latitude, position.coords.longitude)});
			},

			function (error) {
				var info;

				switch (error.code) {
					case error.TIMEOUT:
						info = 'Location: Timeout';
						break;
					case error.POSITION_UNAVAILABLE:
						info = 'Location: Position unavailable';
						break;
					case error.PERMISSION_DENIED:
						info = 'Location: Permission denied';
						break;
					case error.UNKNOWN_ERROR:
						info = 'Location: Unknown error';
						break;
				}

				if (info) {
					$('#message_inform').html('<p class="warning">' + info + '</p>').popup('open');
				}

			}
		);

	}
}


jQuery(document).ready(function ($) {
	/* Basic Box */
	$('#fileupload').fileupload({
		url: URL_MOBILE + 'server/inform.upload.img.php',
		dataType: 'json',
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
		maxFileSize: 5000000, // 5 MB
		loadImageMaxFileSize: 15000000, // 15MB
		disableImageResize: false,
		previewMaxWidth: IMAGE_SIZE[0],
		previewMaxHeight: IMAGE_SIZE[1],
		previewCrop: IMAGE_SIZE[2],
		done: function (e, data) {
			UPLOAD_WARNING = '';

			$.each(data.result, function (index, file) {
				$('#upload_file_link').val(file.url);
			});

			$('.bar', '#progress').css('width', '0').hide();
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('.bar', '#progress').show().css('width', progress + '%');
		}

	}).on('fileuploadadd', function () {
			UPLOAD_WARNING = 'Loading File....';

		}).on('fileuploadprocessalways', function (e, data) {
			var index = data.index,
				file = data.files[index],
				node = $('.box_upload_img_view', '#box_upload_img');

			if (file.preview) {
				node.html(file.preview);
			}

			if (file.error) {
				UPLOAD_WARNING = file.error;
			}

			node.slideDown()
				.parents('#box_upload_img').find('#remove_img').show();

		}).on('fileuploadfail', function (e, data) {
			$.each(data.result.files, function (index, file) {
				UPLOAD_WARNING = file.error;
			});

			$('.bar', '#progress').fadeOut().css('width', '0');
		});

	$('.box_upload_text_view', '#box_upload_img').on('vclick', function () {
		$('#fileupload').trigger('click').trigger('vclick');
	});

	$('#remove_img').on('vclick', function () {
		UPLOAD_WARNING = '';
// TODO Remove images in server
		$('#remove_img').hide();
		$('.box_upload_img_view', '#box_upload_img').slideUp();
		$('#upload_file_link').val('');

	});

	/* Category Box */
	$('.cat_list h3', '#cat_list').on('click vclick', function () {
		var id = $(this).data('cat_id');
		var url = URL_MOBILE + 'server/inform.catategories.php';
		var content = $(this).next();

		if (!content.find('input').length) {
			content.load(url, {cat_id: id}, function () {
				$(this).controlgroup()
					.find('input').checkboxradio()
					.end()
					.find('.cat_list').collapsible()
					.find('h3').bind('vclick click', function () {
						var child_id = $(this).data('child_id');
						var id = $(this).data('cat_id');
						var sub_content = $(this).next();

						if (!sub_content.find('input').length) {
							sub_content.load(url, {cat_id: id, child_id: child_id}, function () {
								$(this).find('input').checkboxradio();
							});
						}
					});
			});
		}

	});

	/* Data/Time */
	$('input', '#type_time_box').change(changeTypeDate);

	$('#time_end').change(changeDateTime).mobiscroll().datetime({
		theme: 'jqm',
		display: 'modal',
		animate: 'slidevertical',
		mode: 'scroller'
	});
	$('#time_start').change(changeDateTime).mobiscroll().datetime({
		theme: 'jqm',
		display: 'modal',
		animate: 'slidevertical',
		mode: 'scroller'
	});

	/* Location */
	defaultCoordinates();

	$('#loc_address').change(function () {
		$('#loc_hear').attr("checked", false).checkboxradio("refresh");
		$('#loc_coordinates').val('');

		setCoordinates({address: $('#loc_address').val()});
	});

	$('#loc_coordinates').change(function () {
		$('#loc_hear').attr("checked", false).checkboxradio("refresh");
		$('#loc_address').val('');

		var coordinate = $('#loc_coordinates').val().split(',');
		if (coordinate.length >= 2) {
			setCoordinates({location: new google.maps.LatLng(coordinate[0], coordinate[1])});
		}
	});

	$('#loc_hear').change(function () {
		if ($(this).is(':checked')) {
			defaultCoordinates();
			$('#loc_coordinates, #loc_address').val('');
		}
	});

	$('#submit_inform').click(function () {
		$('#form_inform').submit();
	});

	$('#form_inform').submit(function () {
		var error = [],
			title =  $('#share_title').val(),
			location = $('#loc_custom_coordinate').val(),
			date = new Date;

		if (!title) {
			error.push('<p class="error">Please, enter a title.</p>');
		}

		if (!location) {
			error.push('<p class="error">Please, enter location.</p>');
		}

		if (UPLOAD_WARNING) {
			error.push('<p class="warning">' + UPLOAD_WARNING + '</p>');
		}

		if (error.length > 0) {
			$('#message_inform').html(error.join("\n")).popup('open');
			return false;
		}

		$('#loc_default_time').val(date.getHours() + ','
			+ date.getMinutes() + ','
			+ date.getSeconds() + ','
			+ (date.getMonth() + 1) + ','
			+ date.getDate() + ','
			+ date.getFullYear());

		$.ajax({
			url: URL_MOBILE + 'server/users.php',
			data: $(this).serialize(),
			type: 'post',
			dataType: 'json',
			success: function (resp) {
				$('#message_inform').html(resp.info).popup('open');

				if (!resp.error) {
					window.location.href = URL_MOBILE;
				}
			}
		});

		return false;
	});
});