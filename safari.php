<?php
/* Get Cookie For Splash Page */
$splash_cookie_name = 'splash_page_seen'; // name of cookie
$splash_cookie_value = 'redirection'; // what we gonna store in cookie
$splash_cookie_expire = time() + (192 * 60 * 60); // expiration date of cookie 8 hours from now

/*Get Cookie For Showing Popup On iOS*/
$safari_cookie_name = 'show_popup_on_entry';
$safari_cookie_value = 'popup';
$safari_cookie_expire = time() + (240); // expiration date of cookie 10 seconds from now

setcookie($splash_cookie_name, $splash_cookie_value, $splash_cookie_expire, '/');  //set splash page seen
setcookie($safari_cookie_name, $safari_cookie_value, $safari_cookie_expire, '/');  //set show popup on entry

header("Location: mobile/index.php");

?>