<?php
require_once 'PHPMailer_5.2.4/class.phpmailer.php';

class Mail
{
	private $mail;

	function __construct()
	{
		$this->mail = new PHPMailer();

		$this->mail->IsSMTP();
//		$this->mail->SMTPDebug = 2;

		$this->mail->SMTPAuth = true;
		$this->mail->Host = SMTP_HOST;
		$this->mail->Port = SMTP_PORT;
		$this->mail->Username = SMTP_USERNAME;
		$this->mail->Password = SMTP_PASSWORD;

		$this->mail->SetFrom(SMTP_USERNAME, '50I50.ORG');

//		$this->mail->AddReplyTo("50i50@fadsf.df", "test test");
	}

	function setSubject($text)
	{
		$this->mail->Subject = $text;
		return $this;
	}

	function setMsg($text)
	{
		$this->mail->MsgHTML($text);
	}

	function addAddress($mail, $name = '')
	{
		$this->mail->AddAddress($mail, $name);
	}

	function send()
	{

		if (!$this->mail->Send()) {
			return $this->mail->ErrorInfo;
		}

		return false;
	}

}