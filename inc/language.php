<?php


if ( false === function_exists('gettext') ) {
	echo "You do not have the gettext library installed with PHP.";
	exit;
}

switch ($loc) {
	case 'ru' :
		putenv("LC_ALL=ru_RU.UTF-8");
		setlocale(LC_ALL, "ru_RU.UTF-8");
		break;
	case 'es' :
		putenv ("LC_ALL=de_DE");
		setlocale (LC_ALL, "Espanish");
		break;
	default:
		putenv ("LC_ALL=en_EN");
		setlocale (LC_ALL, "English");
}

$lang = array(
	'en' => 'English',
	'es' => 'Spanish',
	'ru' => 'Russian'
);

bindtextdomain ($domain, ABS_PATH . 'locale');

textdomain ($domain);