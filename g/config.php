<?php

if ( false === function_exists('gettext') ) {
	echo "You do not have the gettext library installed with PHP.";
	exit;
}

$directory = dirname(__FILE__) . '/locale';
$domain = 'messages';
$locale = "ru_RU.UTF-8";

setlocale(LC_MESSAGES, $locale);
bindtextdomain($domain, $directory);
textdomain($domain);
//bind_textdomain_codeset($domain, 'UTF-8');


echo _('Hello, My people<br />');